f9648b6eb13e13bb51a8c836dce79f90
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = require("@angular/router");
const testing_1 = require("@angular/common/http/testing");
const testing_2 = require("@angular/core/testing");
const ng_jhipster_1 = require("ng-jhipster");
const ngx_webstorage_1 = require("ngx-webstorage");
const app_constants_1 = require("app/app.constants");
const account_service_1 = require("app/core/auth/account.service");
const authority_constants_1 = require("app/shared/constants/authority.constants");
const state_storage_service_1 = require("app/core/auth/state-storage.service");
const mock_language_service_1 = require("../../../helpers/mock-language.service");
const mock_route_service_1 = require("../../../helpers/mock-route.service");
const mock_state_storage_service_1 = require("../../../helpers/mock-state-storage.service");
function accountWithAuthorities(authorities) {
    return {
        activated: true,
        authorities,
        email: '',
        firstName: '',
        langKey: '',
        lastName: '',
        login: '',
        imageUrl: ''
    };
}
describe('Service Tests', () => {
    describe('Account Service', () => {
        let service;
        let httpMock;
        let storageService;
        let router;
        beforeEach(() => {
            testing_2.TestBed.configureTestingModule({
                imports: [testing_1.HttpClientTestingModule, ngx_webstorage_1.NgxWebstorageModule.forRoot()],
                providers: [
                    ng_jhipster_1.JhiDateUtils,
                    {
                        provide: ng_jhipster_1.JhiLanguageService,
                        useClass: mock_language_service_1.MockLanguageService
                    },
                    {
                        provide: state_storage_service_1.StateStorageService,
                        useClass: mock_state_storage_service_1.MockStateStorageService
                    },
                    {
                        provide: router_1.Router,
                        useClass: mock_route_service_1.MockRouter
                    }
                ]
            });
            service = testing_2.TestBed.get(account_service_1.AccountService);
            httpMock = testing_2.TestBed.get(testing_1.HttpTestingController);
            storageService = testing_2.TestBed.get(state_storage_service_1.StateStorageService);
            router = testing_2.TestBed.get(router_1.Router);
        });
        afterEach(() => {
            httpMock.verify();
        });
        describe('authenticate', () => {
            it('authenticationState should emit null if input is null', () => {
                // GIVEN
                let userIdentity = accountWithAuthorities([]);
                service.getAuthenticationState().subscribe(account => (userIdentity = account));
                // WHEN
                service.authenticate(null);
                // THEN
                expect(userIdentity).toBeNull();
                expect(service.isAuthenticated()).toBe(false);
            });
            it('authenticationState should emit the same account as was in input parameter', () => {
                // GIVEN
                const expectedResult = accountWithAuthorities([]);
                let userIdentity = null;
                service.getAuthenticationState().subscribe(account => (userIdentity = account));
                // WHEN
                service.authenticate(expectedResult);
                // THEN
                expect(userIdentity).toEqual(expectedResult);
                expect(service.isAuthenticated()).toBe(true);
            });
        });
        describe('identity', () => {
            it('should call /account if user is undefined', () => {
                service.identity().subscribe();
                const req = httpMock.expectOne({ method: 'GET' });
                const resourceUrl = app_constants_1.SERVER_API_URL + 'api/account';
                expect(req.request.url).toEqual(`${resourceUrl}`);
            });
            it('should call /account only once if not logged out after first authentication and should call /account again if user has logged out', () => {
                // Given the user is authenticated
                service.identity().subscribe();
                httpMock.expectOne({ method: 'GET' }).flush({});
                // When I call
                service.identity().subscribe();
                // Then there is no second request
                httpMock.expectNone({ method: 'GET' });
                // When I log out
                service.authenticate(null);
                // and then call
                service.identity().subscribe();
                // Then there is a new request
                httpMock.expectOne({ method: 'GET' });
            });
            describe('navigateToStoredUrl', () => {
                it('should navigate to the previous stored url post successful authentication', () => {
                    // GIVEN
                    storageService.setResponse('admin/users?page=0');
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush({});
                    // THEN
                    expect(storageService.getUrlSpy).toHaveBeenCalledTimes(1);
                    expect(storageService.clearUrlSpy).toHaveBeenCalledTimes(1);
                    expect(router.navigateByUrlSpy).toHaveBeenCalledWith('admin/users?page=0');
                });
                it('should not navigate to the previous stored url when authentication fails', () => {
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).error(new ErrorEvent(''));
                    // THEN
                    expect(storageService.getUrlSpy).not.toHaveBeenCalled();
                    expect(storageService.clearUrlSpy).not.toHaveBeenCalled();
                    expect(router.navigateByUrlSpy).not.toHaveBeenCalled();
                });
                it('should not navigate to the previous stored url when no such url exists post successful authentication', () => {
                    // GIVEN
                    storageService.setResponse(null);
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush({});
                    // THEN
                    expect(storageService.getUrlSpy).toHaveBeenCalledTimes(1);
                    expect(storageService.clearUrlSpy).not.toHaveBeenCalled();
                    expect(router.navigateByUrlSpy).not.toHaveBeenCalled();
                });
            });
        });
        describe('hasAnyAuthority', () => {
            describe('hasAnyAuthority string parameter', () => {
                it('should return false if user is not logged', () => {
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.USER);
                    expect(hasAuthority).toBe(false);
                });
                it('should return false if user is logged and has not authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.ADMIN);
                    expect(hasAuthority).toBe(false);
                });
                it('should return true if user is logged and has authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.USER);
                    expect(hasAuthority).toBe(true);
                });
            });
            describe('hasAnyAuthority array parameter', () => {
                it('should return false if user is not logged', () => {
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.USER]);
                    expect(hasAuthority).toBeFalsy();
                });
                it('should return false if user is logged and has not authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.ADMIN]);
                    expect(hasAuthority).toBe(false);
                });
                it('should return true if user is logged and has authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.USER, authority_constants_1.Authority.ADMIN]);
                    expect(hasAuthority).toBe(true);
                });
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvY29yZS91c2VyL2FjY291bnQuc2VydmljZS5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsNENBQXlDO0FBQ3pDLDBEQUE4RjtBQUM5RixtREFBZ0Q7QUFDaEQsNkNBQStEO0FBQy9ELG1EQUFxRDtBQUVyRCxxREFBbUQ7QUFDbkQsbUVBQStEO0FBRS9ELGtGQUFxRTtBQUNyRSwrRUFBMEU7QUFDMUUsa0ZBQTZFO0FBQzdFLDRFQUFpRTtBQUNqRSw0RkFBc0Y7QUFFdEYsU0FBUyxzQkFBc0IsQ0FBQyxXQUFxQjtJQUNuRCxPQUFPO1FBQ0wsU0FBUyxFQUFFLElBQUk7UUFDZixXQUFXO1FBQ1gsS0FBSyxFQUFFLEVBQUU7UUFDVCxTQUFTLEVBQUUsRUFBRTtRQUNiLE9BQU8sRUFBRSxFQUFFO1FBQ1gsUUFBUSxFQUFFLEVBQUU7UUFDWixLQUFLLEVBQUUsRUFBRTtRQUNULFFBQVEsRUFBRSxFQUFFO0tBQ2IsQ0FBQztBQUNKLENBQUM7QUFFRCxRQUFRLENBQUMsZUFBZSxFQUFFLEdBQUcsRUFBRTtJQUM3QixRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO1FBQy9CLElBQUksT0FBdUIsQ0FBQztRQUM1QixJQUFJLFFBQStCLENBQUM7UUFDcEMsSUFBSSxjQUF1QyxDQUFDO1FBQzVDLElBQUksTUFBa0IsQ0FBQztRQUV2QixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQXVCLEVBQUUsb0NBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2pFLFNBQVMsRUFBRTtvQkFDVCwwQkFBWTtvQkFDWjt3QkFDRSxPQUFPLEVBQUUsZ0NBQWtCO3dCQUMzQixRQUFRLEVBQUUsMkNBQW1CO3FCQUM5QjtvQkFDRDt3QkFDRSxPQUFPLEVBQUUsMkNBQW1CO3dCQUM1QixRQUFRLEVBQUUsb0RBQXVCO3FCQUNsQztvQkFDRDt3QkFDRSxPQUFPLEVBQUUsZUFBTTt3QkFDZixRQUFRLEVBQUUsK0JBQVU7cUJBQ3JCO2lCQUNGO2FBQ0YsQ0FBQyxDQUFDO1lBRUgsT0FBTyxHQUFHLGlCQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFjLENBQUMsQ0FBQztZQUN0QyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxHQUFHLENBQUMsK0JBQXFCLENBQUMsQ0FBQztZQUM5QyxjQUFjLEdBQUcsaUJBQU8sQ0FBQyxHQUFHLENBQUMsMkNBQW1CLENBQUMsQ0FBQztZQUNsRCxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxHQUFHLENBQUMsZUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2IsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLGNBQWMsRUFBRSxHQUFHLEVBQUU7WUFDNUIsRUFBRSxDQUFDLHVEQUF1RCxFQUFFLEdBQUcsRUFBRTtnQkFDL0QsUUFBUTtnQkFDUixJQUFJLFlBQVksR0FBbUIsc0JBQXNCLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzlELE9BQU8sQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBRWhGLE9BQU87Z0JBQ1AsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFM0IsT0FBTztnQkFDUCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ2hDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEQsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsNEVBQTRFLEVBQUUsR0FBRyxFQUFFO2dCQUNwRixRQUFRO2dCQUNSLE1BQU0sY0FBYyxHQUFHLHNCQUFzQixDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxJQUFJLFlBQVksR0FBbUIsSUFBSSxDQUFDO2dCQUN4QyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUVoRixPQUFPO2dCQUNQLE9BQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBRXJDLE9BQU87Z0JBQ1AsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDN0MsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLEVBQUU7WUFDeEIsRUFBRSxDQUFDLDJDQUEyQyxFQUFFLEdBQUcsRUFBRTtnQkFDbkQsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMvQixNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sV0FBVyxHQUFHLDhCQUFjLEdBQUcsYUFBYSxDQUFDO2dCQUVuRCxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFDO1lBRUgsRUFBRSxDQUFDLG1JQUFtSSxFQUFFLEdBQUcsRUFBRTtnQkFDM0ksa0NBQWtDO2dCQUNsQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRWhELGNBQWM7Z0JBQ2QsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUUvQixrQ0FBa0M7Z0JBQ2xDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFFdkMsaUJBQWlCO2dCQUNqQixPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQixnQkFBZ0I7Z0JBQ2hCLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFFL0IsOEJBQThCO2dCQUM5QixRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7WUFFSCxRQUFRLENBQUMscUJBQXFCLEVBQUUsR0FBRyxFQUFFO2dCQUNuQyxFQUFFLENBQUMsMkVBQTJFLEVBQUUsR0FBRyxFQUFFO29CQUNuRixRQUFRO29CQUNSLGNBQWMsQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFFakQsT0FBTztvQkFDUCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBRWhELE9BQU87b0JBQ1AsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBQzdFLENBQUMsQ0FBQyxDQUFDO2dCQUVILEVBQUUsQ0FBQywwRUFBMEUsRUFBRSxHQUFHLEVBQUU7b0JBQ2xGLE9BQU87b0JBQ1AsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUMvQixRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBRWhFLE9BQU87b0JBQ1AsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDeEQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDMUQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN6RCxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsdUdBQXVHLEVBQUUsR0FBRyxFQUFFO29CQUMvRyxRQUFRO29CQUNSLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRWpDLE9BQU87b0JBQ1AsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUMvQixRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUVoRCxPQUFPO29CQUNQLE1BQU0sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzFELE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQzFELE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDekQsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtZQUMvQixRQUFRLENBQUMsa0NBQWtDLEVBQUUsR0FBRyxFQUFFO2dCQUNoRCxFQUFFLENBQUMsMkNBQTJDLEVBQUUsR0FBRyxFQUFFO29CQUNuRCxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzdELE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsQ0FBQyxDQUFDO2dCQUVILEVBQUUsQ0FBQyw2REFBNkQsRUFBRSxHQUFHLEVBQUU7b0JBQ3JFLE9BQU8sQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFL0QsTUFBTSxZQUFZLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQywrQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUU5RCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsd0RBQXdELEVBQUUsR0FBRyxFQUFFO29CQUNoRSxPQUFPLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRS9ELE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFN0QsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEMsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILFFBQVEsQ0FBQyxpQ0FBaUMsRUFBRSxHQUFHLEVBQUU7Z0JBQy9DLEVBQUUsQ0FBQywyQ0FBMkMsRUFBRSxHQUFHLEVBQUU7b0JBQ25ELE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQy9ELE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLDZEQUE2RCxFQUFFLEdBQUcsRUFBRTtvQkFDckUsT0FBTyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUUvRCxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsK0JBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUVoRSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsd0RBQXdELEVBQUUsR0FBRyxFQUFFO29CQUNoRSxPQUFPLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRS9ELE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQywrQkFBUyxDQUFDLElBQUksRUFBRSwrQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBRWhGLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvY29yZS91c2VyL2FjY291bnQuc2VydmljZS5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBIdHRwQ2xpZW50VGVzdGluZ01vZHVsZSwgSHR0cFRlc3RpbmdDb250cm9sbGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAvdGVzdGluZyc7XG5pbXBvcnQgeyBUZXN0QmVkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IEpoaURhdGVVdGlscywgSmhpTGFuZ3VhZ2VTZXJ2aWNlIH0gZnJvbSAnbmctamhpcHN0ZXInO1xuaW1wb3J0IHsgTmd4V2Vic3RvcmFnZU1vZHVsZSB9IGZyb20gJ25neC13ZWJzdG9yYWdlJztcblxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkwgfSBmcm9tICdhcHAvYXBwLmNvbnN0YW50cyc7XG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gJ2FwcC9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcbmltcG9ydCB7IEFjY291bnQgfSBmcm9tICdhcHAvY29yZS91c2VyL2FjY291bnQubW9kZWwnO1xuaW1wb3J0IHsgQXV0aG9yaXR5IH0gZnJvbSAnYXBwL3NoYXJlZC9jb25zdGFudHMvYXV0aG9yaXR5LmNvbnN0YW50cyc7XG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHsgTW9ja0xhbmd1YWdlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2hlbHBlcnMvbW9jay1sYW5ndWFnZS5zZXJ2aWNlJztcbmltcG9ydCB7IE1vY2tSb3V0ZXIgfSBmcm9tICcuLi8uLi8uLi9oZWxwZXJzL21vY2stcm91dGUuc2VydmljZSc7XG5pbXBvcnQgeyBNb2NrU3RhdGVTdG9yYWdlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2hlbHBlcnMvbW9jay1zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnO1xuXG5mdW5jdGlvbiBhY2NvdW50V2l0aEF1dGhvcml0aWVzKGF1dGhvcml0aWVzOiBzdHJpbmdbXSk6IEFjY291bnQge1xuICByZXR1cm4ge1xuICAgIGFjdGl2YXRlZDogdHJ1ZSxcbiAgICBhdXRob3JpdGllcyxcbiAgICBlbWFpbDogJycsXG4gICAgZmlyc3ROYW1lOiAnJyxcbiAgICBsYW5nS2V5OiAnJyxcbiAgICBsYXN0TmFtZTogJycsXG4gICAgbG9naW46ICcnLFxuICAgIGltYWdlVXJsOiAnJ1xuICB9O1xufVxuXG5kZXNjcmliZSgnU2VydmljZSBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ0FjY291bnQgU2VydmljZScsICgpID0+IHtcbiAgICBsZXQgc2VydmljZTogQWNjb3VudFNlcnZpY2U7XG4gICAgbGV0IGh0dHBNb2NrOiBIdHRwVGVzdGluZ0NvbnRyb2xsZXI7XG4gICAgbGV0IHN0b3JhZ2VTZXJ2aWNlOiBNb2NrU3RhdGVTdG9yYWdlU2VydmljZTtcbiAgICBsZXQgcm91dGVyOiBNb2NrUm91dGVyO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbSHR0cENsaWVudFRlc3RpbmdNb2R1bGUsIE5neFdlYnN0b3JhZ2VNb2R1bGUuZm9yUm9vdCgpXSxcbiAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgSmhpRGF0ZVV0aWxzLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHByb3ZpZGU6IEpoaUxhbmd1YWdlU2VydmljZSxcbiAgICAgICAgICAgIHVzZUNsYXNzOiBNb2NrTGFuZ3VhZ2VTZXJ2aWNlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBTdGF0ZVN0b3JhZ2VTZXJ2aWNlLFxuICAgICAgICAgICAgdXNlQ2xhc3M6IE1vY2tTdGF0ZVN0b3JhZ2VTZXJ2aWNlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBSb3V0ZXIsXG4gICAgICAgICAgICB1c2VDbGFzczogTW9ja1JvdXRlclxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSk7XG5cbiAgICAgIHNlcnZpY2UgPSBUZXN0QmVkLmdldChBY2NvdW50U2VydmljZSk7XG4gICAgICBodHRwTW9jayA9IFRlc3RCZWQuZ2V0KEh0dHBUZXN0aW5nQ29udHJvbGxlcik7XG4gICAgICBzdG9yYWdlU2VydmljZSA9IFRlc3RCZWQuZ2V0KFN0YXRlU3RvcmFnZVNlcnZpY2UpO1xuICAgICAgcm91dGVyID0gVGVzdEJlZC5nZXQoUm91dGVyKTtcbiAgICB9KTtcblxuICAgIGFmdGVyRWFjaCgoKSA9PiB7XG4gICAgICBodHRwTW9jay52ZXJpZnkoKTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdhdXRoZW50aWNhdGUnLCAoKSA9PiB7XG4gICAgICBpdCgnYXV0aGVudGljYXRpb25TdGF0ZSBzaG91bGQgZW1pdCBudWxsIGlmIGlucHV0IGlzIG51bGwnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGxldCB1c2VySWRlbnRpdHk6IEFjY291bnQgfCBudWxsID0gYWNjb3VudFdpdGhBdXRob3JpdGllcyhbXSk7XG4gICAgICAgIHNlcnZpY2UuZ2V0QXV0aGVudGljYXRpb25TdGF0ZSgpLnN1YnNjcmliZShhY2NvdW50ID0+ICh1c2VySWRlbnRpdHkgPSBhY2NvdW50KSk7XG5cbiAgICAgICAgLy8gV0hFTlxuICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShudWxsKTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdCh1c2VySWRlbnRpdHkpLnRvQmVOdWxsKCk7XG4gICAgICAgIGV4cGVjdChzZXJ2aWNlLmlzQXV0aGVudGljYXRlZCgpKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pO1xuXG4gICAgICBpdCgnYXV0aGVudGljYXRpb25TdGF0ZSBzaG91bGQgZW1pdCB0aGUgc2FtZSBhY2NvdW50IGFzIHdhcyBpbiBpbnB1dCBwYXJhbWV0ZXInLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGV4cGVjdGVkUmVzdWx0ID0gYWNjb3VudFdpdGhBdXRob3JpdGllcyhbXSk7XG4gICAgICAgIGxldCB1c2VySWRlbnRpdHk6IEFjY291bnQgfCBudWxsID0gbnVsbDtcbiAgICAgICAgc2VydmljZS5nZXRBdXRoZW50aWNhdGlvblN0YXRlKCkuc3Vic2NyaWJlKGFjY291bnQgPT4gKHVzZXJJZGVudGl0eSA9IGFjY291bnQpKTtcblxuICAgICAgICAvLyBXSEVOXG4gICAgICAgIHNlcnZpY2UuYXV0aGVudGljYXRlKGV4cGVjdGVkUmVzdWx0KTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdCh1c2VySWRlbnRpdHkpLnRvRXF1YWwoZXhwZWN0ZWRSZXN1bHQpO1xuICAgICAgICBleHBlY3Qoc2VydmljZS5pc0F1dGhlbnRpY2F0ZWQoKSkudG9CZSh0cnVlKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ2lkZW50aXR5JywgKCkgPT4ge1xuICAgICAgaXQoJ3Nob3VsZCBjYWxsIC9hY2NvdW50IGlmIHVzZXIgaXMgdW5kZWZpbmVkJywgKCkgPT4ge1xuICAgICAgICBzZXJ2aWNlLmlkZW50aXR5KCkuc3Vic2NyaWJlKCk7XG4gICAgICAgIGNvbnN0IHJlcSA9IGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSk7XG4gICAgICAgIGNvbnN0IHJlc291cmNlVXJsID0gU0VSVkVSX0FQSV9VUkwgKyAnYXBpL2FjY291bnQnO1xuXG4gICAgICAgIGV4cGVjdChyZXEucmVxdWVzdC51cmwpLnRvRXF1YWwoYCR7cmVzb3VyY2VVcmx9YCk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCBjYWxsIC9hY2NvdW50IG9ubHkgb25jZSBpZiBub3QgbG9nZ2VkIG91dCBhZnRlciBmaXJzdCBhdXRoZW50aWNhdGlvbiBhbmQgc2hvdWxkIGNhbGwgL2FjY291bnQgYWdhaW4gaWYgdXNlciBoYXMgbG9nZ2VkIG91dCcsICgpID0+IHtcbiAgICAgICAgLy8gR2l2ZW4gdGhlIHVzZXIgaXMgYXV0aGVudGljYXRlZFxuICAgICAgICBzZXJ2aWNlLmlkZW50aXR5KCkuc3Vic2NyaWJlKCk7XG4gICAgICAgIGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSkuZmx1c2goe30pO1xuXG4gICAgICAgIC8vIFdoZW4gSSBjYWxsXG4gICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcblxuICAgICAgICAvLyBUaGVuIHRoZXJlIGlzIG5vIHNlY29uZCByZXF1ZXN0XG4gICAgICAgIGh0dHBNb2NrLmV4cGVjdE5vbmUoeyBtZXRob2Q6ICdHRVQnIH0pO1xuXG4gICAgICAgIC8vIFdoZW4gSSBsb2cgb3V0XG4gICAgICAgIHNlcnZpY2UuYXV0aGVudGljYXRlKG51bGwpO1xuICAgICAgICAvLyBhbmQgdGhlbiBjYWxsXG4gICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcblxuICAgICAgICAvLyBUaGVuIHRoZXJlIGlzIGEgbmV3IHJlcXVlc3RcbiAgICAgICAgaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KTtcbiAgICAgIH0pO1xuXG4gICAgICBkZXNjcmliZSgnbmF2aWdhdGVUb1N0b3JlZFVybCcsICgpID0+IHtcbiAgICAgICAgaXQoJ3Nob3VsZCBuYXZpZ2F0ZSB0byB0aGUgcHJldmlvdXMgc3RvcmVkIHVybCBwb3N0IHN1Y2Nlc3NmdWwgYXV0aGVudGljYXRpb24nLCAoKSA9PiB7XG4gICAgICAgICAgLy8gR0lWRU5cbiAgICAgICAgICBzdG9yYWdlU2VydmljZS5zZXRSZXNwb25zZSgnYWRtaW4vdXNlcnM/cGFnZT0wJyk7XG5cbiAgICAgICAgICAvLyBXSEVOXG4gICAgICAgICAgc2VydmljZS5pZGVudGl0eSgpLnN1YnNjcmliZSgpO1xuICAgICAgICAgIGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSkuZmx1c2goe30pO1xuXG4gICAgICAgICAgLy8gVEhFTlxuICAgICAgICAgIGV4cGVjdChzdG9yYWdlU2VydmljZS5nZXRVcmxTcHkpLnRvSGF2ZUJlZW5DYWxsZWRUaW1lcygxKTtcbiAgICAgICAgICBleHBlY3Qoc3RvcmFnZVNlcnZpY2UuY2xlYXJVcmxTcHkpLnRvSGF2ZUJlZW5DYWxsZWRUaW1lcygxKTtcbiAgICAgICAgICBleHBlY3Qocm91dGVyLm5hdmlnYXRlQnlVcmxTcHkpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKCdhZG1pbi91c2Vycz9wYWdlPTAnKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaXQoJ3Nob3VsZCBub3QgbmF2aWdhdGUgdG8gdGhlIHByZXZpb3VzIHN0b3JlZCB1cmwgd2hlbiBhdXRoZW50aWNhdGlvbiBmYWlscycsICgpID0+IHtcbiAgICAgICAgICAvLyBXSEVOXG4gICAgICAgICAgc2VydmljZS5pZGVudGl0eSgpLnN1YnNjcmliZSgpO1xuICAgICAgICAgIGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSkuZXJyb3IobmV3IEVycm9yRXZlbnQoJycpKTtcblxuICAgICAgICAgIC8vIFRIRU5cbiAgICAgICAgICBleHBlY3Qoc3RvcmFnZVNlcnZpY2UuZ2V0VXJsU3B5KS5ub3QudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICAgIGV4cGVjdChzdG9yYWdlU2VydmljZS5jbGVhclVybFNweSkubm90LnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgICBleHBlY3Qocm91dGVyLm5hdmlnYXRlQnlVcmxTcHkpLm5vdC50b0hhdmVCZWVuQ2FsbGVkKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGl0KCdzaG91bGQgbm90IG5hdmlnYXRlIHRvIHRoZSBwcmV2aW91cyBzdG9yZWQgdXJsIHdoZW4gbm8gc3VjaCB1cmwgZXhpc3RzIHBvc3Qgc3VjY2Vzc2Z1bCBhdXRoZW50aWNhdGlvbicsICgpID0+IHtcbiAgICAgICAgICAvLyBHSVZFTlxuICAgICAgICAgIHN0b3JhZ2VTZXJ2aWNlLnNldFJlc3BvbnNlKG51bGwpO1xuXG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcbiAgICAgICAgICBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdHRVQnIH0pLmZsdXNoKHt9KTtcblxuICAgICAgICAgIC8vIFRIRU5cbiAgICAgICAgICBleHBlY3Qoc3RvcmFnZVNlcnZpY2UuZ2V0VXJsU3B5KS50b0hhdmVCZWVuQ2FsbGVkVGltZXMoMSk7XG4gICAgICAgICAgZXhwZWN0KHN0b3JhZ2VTZXJ2aWNlLmNsZWFyVXJsU3B5KS5ub3QudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICAgIGV4cGVjdChyb3V0ZXIubmF2aWdhdGVCeVVybFNweSkubm90LnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdoYXNBbnlBdXRob3JpdHknLCAoKSA9PiB7XG4gICAgICBkZXNjcmliZSgnaGFzQW55QXV0aG9yaXR5IHN0cmluZyBwYXJhbWV0ZXInLCAoKSA9PiB7XG4gICAgICAgIGl0KCdzaG91bGQgcmV0dXJuIGZhbHNlIGlmIHVzZXIgaXMgbm90IGxvZ2dlZCcsICgpID0+IHtcbiAgICAgICAgICBjb25zdCBoYXNBdXRob3JpdHkgPSBzZXJ2aWNlLmhhc0FueUF1dGhvcml0eShBdXRob3JpdHkuVVNFUik7XG4gICAgICAgICAgZXhwZWN0KGhhc0F1dGhvcml0eSkudG9CZShmYWxzZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGl0KCdzaG91bGQgcmV0dXJuIGZhbHNlIGlmIHVzZXIgaXMgbG9nZ2VkIGFuZCBoYXMgbm90IGF1dGhvcml0eScsICgpID0+IHtcbiAgICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShhY2NvdW50V2l0aEF1dGhvcml0aWVzKFtBdXRob3JpdHkuVVNFUl0pKTtcblxuICAgICAgICAgIGNvbnN0IGhhc0F1dGhvcml0eSA9IHNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KEF1dGhvcml0eS5BRE1JTik7XG5cbiAgICAgICAgICBleHBlY3QoaGFzQXV0aG9yaXR5KS50b0JlKGZhbHNlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaXQoJ3Nob3VsZCByZXR1cm4gdHJ1ZSBpZiB1c2VyIGlzIGxvZ2dlZCBhbmQgaGFzIGF1dGhvcml0eScsICgpID0+IHtcbiAgICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShhY2NvdW50V2l0aEF1dGhvcml0aWVzKFtBdXRob3JpdHkuVVNFUl0pKTtcblxuICAgICAgICAgIGNvbnN0IGhhc0F1dGhvcml0eSA9IHNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KEF1dGhvcml0eS5VU0VSKTtcblxuICAgICAgICAgIGV4cGVjdChoYXNBdXRob3JpdHkpLnRvQmUodHJ1ZSk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG5cbiAgICAgIGRlc2NyaWJlKCdoYXNBbnlBdXRob3JpdHkgYXJyYXkgcGFyYW1ldGVyJywgKCkgPT4ge1xuICAgICAgICBpdCgnc2hvdWxkIHJldHVybiBmYWxzZSBpZiB1c2VyIGlzIG5vdCBsb2dnZWQnLCAoKSA9PiB7XG4gICAgICAgICAgY29uc3QgaGFzQXV0aG9yaXR5ID0gc2VydmljZS5oYXNBbnlBdXRob3JpdHkoW0F1dGhvcml0eS5VU0VSXSk7XG4gICAgICAgICAgZXhwZWN0KGhhc0F1dGhvcml0eSkudG9CZUZhbHN5KCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGl0KCdzaG91bGQgcmV0dXJuIGZhbHNlIGlmIHVzZXIgaXMgbG9nZ2VkIGFuZCBoYXMgbm90IGF1dGhvcml0eScsICgpID0+IHtcbiAgICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShhY2NvdW50V2l0aEF1dGhvcml0aWVzKFtBdXRob3JpdHkuVVNFUl0pKTtcblxuICAgICAgICAgIGNvbnN0IGhhc0F1dGhvcml0eSA9IHNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KFtBdXRob3JpdHkuQURNSU5dKTtcblxuICAgICAgICAgIGV4cGVjdChoYXNBdXRob3JpdHkpLnRvQmUoZmFsc2UpO1xuICAgICAgICB9KTtcblxuICAgICAgICBpdCgnc2hvdWxkIHJldHVybiB0cnVlIGlmIHVzZXIgaXMgbG9nZ2VkIGFuZCBoYXMgYXV0aG9yaXR5JywgKCkgPT4ge1xuICAgICAgICAgIHNlcnZpY2UuYXV0aGVudGljYXRlKGFjY291bnRXaXRoQXV0aG9yaXRpZXMoW0F1dGhvcml0eS5VU0VSXSkpO1xuXG4gICAgICAgICAgY29uc3QgaGFzQXV0aG9yaXR5ID0gc2VydmljZS5oYXNBbnlBdXRob3JpdHkoW0F1dGhvcml0eS5VU0VSLCBBdXRob3JpdHkuQURNSU5dKTtcblxuICAgICAgICAgIGV4cGVjdChoYXNBdXRob3JpdHkpLnRvQmUodHJ1ZSk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=