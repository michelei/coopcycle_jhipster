"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const product_model_1 = require("app/shared/model/product.model");
const product_service_1 = require("./product.service");
const basket_service_1 = require("app/entities/basket/basket.service");
const restaurant_service_1 = require("app/entities/restaurant/restaurant.service");
let ProductUpdateComponent = class ProductUpdateComponent {
    constructor(productService, basketService, restaurantService, activatedRoute, fb) {
        this.productService = productService;
        this.basketService = basketService;
        this.restaurantService = restaurantService;
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.isSaving = false;
        this.baskets = [];
        this.restaurants = [];
        this.editForm = this.fb.group({
            id: [],
            price: [null, [forms_1.Validators.required, forms_1.Validators.min(0)]],
            disponibility: [],
            description: [],
            basket: [],
            restaurant: []
        });
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ product }) => {
            this.updateForm(product);
            this.basketService.query().subscribe((res) => (this.baskets = res.body || []));
            this.restaurantService.query().subscribe((res) => (this.restaurants = res.body || []));
        });
    }
    updateForm(product) {
        this.editForm.patchValue({
            id: product.id,
            price: product.price,
            disponibility: product.disponibility,
            description: product.description,
            basket: product.basket,
            restaurant: product.restaurant
        });
    }
    previousState() {
        window.history.back();
    }
    save() {
        this.isSaving = true;
        const product = this.createFromForm();
        if (product.id !== undefined) {
            this.subscribeToSaveResponse(this.productService.update(product));
        }
        else {
            this.subscribeToSaveResponse(this.productService.create(product));
        }
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new product_model_1.Product()), { id: this.editForm.get(['id']).value, price: this.editForm.get(['price']).value, disponibility: this.editForm.get(['disponibility']).value, description: this.editForm.get(['description']).value, basket: this.editForm.get(['basket']).value, restaurant: this.editForm.get(['restaurant']).value });
    }
    subscribeToSaveResponse(result) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
    }
    trackById(index, item) {
        return item.id;
    }
};
ProductUpdateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-product-update',
        template: require('./product-update.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [product_service_1.ProductService,
        basket_service_1.BasketService,
        restaurant_service_1.RestaurantService,
        router_1.ActivatedRoute,
        forms_1.FormBuilder])
], ProductUpdateComponent);
exports.ProductUpdateComponent = ProductUpdateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvcHJvZHVjdC9wcm9kdWN0LXVwZGF0ZS5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBRWxELDZEQUE2RDtBQUM3RCwwQ0FBeUQ7QUFDekQsNENBQWlEO0FBR2pELGtFQUFtRTtBQUNuRSx1REFBbUQ7QUFFbkQsdUVBQW1FO0FBRW5FLG1GQUErRTtBQVEvRSxJQUFhLHNCQUFzQixHQUFuQyxNQUFhLHNCQUFzQjtJQWNqQyxZQUNZLGNBQThCLEVBQzlCLGFBQTRCLEVBQzVCLGlCQUFvQyxFQUNwQyxjQUE4QixFQUNoQyxFQUFlO1FBSmIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQ2hDLE9BQUUsR0FBRixFQUFFLENBQWE7UUFsQnpCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsWUFBTyxHQUFjLEVBQUUsQ0FBQztRQUN4QixnQkFBVyxHQUFrQixFQUFFLENBQUM7UUFFaEMsYUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLEVBQUUsRUFBRSxFQUFFO1lBQ04sS0FBSyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RCxhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtZQUNmLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLEVBQUU7U0FDZixDQUFDLENBQUM7SUFRQSxDQUFDO0lBRUosUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRTtZQUNqRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXpCLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBNEIsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUV4RyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBZ0MsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0SCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsT0FBaUI7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7WUFDdkIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO1lBQ2QsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLO1lBQ3BCLGFBQWEsRUFBRSxPQUFPLENBQUMsYUFBYTtZQUNwQyxXQUFXLEVBQUUsT0FBTyxDQUFDLFdBQVc7WUFDaEMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxNQUFNO1lBQ3RCLFVBQVUsRUFBRSxPQUFPLENBQUMsVUFBVTtTQUMvQixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNYLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEMsSUFBSSxPQUFPLENBQUMsRUFBRSxLQUFLLFNBQVMsRUFBRTtZQUM1QixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUNuRTthQUFNO1lBQ0wsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7U0FDbkU7SUFDSCxDQUFDO0lBRU8sY0FBYztRQUNwQix1Q0FDSyxJQUFJLHVCQUFPLEVBQUUsS0FDaEIsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUUsQ0FBQyxLQUFLLEVBQ3BDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUMxQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFDMUQsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUUsQ0FBQyxLQUFLLEVBQ3RELE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUM1QyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBRSxDQUFDLEtBQUssSUFDcEQ7SUFDSixDQUFDO0lBRVMsdUJBQXVCLENBQUMsTUFBMEM7UUFDMUUsTUFBTSxDQUFDLFNBQVMsQ0FDZCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQzFCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDekIsQ0FBQztJQUNKLENBQUM7SUFFUyxhQUFhO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQWEsRUFBRSxJQUFzQjtRQUM3QyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQztDQUNGLENBQUE7QUF4Rlksc0JBQXNCO0lBSmxDLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLGtCQUFhLGlDQUFpQyxDQUFBO0tBQy9DLENBQUM7NkNBZ0I0QixnQ0FBYztRQUNmLDhCQUFhO1FBQ1Qsc0NBQWlCO1FBQ3BCLHVCQUFjO1FBQzVCLG1CQUFXO0dBbkJkLHNCQUFzQixDQXdGbEM7QUF4Rlksd0RBQXNCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2VudGl0aWVzL3Byb2R1Y3QvcHJvZHVjdC11cGRhdGUuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVudXNlZC12YXJzXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgSVByb2R1Y3QsIFByb2R1Y3QgfSBmcm9tICdhcHAvc2hhcmVkL21vZGVsL3Byb2R1Y3QubW9kZWwnO1xuaW1wb3J0IHsgUHJvZHVjdFNlcnZpY2UgfSBmcm9tICcuL3Byb2R1Y3Quc2VydmljZSc7XG5pbXBvcnQgeyBJQmFza2V0IH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9iYXNrZXQubW9kZWwnO1xuaW1wb3J0IHsgQmFza2V0U2VydmljZSB9IGZyb20gJ2FwcC9lbnRpdGllcy9iYXNrZXQvYmFza2V0LnNlcnZpY2UnO1xuaW1wb3J0IHsgSVJlc3RhdXJhbnQgfSBmcm9tICdhcHAvc2hhcmVkL21vZGVsL3Jlc3RhdXJhbnQubW9kZWwnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICdhcHAvZW50aXRpZXMvcmVzdGF1cmFudC9yZXN0YXVyYW50LnNlcnZpY2UnO1xuXG50eXBlIFNlbGVjdGFibGVFbnRpdHkgPSBJQmFza2V0IHwgSVJlc3RhdXJhbnQ7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1wcm9kdWN0LXVwZGF0ZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9kdWN0LXVwZGF0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgUHJvZHVjdFVwZGF0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGlzU2F2aW5nID0gZmFsc2U7XG4gIGJhc2tldHM6IElCYXNrZXRbXSA9IFtdO1xuICByZXN0YXVyYW50czogSVJlc3RhdXJhbnRbXSA9IFtdO1xuXG4gIGVkaXRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgaWQ6IFtdLFxuICAgIHByaWNlOiBbbnVsbCwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluKDApXV0sXG4gICAgZGlzcG9uaWJpbGl0eTogW10sXG4gICAgZGVzY3JpcHRpb246IFtdLFxuICAgIGJhc2tldDogW10sXG4gICAgcmVzdGF1cmFudDogW11cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHByb2R1Y3RTZXJ2aWNlOiBQcm9kdWN0U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgYmFza2V0U2VydmljZTogQmFza2V0U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcmVzdGF1cmFudFNlcnZpY2U6IFJlc3RhdXJhbnRTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXJcbiAgKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUuZGF0YS5zdWJzY3JpYmUoKHsgcHJvZHVjdCB9KSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZUZvcm0ocHJvZHVjdCk7XG5cbiAgICAgIHRoaXMuYmFza2V0U2VydmljZS5xdWVyeSgpLnN1YnNjcmliZSgocmVzOiBIdHRwUmVzcG9uc2U8SUJhc2tldFtdPikgPT4gKHRoaXMuYmFza2V0cyA9IHJlcy5ib2R5IHx8IFtdKSk7XG5cbiAgICAgIHRoaXMucmVzdGF1cmFudFNlcnZpY2UucXVlcnkoKS5zdWJzY3JpYmUoKHJlczogSHR0cFJlc3BvbnNlPElSZXN0YXVyYW50W10+KSA9PiAodGhpcy5yZXN0YXVyYW50cyA9IHJlcy5ib2R5IHx8IFtdKSk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVGb3JtKHByb2R1Y3Q6IElQcm9kdWN0KTogdm9pZCB7XG4gICAgdGhpcy5lZGl0Rm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgIGlkOiBwcm9kdWN0LmlkLFxuICAgICAgcHJpY2U6IHByb2R1Y3QucHJpY2UsXG4gICAgICBkaXNwb25pYmlsaXR5OiBwcm9kdWN0LmRpc3BvbmliaWxpdHksXG4gICAgICBkZXNjcmlwdGlvbjogcHJvZHVjdC5kZXNjcmlwdGlvbixcbiAgICAgIGJhc2tldDogcHJvZHVjdC5iYXNrZXQsXG4gICAgICByZXN0YXVyYW50OiBwcm9kdWN0LnJlc3RhdXJhbnRcbiAgICB9KTtcbiAgfVxuXG4gIHByZXZpb3VzU3RhdGUoKTogdm9pZCB7XG4gICAgd2luZG93Lmhpc3RvcnkuYmFjaygpO1xuICB9XG5cbiAgc2F2ZSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gdHJ1ZTtcbiAgICBjb25zdCBwcm9kdWN0ID0gdGhpcy5jcmVhdGVGcm9tRm9ybSgpO1xuICAgIGlmIChwcm9kdWN0LmlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UodGhpcy5wcm9kdWN0U2VydmljZS51cGRhdGUocHJvZHVjdCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHRoaXMucHJvZHVjdFNlcnZpY2UuY3JlYXRlKHByb2R1Y3QpKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUZyb21Gb3JtKCk6IElQcm9kdWN0IHtcbiAgICByZXR1cm4ge1xuICAgICAgLi4ubmV3IFByb2R1Y3QoKSxcbiAgICAgIGlkOiB0aGlzLmVkaXRGb3JtLmdldChbJ2lkJ10pIS52YWx1ZSxcbiAgICAgIHByaWNlOiB0aGlzLmVkaXRGb3JtLmdldChbJ3ByaWNlJ10pIS52YWx1ZSxcbiAgICAgIGRpc3BvbmliaWxpdHk6IHRoaXMuZWRpdEZvcm0uZ2V0KFsnZGlzcG9uaWJpbGl0eSddKSEudmFsdWUsXG4gICAgICBkZXNjcmlwdGlvbjogdGhpcy5lZGl0Rm9ybS5nZXQoWydkZXNjcmlwdGlvbiddKSEudmFsdWUsXG4gICAgICBiYXNrZXQ6IHRoaXMuZWRpdEZvcm0uZ2V0KFsnYmFza2V0J10pIS52YWx1ZSxcbiAgICAgIHJlc3RhdXJhbnQ6IHRoaXMuZWRpdEZvcm0uZ2V0KFsncmVzdGF1cmFudCddKSEudmFsdWVcbiAgICB9O1xuICB9XG5cbiAgcHJvdGVjdGVkIHN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHJlc3VsdDogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8SVByb2R1Y3Q+Pik6IHZvaWQge1xuICAgIHJlc3VsdC5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB0aGlzLm9uU2F2ZVN1Y2Nlc3MoKSxcbiAgICAgICgpID0+IHRoaXMub25TYXZlRXJyb3IoKVxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgb25TYXZlU3VjY2VzcygpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gZmFsc2U7XG4gICAgdGhpcy5wcmV2aW91c1N0YXRlKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgb25TYXZlRXJyb3IoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xuICB9XG5cbiAgdHJhY2tCeUlkKGluZGV4OiBudW1iZXIsIGl0ZW06IFNlbGVjdGFibGVFbnRpdHkpOiBhbnkge1xuICAgIHJldHVybiBpdGVtLmlkO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=