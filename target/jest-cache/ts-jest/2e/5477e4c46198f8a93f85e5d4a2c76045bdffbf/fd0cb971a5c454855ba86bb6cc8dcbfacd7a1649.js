"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const authority_constants_1 = require("app/shared/constants/authority.constants");
const test_module_1 = require("../../../test.module");
const user_management_detail_component_1 = require("app/admin/user-management/user-management-detail.component");
const user_model_1 = require("app/core/user/user.model");
describe('Component Tests', () => {
    describe('User Management Detail Component', () => {
        let comp;
        let fixture;
        const route = {
            data: rxjs_1.of({ user: new user_model_1.User(1, 'user', 'first', 'last', 'first@last.com', true, 'en', [authority_constants_1.Authority.USER], 'admin') })
        };
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [user_management_detail_component_1.UserManagementDetailComponent],
                providers: [
                    {
                        provide: router_1.ActivatedRoute,
                        useValue: route
                    }
                ]
            })
                .overrideTemplate(user_management_detail_component_1.UserManagementDetailComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(user_management_detail_component_1.UserManagementDetailComponent);
            comp = fixture.componentInstance;
        });
        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(comp.user).toEqual(jasmine.objectContaining({
                    id: 1,
                    login: 'user',
                    firstName: 'first',
                    lastName: 'last',
                    email: 'first@last.com',
                    activated: true,
                    langKey: 'en',
                    authorities: [authority_constants_1.Authority.USER],
                    createdBy: 'admin'
                }));
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC1kZXRhaWwuY29tcG9uZW50LnNwZWMudHMiLCJtYXBwaW5ncyI6Ijs7QUFBQSxtREFBeUU7QUFDekUsNENBQWlEO0FBQ2pELCtCQUEwQjtBQUUxQixrRkFBcUU7QUFDckUsc0RBQTJEO0FBQzNELGlIQUEyRztBQUMzRyx5REFBZ0Q7QUFFaEQsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsa0NBQWtDLEVBQUUsR0FBRyxFQUFFO1FBQ2hELElBQUksSUFBbUMsQ0FBQztRQUN4QyxJQUFJLE9BQXdELENBQUM7UUFDN0QsTUFBTSxLQUFLLEdBQW9CO1lBQzdCLElBQUksRUFBRSxTQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxpQkFBSSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRSxDQUFDO1NBQ3hGLENBQUM7UUFFNUIsVUFBVSxDQUFDLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDcEIsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQW1CLENBQUM7Z0JBQzlCLFlBQVksRUFBRSxDQUFDLGdFQUE2QixDQUFDO2dCQUM3QyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLHVCQUFjO3dCQUN2QixRQUFRLEVBQUUsS0FBSztxQkFDaEI7aUJBQ0Y7YUFDRixDQUFDO2lCQUNDLGdCQUFnQixDQUFDLGdFQUE2QixFQUFFLEVBQUUsQ0FBQztpQkFDbkQsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRUosVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxnRUFBNkIsQ0FBQyxDQUFDO1lBQ2pFLElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtZQUN0QixFQUFFLENBQUMsOEJBQThCLEVBQUUsR0FBRyxFQUFFO2dCQUN0QyxRQUFRO2dCQUVSLE9BQU87Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUVoQixPQUFPO2dCQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUN2QixPQUFPLENBQUMsZ0JBQWdCLENBQUM7b0JBQ3ZCLEVBQUUsRUFBRSxDQUFDO29CQUNMLEtBQUssRUFBRSxNQUFNO29CQUNiLFNBQVMsRUFBRSxPQUFPO29CQUNsQixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsS0FBSyxFQUFFLGdCQUFnQjtvQkFDdkIsU0FBUyxFQUFFLElBQUk7b0JBQ2YsT0FBTyxFQUFFLElBQUk7b0JBQ2IsV0FBVyxFQUFFLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLFNBQVMsRUFBRSxPQUFPO2lCQUNuQixDQUFDLENBQ0gsQ0FBQztZQUNKLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2FkbWluL3VzZXItbWFuYWdlbWVudC91c2VyLW1hbmFnZW1lbnQtZGV0YWlsLmNvbXBvbmVudC5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQsIGFzeW5jIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEF1dGhvcml0eSB9IGZyb20gJ2FwcC9zaGFyZWQvY29uc3RhbnRzL2F1dGhvcml0eS5jb25zdGFudHMnO1xuaW1wb3J0IHsgQ29vcGN5Y2xlVGVzdE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3Rlc3QubW9kdWxlJztcbmltcG9ydCB7IFVzZXJNYW5hZ2VtZW50RGV0YWlsQ29tcG9uZW50IH0gZnJvbSAnYXBwL2FkbWluL3VzZXItbWFuYWdlbWVudC91c2VyLW1hbmFnZW1lbnQtZGV0YWlsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnYXBwL2NvcmUvdXNlci91c2VyLm1vZGVsJztcblxuZGVzY3JpYmUoJ0NvbXBvbmVudCBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ1VzZXIgTWFuYWdlbWVudCBEZXRhaWwgQ29tcG9uZW50JywgKCkgPT4ge1xuICAgIGxldCBjb21wOiBVc2VyTWFuYWdlbWVudERldGFpbENvbXBvbmVudDtcbiAgICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxVc2VyTWFuYWdlbWVudERldGFpbENvbXBvbmVudD47XG4gICAgY29uc3Qgcm91dGU6IEFjdGl2YXRlZFJvdXRlID0gKHtcbiAgICAgIGRhdGE6IG9mKHsgdXNlcjogbmV3IFVzZXIoMSwgJ3VzZXInLCAnZmlyc3QnLCAnbGFzdCcsICdmaXJzdEBsYXN0LmNvbScsIHRydWUsICdlbicsIFtBdXRob3JpdHkuVVNFUl0sICdhZG1pbicpIH0pXG4gICAgfSBhcyBhbnkpIGFzIEFjdGl2YXRlZFJvdXRlO1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW1VzZXJNYW5hZ2VtZW50RGV0YWlsQ29tcG9uZW50XSxcbiAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgcHJvdmlkZTogQWN0aXZhdGVkUm91dGUsXG4gICAgICAgICAgICB1c2VWYWx1ZTogcm91dGVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0pXG4gICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKFVzZXJNYW5hZ2VtZW50RGV0YWlsQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgfSkpO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBmaXh0dXJlID0gVGVzdEJlZC5jcmVhdGVDb21wb25lbnQoVXNlck1hbmFnZW1lbnREZXRhaWxDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnT25Jbml0JywgKCkgPT4ge1xuICAgICAgaXQoJ1Nob3VsZCBjYWxsIGxvYWQgYWxsIG9uIGluaXQnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG5cbiAgICAgICAgLy8gV0hFTlxuICAgICAgICBjb21wLm5nT25Jbml0KCk7XG5cbiAgICAgICAgLy8gVEhFTlxuICAgICAgICBleHBlY3QoY29tcC51c2VyKS50b0VxdWFsKFxuICAgICAgICAgIGphc21pbmUub2JqZWN0Q29udGFpbmluZyh7XG4gICAgICAgICAgICBpZDogMSxcbiAgICAgICAgICAgIGxvZ2luOiAndXNlcicsXG4gICAgICAgICAgICBmaXJzdE5hbWU6ICdmaXJzdCcsXG4gICAgICAgICAgICBsYXN0TmFtZTogJ2xhc3QnLFxuICAgICAgICAgICAgZW1haWw6ICdmaXJzdEBsYXN0LmNvbScsXG4gICAgICAgICAgICBhY3RpdmF0ZWQ6IHRydWUsXG4gICAgICAgICAgICBsYW5nS2V5OiAnZW4nLFxuICAgICAgICAgICAgYXV0aG9yaXRpZXM6IFtBdXRob3JpdHkuVVNFUl0sXG4gICAgICAgICAgICBjcmVhdGVkQnk6ICdhZG1pbidcbiAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=