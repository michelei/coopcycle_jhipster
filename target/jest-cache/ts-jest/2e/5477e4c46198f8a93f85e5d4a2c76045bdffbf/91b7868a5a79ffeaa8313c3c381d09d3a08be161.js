"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const operators_1 = require("rxjs/operators");
const metrics_service_1 = require("./metrics.service");
let MetricsComponent = class MetricsComponent {
    constructor(metricsService, changeDetector) {
        this.metricsService = metricsService;
        this.changeDetector = changeDetector;
        this.updatingMetrics = true;
    }
    ngOnInit() {
        this.refresh();
    }
    refresh() {
        this.updatingMetrics = true;
        this.metricsService
            .getMetrics()
            .pipe(operators_1.flatMap(() => this.metricsService.threadDump(), (metrics, threadDump) => {
            this.metrics = metrics;
            this.threads = threadDump.threads;
            this.updatingMetrics = false;
            this.changeDetector.detectChanges();
        }))
            .subscribe();
    }
    metricsKeyExists(key) {
        return this.metrics && this.metrics[key];
    }
    metricsKeyExistsAndObjectNotEmpty(key) {
        return this.metrics && this.metrics[key] && JSON.stringify(this.metrics[key]) !== '{}';
    }
};
MetricsComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-metrics',
        template: require('./metrics.component.html'),
        changeDetection: core_1.ChangeDetectionStrategy.OnPush
    }),
    tslib_1.__metadata("design:paramtypes", [metrics_service_1.MetricsService, core_1.ChangeDetectorRef])
], MetricsComponent);
exports.MetricsComponent = MetricsComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBOEY7QUFDOUYsOENBQXlDO0FBRXpDLHVEQUE0RjtBQU81RixJQUFhLGdCQUFnQixHQUE3QixNQUFhLGdCQUFnQjtJQUszQixZQUFvQixjQUE4QixFQUFVLGNBQWlDO1FBQXpFLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUY3RixvQkFBZSxHQUFHLElBQUksQ0FBQztJQUV5RSxDQUFDO0lBRWpHLFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsY0FBYzthQUNoQixVQUFVLEVBQUU7YUFDWixJQUFJLENBQ0gsbUJBQU8sQ0FDTCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxFQUN0QyxDQUFDLE9BQWdCLEVBQUUsVUFBc0IsRUFBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQztZQUNsQyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RDLENBQUMsQ0FDRixDQUNGO2FBQ0EsU0FBUyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQUVELGdCQUFnQixDQUFDLEdBQWU7UUFDOUIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELGlDQUFpQyxDQUFDLEdBQWU7UUFDL0MsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDO0lBQ3pGLENBQUM7Q0FDRixDQUFBO0FBcENZLGdCQUFnQjtJQUw1QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGFBQWE7UUFDdkIsa0JBQWEsMEJBQTBCLENBQUE7UUFDdkMsZUFBZSxFQUFFLDhCQUF1QixDQUFDLE1BQU07S0FDaEQsQ0FBQzs2Q0FNb0MsZ0NBQWMsRUFBMEIsd0JBQWlCO0dBTGxGLGdCQUFnQixDQW9DNUI7QUFwQ1ksNENBQWdCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FkbWluL21ldHJpY3MvbWV0cmljcy5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgZmxhdE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuaW1wb3J0IHsgTWV0cmljc1NlcnZpY2UsIE1ldHJpY3MsIE1ldHJpY3NLZXksIFRocmVhZER1bXAsIFRocmVhZCB9IGZyb20gJy4vbWV0cmljcy5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLW1ldHJpY3MnLFxuICB0ZW1wbGF0ZVVybDogJy4vbWV0cmljcy5jb21wb25lbnQuaHRtbCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIE1ldHJpY3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBtZXRyaWNzPzogTWV0cmljcztcbiAgdGhyZWFkcz86IFRocmVhZFtdO1xuICB1cGRhdGluZ01ldHJpY3MgPSB0cnVlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbWV0cmljc1NlcnZpY2U6IE1ldHJpY3NTZXJ2aWNlLCBwcml2YXRlIGNoYW5nZURldGVjdG9yOiBDaGFuZ2VEZXRlY3RvclJlZikge31cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLnJlZnJlc2goKTtcbiAgfVxuXG4gIHJlZnJlc2goKTogdm9pZCB7XG4gICAgdGhpcy51cGRhdGluZ01ldHJpY3MgPSB0cnVlO1xuICAgIHRoaXMubWV0cmljc1NlcnZpY2VcbiAgICAgIC5nZXRNZXRyaWNzKClcbiAgICAgIC5waXBlKFxuICAgICAgICBmbGF0TWFwKFxuICAgICAgICAgICgpID0+IHRoaXMubWV0cmljc1NlcnZpY2UudGhyZWFkRHVtcCgpLFxuICAgICAgICAgIChtZXRyaWNzOiBNZXRyaWNzLCB0aHJlYWREdW1wOiBUaHJlYWREdW1wKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm1ldHJpY3MgPSBtZXRyaWNzO1xuICAgICAgICAgICAgdGhpcy50aHJlYWRzID0gdGhyZWFkRHVtcC50aHJlYWRzO1xuICAgICAgICAgICAgdGhpcy51cGRhdGluZ01ldHJpY3MgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlRGV0ZWN0b3IuZGV0ZWN0Q2hhbmdlcygpO1xuICAgICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgKVxuICAgICAgLnN1YnNjcmliZSgpO1xuICB9XG5cbiAgbWV0cmljc0tleUV4aXN0cyhrZXk6IE1ldHJpY3NLZXkpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5tZXRyaWNzICYmIHRoaXMubWV0cmljc1trZXldO1xuICB9XG5cbiAgbWV0cmljc0tleUV4aXN0c0FuZE9iamVjdE5vdEVtcHR5KGtleTogTWV0cmljc0tleSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLm1ldHJpY3MgJiYgdGhpcy5tZXRyaWNzW2tleV0gJiYgSlNPTi5zdHJpbmdpZnkodGhpcy5tZXRyaWNzW2tleV0pICE9PSAne30nO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=