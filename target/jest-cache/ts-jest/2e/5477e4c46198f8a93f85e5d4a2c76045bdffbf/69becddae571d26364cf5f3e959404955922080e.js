"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const moment = require("moment");
const input_constants_1 = require("app/shared/constants/input.constants");
const course_model_1 = require("app/shared/model/course.model");
const course_service_1 = require("./course.service");
const restaurant_service_1 = require("app/entities/restaurant/restaurant.service");
let CourseUpdateComponent = class CourseUpdateComponent {
    constructor(courseService, restaurantService, activatedRoute, fb) {
        this.courseService = courseService;
        this.restaurantService = restaurantService;
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.isSaving = false;
        this.restaurants = [];
        this.editForm = this.fb.group({
            id: [],
            state: [null, [forms_1.Validators.required]],
            deliveryTime: [null, [forms_1.Validators.required]],
            restaurant: []
        });
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ course }) => {
            if (!course.id) {
                const today = moment().startOf('day');
                course.deliveryTime = today;
            }
            this.updateForm(course);
            this.restaurantService.query().subscribe((res) => (this.restaurants = res.body || []));
        });
    }
    updateForm(course) {
        this.editForm.patchValue({
            id: course.id,
            state: course.state,
            deliveryTime: course.deliveryTime ? course.deliveryTime.format(input_constants_1.DATE_TIME_FORMAT) : null,
            restaurant: course.restaurant
        });
    }
    previousState() {
        window.history.back();
    }
    save() {
        this.isSaving = true;
        const course = this.createFromForm();
        if (course.id !== undefined) {
            this.subscribeToSaveResponse(this.courseService.update(course));
        }
        else {
            this.subscribeToSaveResponse(this.courseService.create(course));
        }
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new course_model_1.Course()), { id: this.editForm.get(['id']).value, state: this.editForm.get(['state']).value, deliveryTime: this.editForm.get(['deliveryTime']).value
                ? moment(this.editForm.get(['deliveryTime']).value, input_constants_1.DATE_TIME_FORMAT)
                : undefined, restaurant: this.editForm.get(['restaurant']).value });
    }
    subscribeToSaveResponse(result) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
    }
    trackById(index, item) {
        return item.id;
    }
};
CourseUpdateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-course-update',
        template: require('./course-update.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [course_service_1.CourseService,
        restaurant_service_1.RestaurantService,
        router_1.ActivatedRoute,
        forms_1.FormBuilder])
], CourseUpdateComponent);
exports.CourseUpdateComponent = CourseUpdateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvY291cnNlL2NvdXJzZS11cGRhdGUuY29tcG9uZW50LnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUFrRDtBQUVsRCw2REFBNkQ7QUFDN0QsMENBQXlEO0FBQ3pELDRDQUFpRDtBQUVqRCxpQ0FBaUM7QUFDakMsMEVBQXdFO0FBRXhFLGdFQUFnRTtBQUNoRSxxREFBaUQ7QUFFakQsbUZBQStFO0FBTS9FLElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBV2hDLFlBQ1ksYUFBNEIsRUFDNUIsaUJBQW9DLEVBQ3BDLGNBQThCLEVBQ2hDLEVBQWU7UUFIYixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUNoQyxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBZHpCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsZ0JBQVcsR0FBa0IsRUFBRSxDQUFDO1FBRWhDLGFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUN2QixFQUFFLEVBQUUsRUFBRTtZQUNOLEtBQUssRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEMsWUFBWSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxVQUFVLEVBQUUsRUFBRTtTQUNmLENBQUMsQ0FBQztJQU9BLENBQUM7SUFFSixRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFO1lBQ2hELElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFO2dCQUNkLE1BQU0sS0FBSyxHQUFHLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7YUFDN0I7WUFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXhCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFnQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3RILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVUsQ0FBQyxNQUFlO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO1lBQ3ZCLEVBQUUsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNiLEtBQUssRUFBRSxNQUFNLENBQUMsS0FBSztZQUNuQixZQUFZLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsa0NBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN2RixVQUFVLEVBQUUsTUFBTSxDQUFDLFVBQVU7U0FDOUIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGFBQWE7UUFDWCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JDLElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7WUFDM0IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDakU7YUFBTTtZQUNMLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ2pFO0lBQ0gsQ0FBQztJQUVPLGNBQWM7UUFDcEIsdUNBQ0ssSUFBSSxxQkFBTSxFQUFFLEtBQ2YsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUUsQ0FBQyxLQUFLLEVBQ3BDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUMxQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBRSxDQUFDLEtBQUs7Z0JBQ3RELENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFBRSxrQ0FBZ0IsQ0FBQztnQkFDdEUsQ0FBQyxDQUFDLFNBQVMsRUFDYixVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBRSxDQUFDLEtBQUssSUFDcEQ7SUFDSixDQUFDO0lBRVMsdUJBQXVCLENBQUMsTUFBeUM7UUFDekUsTUFBTSxDQUFDLFNBQVMsQ0FDZCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQzFCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDekIsQ0FBQztJQUNKLENBQUM7SUFFUyxhQUFhO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQWEsRUFBRSxJQUFpQjtRQUN4QyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQztDQUNGLENBQUE7QUFyRlkscUJBQXFCO0lBSmpDLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLGtCQUFhLGdDQUFnQyxDQUFBO0tBQzlDLENBQUM7NkNBYTJCLDhCQUFhO1FBQ1Qsc0NBQWlCO1FBQ3BCLHVCQUFjO1FBQzVCLG1CQUFXO0dBZmQscUJBQXFCLENBcUZqQztBQXJGWSxzREFBcUIiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvY291cnNlL2NvdXJzZS11cGRhdGUuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVudXNlZC12YXJzXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xuaW1wb3J0IHsgREFURV9USU1FX0ZPUk1BVCB9IGZyb20gJ2FwcC9zaGFyZWQvY29uc3RhbnRzL2lucHV0LmNvbnN0YW50cyc7XG5cbmltcG9ydCB7IElDb3Vyc2UsIENvdXJzZSB9IGZyb20gJ2FwcC9zaGFyZWQvbW9kZWwvY291cnNlLm1vZGVsJztcbmltcG9ydCB7IENvdXJzZVNlcnZpY2UgfSBmcm9tICcuL2NvdXJzZS5zZXJ2aWNlJztcbmltcG9ydCB7IElSZXN0YXVyYW50IH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9yZXN0YXVyYW50Lm1vZGVsJztcbmltcG9ydCB7IFJlc3RhdXJhbnRTZXJ2aWNlIH0gZnJvbSAnYXBwL2VudGl0aWVzL3Jlc3RhdXJhbnQvcmVzdGF1cmFudC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLWNvdXJzZS11cGRhdGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vY291cnNlLXVwZGF0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQ291cnNlVXBkYXRlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgaXNTYXZpbmcgPSBmYWxzZTtcbiAgcmVzdGF1cmFudHM6IElSZXN0YXVyYW50W10gPSBbXTtcblxuICBlZGl0Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgIGlkOiBbXSxcbiAgICBzdGF0ZTogW251bGwsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXV0sXG4gICAgZGVsaXZlcnlUaW1lOiBbbnVsbCwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdXSxcbiAgICByZXN0YXVyYW50OiBbXVxuICB9KTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgY291cnNlU2VydmljZTogQ291cnNlU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcmVzdGF1cmFudFNlcnZpY2U6IFJlc3RhdXJhbnRTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXJcbiAgKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUuZGF0YS5zdWJzY3JpYmUoKHsgY291cnNlIH0pID0+IHtcbiAgICAgIGlmICghY291cnNlLmlkKSB7XG4gICAgICAgIGNvbnN0IHRvZGF5ID0gbW9tZW50KCkuc3RhcnRPZignZGF5Jyk7XG4gICAgICAgIGNvdXJzZS5kZWxpdmVyeVRpbWUgPSB0b2RheTtcbiAgICAgIH1cblxuICAgICAgdGhpcy51cGRhdGVGb3JtKGNvdXJzZSk7XG5cbiAgICAgIHRoaXMucmVzdGF1cmFudFNlcnZpY2UucXVlcnkoKS5zdWJzY3JpYmUoKHJlczogSHR0cFJlc3BvbnNlPElSZXN0YXVyYW50W10+KSA9PiAodGhpcy5yZXN0YXVyYW50cyA9IHJlcy5ib2R5IHx8IFtdKSk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVGb3JtKGNvdXJzZTogSUNvdXJzZSk6IHZvaWQge1xuICAgIHRoaXMuZWRpdEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICBpZDogY291cnNlLmlkLFxuICAgICAgc3RhdGU6IGNvdXJzZS5zdGF0ZSxcbiAgICAgIGRlbGl2ZXJ5VGltZTogY291cnNlLmRlbGl2ZXJ5VGltZSA/IGNvdXJzZS5kZWxpdmVyeVRpbWUuZm9ybWF0KERBVEVfVElNRV9GT1JNQVQpIDogbnVsbCxcbiAgICAgIHJlc3RhdXJhbnQ6IGNvdXJzZS5yZXN0YXVyYW50XG4gICAgfSk7XG4gIH1cblxuICBwcmV2aW91c1N0YXRlKCk6IHZvaWQge1xuICAgIHdpbmRvdy5oaXN0b3J5LmJhY2soKTtcbiAgfVxuXG4gIHNhdmUoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IHRydWU7XG4gICAgY29uc3QgY291cnNlID0gdGhpcy5jcmVhdGVGcm9tRm9ybSgpO1xuICAgIGlmIChjb3Vyc2UuaWQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5zdWJzY3JpYmVUb1NhdmVSZXNwb25zZSh0aGlzLmNvdXJzZVNlcnZpY2UudXBkYXRlKGNvdXJzZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHRoaXMuY291cnNlU2VydmljZS5jcmVhdGUoY291cnNlKSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVGcm9tRm9ybSgpOiBJQ291cnNlIHtcbiAgICByZXR1cm4ge1xuICAgICAgLi4ubmV3IENvdXJzZSgpLFxuICAgICAgaWQ6IHRoaXMuZWRpdEZvcm0uZ2V0KFsnaWQnXSkhLnZhbHVlLFxuICAgICAgc3RhdGU6IHRoaXMuZWRpdEZvcm0uZ2V0KFsnc3RhdGUnXSkhLnZhbHVlLFxuICAgICAgZGVsaXZlcnlUaW1lOiB0aGlzLmVkaXRGb3JtLmdldChbJ2RlbGl2ZXJ5VGltZSddKSEudmFsdWVcbiAgICAgICAgPyBtb21lbnQodGhpcy5lZGl0Rm9ybS5nZXQoWydkZWxpdmVyeVRpbWUnXSkhLnZhbHVlLCBEQVRFX1RJTUVfRk9STUFUKVxuICAgICAgICA6IHVuZGVmaW5lZCxcbiAgICAgIHJlc3RhdXJhbnQ6IHRoaXMuZWRpdEZvcm0uZ2V0KFsncmVzdGF1cmFudCddKSEudmFsdWVcbiAgICB9O1xuICB9XG5cbiAgcHJvdGVjdGVkIHN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHJlc3VsdDogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8SUNvdXJzZT4+KTogdm9pZCB7XG4gICAgcmVzdWx0LnN1YnNjcmliZShcbiAgICAgICgpID0+IHRoaXMub25TYXZlU3VjY2VzcygpLFxuICAgICAgKCkgPT4gdGhpcy5vblNhdmVFcnJvcigpXG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvblNhdmVTdWNjZXNzKCk6IHZvaWQge1xuICAgIHRoaXMuaXNTYXZpbmcgPSBmYWxzZTtcbiAgICB0aGlzLnByZXZpb3VzU3RhdGUoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvblNhdmVFcnJvcigpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gZmFsc2U7XG4gIH1cblxuICB0cmFja0J5SWQoaW5kZXg6IG51bWJlciwgaXRlbTogSVJlc3RhdXJhbnQpOiBhbnkge1xuICAgIHJldHVybiBpdGVtLmlkO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=