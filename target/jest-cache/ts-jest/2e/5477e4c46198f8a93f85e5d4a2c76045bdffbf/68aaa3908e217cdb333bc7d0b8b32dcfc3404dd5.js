"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const login_modal_service_1 = require("app/core/login/login-modal.service");
const password_reset_finish_service_1 = require("./password-reset-finish.service");
let PasswordResetFinishComponent = class PasswordResetFinishComponent {
    constructor(passwordResetFinishService, loginModalService, route, fb) {
        this.passwordResetFinishService = passwordResetFinishService;
        this.loginModalService = loginModalService;
        this.route = route;
        this.fb = fb;
        this.initialized = false;
        this.doNotMatch = false;
        this.error = false;
        this.success = false;
        this.key = '';
        this.passwordForm = this.fb.group({
            newPassword: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]],
            confirmPassword: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]]
        });
    }
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (params['key']) {
                this.key = params['key'];
            }
            this.initialized = true;
        });
    }
    ngAfterViewInit() {
        if (this.newPassword) {
            this.newPassword.nativeElement.focus();
        }
    }
    finishReset() {
        this.doNotMatch = false;
        this.error = false;
        const newPassword = this.passwordForm.get(['newPassword']).value;
        const confirmPassword = this.passwordForm.get(['confirmPassword']).value;
        if (newPassword !== confirmPassword) {
            this.doNotMatch = true;
        }
        else {
            this.passwordResetFinishService.save(this.key, newPassword).subscribe(() => (this.success = true), () => (this.error = true));
        }
    }
    login() {
        this.loginModalService.open();
    }
};
tslib_1.__decorate([
    core_1.ViewChild('newPassword', { static: false }),
    tslib_1.__metadata("design:type", core_1.ElementRef)
], PasswordResetFinishComponent.prototype, "newPassword", void 0);
PasswordResetFinishComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-password-reset-finish',
        template: require('./password-reset-finish.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [password_reset_finish_service_1.PasswordResetFinishService,
        login_modal_service_1.LoginModalService,
        router_1.ActivatedRoute,
        forms_1.FormBuilder])
], PasswordResetFinishComponent);
exports.PasswordResetFinishComponent = PasswordResetFinishComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9maW5pc2gvcGFzc3dvcmQtcmVzZXQtZmluaXNoLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBd0Y7QUFDeEYsMENBQXlEO0FBQ3pELDRDQUFpRDtBQUVqRCw0RUFBdUU7QUFDdkUsbUZBQTZFO0FBTTdFLElBQWEsNEJBQTRCLEdBQXpDLE1BQWEsNEJBQTRCO0lBZXZDLFlBQ1UsMEJBQXNELEVBQ3RELGlCQUFvQyxFQUNwQyxLQUFxQixFQUNyQixFQUFlO1FBSGYsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLE9BQUUsR0FBRixFQUFFLENBQWE7UUFmekIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2QsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixRQUFHLEdBQUcsRUFBRSxDQUFDO1FBRVQsaUJBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUMzQixXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNGLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEcsQ0FBQyxDQUFDO0lBT0EsQ0FBQztJQUVKLFFBQVE7UUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzFCO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN4QztJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFFbkIsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBRSxDQUFDLEtBQUssQ0FBQztRQUNsRSxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUUsQ0FBQyxLQUFLLENBQUM7UUFFMUUsSUFBSSxXQUFXLEtBQUssZUFBZSxFQUFFO1lBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO2FBQU07WUFDTCxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUNuRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQzNCLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FDMUIsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEMsQ0FBQztDQUNGLENBQUE7QUF2REM7SUFEQyxnQkFBUyxDQUFDLGFBQWEsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztzQ0FDOUIsaUJBQVU7aUVBQUM7QUFGZCw0QkFBNEI7SUFKeEMsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSwyQkFBMkI7UUFDckMsa0JBQWEsd0NBQXdDLENBQUE7S0FDdEQsQ0FBQzs2Q0FpQnNDLDBEQUEwQjtRQUNuQyx1Q0FBaUI7UUFDN0IsdUJBQWM7UUFDakIsbUJBQVc7R0FuQmQsNEJBQTRCLENBeUR4QztBQXpEWSxvRUFBNEIiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9maW5pc2gvcGFzc3dvcmQtcmVzZXQtZmluaXNoLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgRWxlbWVudFJlZiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHsgTG9naW5Nb2RhbFNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS9sb2dpbi9sb2dpbi1tb2RhbC5zZXJ2aWNlJztcbmltcG9ydCB7IFBhc3N3b3JkUmVzZXRGaW5pc2hTZXJ2aWNlIH0gZnJvbSAnLi9wYXNzd29yZC1yZXNldC1maW5pc2guc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1wYXNzd29yZC1yZXNldC1maW5pc2gnLFxuICB0ZW1wbGF0ZVVybDogJy4vcGFzc3dvcmQtcmVzZXQtZmluaXNoLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBQYXNzd29yZFJlc2V0RmluaXNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcbiAgQFZpZXdDaGlsZCgnbmV3UGFzc3dvcmQnLCB7IHN0YXRpYzogZmFsc2UgfSlcbiAgbmV3UGFzc3dvcmQ/OiBFbGVtZW50UmVmO1xuXG4gIGluaXRpYWxpemVkID0gZmFsc2U7XG4gIGRvTm90TWF0Y2ggPSBmYWxzZTtcbiAgZXJyb3IgPSBmYWxzZTtcbiAgc3VjY2VzcyA9IGZhbHNlO1xuICBrZXkgPSAnJztcblxuICBwYXNzd29yZEZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICBuZXdQYXNzd29yZDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoNCksIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV1dLFxuICAgIGNvbmZpcm1QYXNzd29yZDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoNCksIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV1dXG4gIH0pO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcGFzc3dvcmRSZXNldEZpbmlzaFNlcnZpY2U6IFBhc3N3b3JkUmVzZXRGaW5pc2hTZXJ2aWNlLFxuICAgIHByaXZhdGUgbG9naW5Nb2RhbFNlcnZpY2U6IExvZ2luTW9kYWxTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyXG4gICkge31cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLnJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgaWYgKHBhcmFtc1sna2V5J10pIHtcbiAgICAgICAgdGhpcy5rZXkgPSBwYXJhbXNbJ2tleSddO1xuICAgICAgfVxuICAgICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XG4gICAgfSk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMubmV3UGFzc3dvcmQpIHtcbiAgICAgIHRoaXMubmV3UGFzc3dvcmQubmF0aXZlRWxlbWVudC5mb2N1cygpO1xuICAgIH1cbiAgfVxuXG4gIGZpbmlzaFJlc2V0KCk6IHZvaWQge1xuICAgIHRoaXMuZG9Ob3RNYXRjaCA9IGZhbHNlO1xuICAgIHRoaXMuZXJyb3IgPSBmYWxzZTtcblxuICAgIGNvbnN0IG5ld1Bhc3N3b3JkID0gdGhpcy5wYXNzd29yZEZvcm0uZ2V0KFsnbmV3UGFzc3dvcmQnXSkhLnZhbHVlO1xuICAgIGNvbnN0IGNvbmZpcm1QYXNzd29yZCA9IHRoaXMucGFzc3dvcmRGb3JtLmdldChbJ2NvbmZpcm1QYXNzd29yZCddKSEudmFsdWU7XG5cbiAgICBpZiAobmV3UGFzc3dvcmQgIT09IGNvbmZpcm1QYXNzd29yZCkge1xuICAgICAgdGhpcy5kb05vdE1hdGNoID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wYXNzd29yZFJlc2V0RmluaXNoU2VydmljZS5zYXZlKHRoaXMua2V5LCBuZXdQYXNzd29yZCkuc3Vic2NyaWJlKFxuICAgICAgICAoKSA9PiAodGhpcy5zdWNjZXNzID0gdHJ1ZSksXG4gICAgICAgICgpID0+ICh0aGlzLmVycm9yID0gdHJ1ZSlcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgbG9naW4oKTogdm9pZCB7XG4gICAgdGhpcy5sb2dpbk1vZGFsU2VydmljZS5vcGVuKCk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==