"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const account_service_1 = require("app/core/auth/account.service");
const password_service_1 = require("./password.service");
let PasswordComponent = class PasswordComponent {
    constructor(passwordService, accountService, fb) {
        this.passwordService = passwordService;
        this.accountService = accountService;
        this.fb = fb;
        this.doNotMatch = false;
        this.error = false;
        this.success = false;
        this.passwordForm = this.fb.group({
            currentPassword: ['', [forms_1.Validators.required]],
            newPassword: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]],
            confirmPassword: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]]
        });
    }
    ngOnInit() {
        this.account$ = this.accountService.identity();
    }
    changePassword() {
        this.error = false;
        this.success = false;
        this.doNotMatch = false;
        const newPassword = this.passwordForm.get(['newPassword']).value;
        if (newPassword !== this.passwordForm.get(['confirmPassword']).value) {
            this.doNotMatch = true;
        }
        else {
            this.passwordService.save(newPassword, this.passwordForm.get(['currentPassword']).value).subscribe(() => (this.success = true), () => (this.error = true));
        }
    }
};
PasswordComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-password',
        template: require('./password.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [password_service_1.PasswordService, account_service_1.AccountService, forms_1.FormBuilder])
], PasswordComponent);
exports.PasswordComponent = PasswordComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC9wYXNzd29yZC5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBQ2xELDBDQUF5RDtBQUd6RCxtRUFBK0Q7QUFFL0QseURBQXFEO0FBTXJELElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBVzVCLFlBQW9CLGVBQWdDLEVBQVUsY0FBOEIsRUFBVSxFQUFlO1FBQWpHLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLE9BQUUsR0FBRixFQUFFLENBQWE7UUFWckgsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2QsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUVoQixpQkFBWSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzNCLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMzRixlQUFlLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hHLENBQUMsQ0FBQztJQUVxSCxDQUFDO0lBRXpILFFBQVE7UUFDTixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDakQsQ0FBQztJQUVELGNBQWM7UUFDWixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUV4QixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFFLENBQUMsS0FBSyxDQUFDO1FBQ2xFLElBQUksV0FBVyxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFBRTtZQUNyRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztTQUN4QjthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FDakcsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxFQUMzQixHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQzFCLENBQUM7U0FDSDtJQUNILENBQUM7Q0FDRixDQUFBO0FBaENZLGlCQUFpQjtJQUo3QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7UUFDeEIsa0JBQWEsMkJBQTJCLENBQUE7S0FDekMsQ0FBQzs2Q0FZcUMsa0NBQWUsRUFBMEIsZ0NBQWMsRUFBYyxtQkFBVztHQVgxRyxpQkFBaUIsQ0FnQzdCO0FBaENZLDhDQUFpQiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9hY2NvdW50L3Bhc3N3b3JkL3Bhc3N3b3JkLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9hY2NvdW50LnNlcnZpY2UnO1xuaW1wb3J0IHsgQWNjb3VudCB9IGZyb20gJ2FwcC9jb3JlL3VzZXIvYWNjb3VudC5tb2RlbCc7XG5pbXBvcnQgeyBQYXNzd29yZFNlcnZpY2UgfSBmcm9tICcuL3Bhc3N3b3JkLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktcGFzc3dvcmQnLFxuICB0ZW1wbGF0ZVVybDogJy4vcGFzc3dvcmQuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFBhc3N3b3JkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgZG9Ob3RNYXRjaCA9IGZhbHNlO1xuICBlcnJvciA9IGZhbHNlO1xuICBzdWNjZXNzID0gZmFsc2U7XG4gIGFjY291bnQkPzogT2JzZXJ2YWJsZTxBY2NvdW50IHwgbnVsbD47XG4gIHBhc3N3b3JkRm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgIGN1cnJlbnRQYXNzd29yZDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF1dLFxuICAgIG5ld1Bhc3N3b3JkOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg0KSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXV0sXG4gICAgY29uZmlybVBhc3N3b3JkOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg0KSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXV1cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYXNzd29yZFNlcnZpY2U6IFBhc3N3b3JkU2VydmljZSwgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYWNjb3VudCQgPSB0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KCk7XG4gIH1cblxuICBjaGFuZ2VQYXNzd29yZCgpOiB2b2lkIHtcbiAgICB0aGlzLmVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5zdWNjZXNzID0gZmFsc2U7XG4gICAgdGhpcy5kb05vdE1hdGNoID0gZmFsc2U7XG5cbiAgICBjb25zdCBuZXdQYXNzd29yZCA9IHRoaXMucGFzc3dvcmRGb3JtLmdldChbJ25ld1Bhc3N3b3JkJ10pIS52YWx1ZTtcbiAgICBpZiAobmV3UGFzc3dvcmQgIT09IHRoaXMucGFzc3dvcmRGb3JtLmdldChbJ2NvbmZpcm1QYXNzd29yZCddKSEudmFsdWUpIHtcbiAgICAgIHRoaXMuZG9Ob3RNYXRjaCA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucGFzc3dvcmRTZXJ2aWNlLnNhdmUobmV3UGFzc3dvcmQsIHRoaXMucGFzc3dvcmRGb3JtLmdldChbJ2N1cnJlbnRQYXNzd29yZCddKSEudmFsdWUpLnN1YnNjcmliZShcbiAgICAgICAgKCkgPT4gKHRoaXMuc3VjY2VzcyA9IHRydWUpLFxuICAgICAgICAoKSA9PiAodGhpcy5lcnJvciA9IHRydWUpXG4gICAgICApO1xuICAgIH1cbiAgfVxufVxuIl0sInZlcnNpb24iOjN9