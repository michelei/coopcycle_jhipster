"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const http_1 = require("@angular/common/http");
const forms_1 = require("@angular/forms");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const password_component_1 = require("app/account/password/password.component");
const password_service_1 = require("app/account/password/password.service");
describe('Component Tests', () => {
    describe('PasswordComponent', () => {
        let comp;
        let fixture;
        let service;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [password_component_1.PasswordComponent],
                providers: [forms_1.FormBuilder]
            })
                .overrideTemplate(password_component_1.PasswordComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(password_component_1.PasswordComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(password_service_1.PasswordService);
        });
        it('should show error if passwords do not match', () => {
            // GIVEN
            comp.passwordForm.patchValue({
                newPassword: 'password1',
                confirmPassword: 'password2'
            });
            // WHEN
            comp.changePassword();
            // THEN
            expect(comp.doNotMatch).toBe(true);
            expect(comp.error).toBe(false);
            expect(comp.success).toBe(false);
        });
        it('should call Auth.changePassword when passwords match', () => {
            // GIVEN
            const passwordValues = {
                currentPassword: 'oldPassword',
                newPassword: 'myPassword'
            };
            spyOn(service, 'save').and.returnValue(rxjs_1.of(new http_1.HttpResponse({ body: true })));
            comp.passwordForm.patchValue({
                currentPassword: passwordValues.currentPassword,
                newPassword: passwordValues.newPassword,
                confirmPassword: passwordValues.newPassword
            });
            // WHEN
            comp.changePassword();
            // THEN
            expect(service.save).toHaveBeenCalledWith(passwordValues.newPassword, passwordValues.currentPassword);
        });
        it('should set success to true upon success', () => {
            // GIVEN
            spyOn(service, 'save').and.returnValue(rxjs_1.of(new http_1.HttpResponse({ body: true })));
            comp.passwordForm.patchValue({
                newPassword: 'myPassword',
                confirmPassword: 'myPassword'
            });
            // WHEN
            comp.changePassword();
            // THEN
            expect(comp.doNotMatch).toBe(false);
            expect(comp.error).toBe(false);
            expect(comp.success).toBe(true);
        });
        it('should notify of error if change password fails', () => {
            // GIVEN
            spyOn(service, 'save').and.returnValue(rxjs_1.throwError('ERROR'));
            comp.passwordForm.patchValue({
                newPassword: 'myPassword',
                confirmPassword: 'myPassword'
            });
            // WHEN
            comp.changePassword();
            // THEN
            expect(comp.doNotMatch).toBe(false);
            expect(comp.success).toBe(false);
            expect(comp.error).toBe(true);
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9wYXNzd29yZC9wYXNzd29yZC5jb21wb25lbnQuc3BlYy50cyIsIm1hcHBpbmdzIjoiOztBQUFBLG1EQUF5RTtBQUN6RSwrQ0FBb0Q7QUFDcEQsMENBQTZDO0FBQzdDLCtCQUFzQztBQUV0QyxzREFBMkQ7QUFDM0QsZ0ZBQTRFO0FBQzVFLDRFQUF3RTtBQUV4RSxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLEVBQUU7UUFDakMsSUFBSSxJQUF1QixDQUFDO1FBQzVCLElBQUksT0FBNEMsQ0FBQztRQUNqRCxJQUFJLE9BQXdCLENBQUM7UUFFN0IsVUFBVSxDQUFDLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDcEIsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQW1CLENBQUM7Z0JBQzlCLFlBQVksRUFBRSxDQUFDLHNDQUFpQixDQUFDO2dCQUNqQyxTQUFTLEVBQUUsQ0FBQyxtQkFBVyxDQUFDO2FBQ3pCLENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMsc0NBQWlCLEVBQUUsRUFBRSxDQUFDO2lCQUN2QyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLHNDQUFpQixDQUFDLENBQUM7WUFDckQsSUFBSSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztZQUNqQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGtDQUFlLENBQUMsQ0FBQztRQUMvRCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyw2Q0FBNkMsRUFBRSxHQUFHLEVBQUU7WUFDckQsUUFBUTtZQUNSLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixXQUFXLEVBQUUsV0FBVztnQkFDeEIsZUFBZSxFQUFFLFdBQVc7YUFDN0IsQ0FBQyxDQUFDO1lBQ0gsT0FBTztZQUNQLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixPQUFPO1lBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsc0RBQXNELEVBQUUsR0FBRyxFQUFFO1lBQzlELFFBQVE7WUFDUixNQUFNLGNBQWMsR0FBRztnQkFDckIsZUFBZSxFQUFFLGFBQWE7Z0JBQzlCLFdBQVcsRUFBRSxZQUFZO2FBQzFCLENBQUM7WUFFRixLQUFLLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsU0FBRSxDQUFDLElBQUksbUJBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUU3RSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztnQkFDM0IsZUFBZSxFQUFFLGNBQWMsQ0FBQyxlQUFlO2dCQUMvQyxXQUFXLEVBQUUsY0FBYyxDQUFDLFdBQVc7Z0JBQ3ZDLGVBQWUsRUFBRSxjQUFjLENBQUMsV0FBVzthQUM1QyxDQUFDLENBQUM7WUFFSCxPQUFPO1lBQ1AsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRXRCLE9BQU87WUFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hHLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHlDQUF5QyxFQUFFLEdBQUcsRUFBRTtZQUNqRCxRQUFRO1lBQ1IsS0FBSyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsQ0FBQyxJQUFJLG1CQUFZLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7Z0JBQzNCLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixlQUFlLEVBQUUsWUFBWTthQUM5QixDQUFDLENBQUM7WUFFSCxPQUFPO1lBQ1AsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRXRCLE9BQU87WUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpREFBaUQsRUFBRSxHQUFHLEVBQUU7WUFDekQsUUFBUTtZQUNSLEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxpQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7Z0JBQzNCLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixlQUFlLEVBQUUsWUFBWTthQUM5QixDQUFDLENBQUM7WUFFSCxPQUFPO1lBQ1AsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRXRCLE9BQU87WUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9wYXNzd29yZC9wYXNzd29yZC5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRGaXh0dXJlLCBUZXN0QmVkLCBhc3luYyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IENvb3BjeWNsZVRlc3RNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi90ZXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBQYXNzd29yZENvbXBvbmVudCB9IGZyb20gJ2FwcC9hY2NvdW50L3Bhc3N3b3JkL3Bhc3N3b3JkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQYXNzd29yZFNlcnZpY2UgfSBmcm9tICdhcHAvYWNjb3VudC9wYXNzd29yZC9wYXNzd29yZC5zZXJ2aWNlJztcblxuZGVzY3JpYmUoJ0NvbXBvbmVudCBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ1Bhc3N3b3JkQ29tcG9uZW50JywgKCkgPT4ge1xuICAgIGxldCBjb21wOiBQYXNzd29yZENvbXBvbmVudDtcbiAgICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxQYXNzd29yZENvbXBvbmVudD47XG4gICAgbGV0IHNlcnZpY2U6IFBhc3N3b3JkU2VydmljZTtcblxuICAgIGJlZm9yZUVhY2goYXN5bmMoKCkgPT4ge1xuICAgICAgVGVzdEJlZC5jb25maWd1cmVUZXN0aW5nTW9kdWxlKHtcbiAgICAgICAgaW1wb3J0czogW0Nvb3BjeWNsZVRlc3RNb2R1bGVdLFxuICAgICAgICBkZWNsYXJhdGlvbnM6IFtQYXNzd29yZENvbXBvbmVudF0sXG4gICAgICAgIHByb3ZpZGVyczogW0Zvcm1CdWlsZGVyXVxuICAgICAgfSlcbiAgICAgICAgLm92ZXJyaWRlVGVtcGxhdGUoUGFzc3dvcmRDb21wb25lbnQsICcnKVxuICAgICAgICAuY29tcGlsZUNvbXBvbmVudHMoKTtcbiAgICB9KSk7XG5cbiAgICBiZWZvcmVFYWNoKCgpID0+IHtcbiAgICAgIGZpeHR1cmUgPSBUZXN0QmVkLmNyZWF0ZUNvbXBvbmVudChQYXNzd29yZENvbXBvbmVudCk7XG4gICAgICBjb21wID0gZml4dHVyZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICAgIHNlcnZpY2UgPSBmaXh0dXJlLmRlYnVnRWxlbWVudC5pbmplY3Rvci5nZXQoUGFzc3dvcmRTZXJ2aWNlKTtcbiAgICB9KTtcblxuICAgIGl0KCdzaG91bGQgc2hvdyBlcnJvciBpZiBwYXNzd29yZHMgZG8gbm90IG1hdGNoJywgKCkgPT4ge1xuICAgICAgLy8gR0lWRU5cbiAgICAgIGNvbXAucGFzc3dvcmRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgICBuZXdQYXNzd29yZDogJ3Bhc3N3b3JkMScsXG4gICAgICAgIGNvbmZpcm1QYXNzd29yZDogJ3Bhc3N3b3JkMidcbiAgICAgIH0pO1xuICAgICAgLy8gV0hFTlxuICAgICAgY29tcC5jaGFuZ2VQYXNzd29yZCgpO1xuICAgICAgLy8gVEhFTlxuICAgICAgZXhwZWN0KGNvbXAuZG9Ob3RNYXRjaCkudG9CZSh0cnVlKTtcbiAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKGZhbHNlKTtcbiAgICAgIGV4cGVjdChjb21wLnN1Y2Nlc3MpLnRvQmUoZmFsc2UpO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBjYWxsIEF1dGguY2hhbmdlUGFzc3dvcmQgd2hlbiBwYXNzd29yZHMgbWF0Y2gnLCAoKSA9PiB7XG4gICAgICAvLyBHSVZFTlxuICAgICAgY29uc3QgcGFzc3dvcmRWYWx1ZXMgPSB7XG4gICAgICAgIGN1cnJlbnRQYXNzd29yZDogJ29sZFBhc3N3b3JkJyxcbiAgICAgICAgbmV3UGFzc3dvcmQ6ICdteVBhc3N3b3JkJ1xuICAgICAgfTtcblxuICAgICAgc3B5T24oc2VydmljZSwgJ3NhdmUnKS5hbmQucmV0dXJuVmFsdWUob2YobmV3IEh0dHBSZXNwb25zZSh7IGJvZHk6IHRydWUgfSkpKTtcblxuICAgICAgY29tcC5wYXNzd29yZEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgIGN1cnJlbnRQYXNzd29yZDogcGFzc3dvcmRWYWx1ZXMuY3VycmVudFBhc3N3b3JkLFxuICAgICAgICBuZXdQYXNzd29yZDogcGFzc3dvcmRWYWx1ZXMubmV3UGFzc3dvcmQsXG4gICAgICAgIGNvbmZpcm1QYXNzd29yZDogcGFzc3dvcmRWYWx1ZXMubmV3UGFzc3dvcmRcbiAgICAgIH0pO1xuXG4gICAgICAvLyBXSEVOXG4gICAgICBjb21wLmNoYW5nZVBhc3N3b3JkKCk7XG5cbiAgICAgIC8vIFRIRU5cbiAgICAgIGV4cGVjdChzZXJ2aWNlLnNhdmUpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKHBhc3N3b3JkVmFsdWVzLm5ld1Bhc3N3b3JkLCBwYXNzd29yZFZhbHVlcy5jdXJyZW50UGFzc3dvcmQpO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBzZXQgc3VjY2VzcyB0byB0cnVlIHVwb24gc3VjY2VzcycsICgpID0+IHtcbiAgICAgIC8vIEdJVkVOXG4gICAgICBzcHlPbihzZXJ2aWNlLCAnc2F2ZScpLmFuZC5yZXR1cm5WYWx1ZShvZihuZXcgSHR0cFJlc3BvbnNlKHsgYm9keTogdHJ1ZSB9KSkpO1xuICAgICAgY29tcC5wYXNzd29yZEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgIG5ld1Bhc3N3b3JkOiAnbXlQYXNzd29yZCcsXG4gICAgICAgIGNvbmZpcm1QYXNzd29yZDogJ215UGFzc3dvcmQnXG4gICAgICB9KTtcblxuICAgICAgLy8gV0hFTlxuICAgICAgY29tcC5jaGFuZ2VQYXNzd29yZCgpO1xuXG4gICAgICAvLyBUSEVOXG4gICAgICBleHBlY3QoY29tcC5kb05vdE1hdGNoKS50b0JlKGZhbHNlKTtcbiAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKGZhbHNlKTtcbiAgICAgIGV4cGVjdChjb21wLnN1Y2Nlc3MpLnRvQmUodHJ1ZSk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIG5vdGlmeSBvZiBlcnJvciBpZiBjaGFuZ2UgcGFzc3dvcmQgZmFpbHMnLCAoKSA9PiB7XG4gICAgICAvLyBHSVZFTlxuICAgICAgc3B5T24oc2VydmljZSwgJ3NhdmUnKS5hbmQucmV0dXJuVmFsdWUodGhyb3dFcnJvcignRVJST1InKSk7XG4gICAgICBjb21wLnBhc3N3b3JkRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgbmV3UGFzc3dvcmQ6ICdteVBhc3N3b3JkJyxcbiAgICAgICAgY29uZmlybVBhc3N3b3JkOiAnbXlQYXNzd29yZCdcbiAgICAgIH0pO1xuXG4gICAgICAvLyBXSEVOXG4gICAgICBjb21wLmNoYW5nZVBhc3N3b3JkKCk7XG5cbiAgICAgIC8vIFRIRU5cbiAgICAgIGV4cGVjdChjb21wLmRvTm90TWF0Y2gpLnRvQmUoZmFsc2UpO1xuICAgICAgZXhwZWN0KGNvbXAuc3VjY2VzcykudG9CZShmYWxzZSk7XG4gICAgICBleHBlY3QoY29tcC5lcnJvcikudG9CZSh0cnVlKTtcbiAgICB9KTtcbiAgfSk7XG59KTtcbiJdLCJ2ZXJzaW9uIjozfQ==