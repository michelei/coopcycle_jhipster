"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_jhipster_1 = require("ng-jhipster");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const order_service_1 = require("./order.service");
const order_delete_dialog_component_1 = require("./order-delete-dialog.component");
let OrderComponent = class OrderComponent {
    constructor(orderService, eventManager, modalService) {
        this.orderService = orderService;
        this.eventManager = eventManager;
        this.modalService = modalService;
    }
    loadAll() {
        this.orderService.query().subscribe((res) => (this.orders = res.body || []));
    }
    ngOnInit() {
        this.loadAll();
        this.registerChangeInOrders();
    }
    ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventManager.destroy(this.eventSubscriber);
        }
    }
    trackId(index, item) {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
        return item.id;
    }
    registerChangeInOrders() {
        this.eventSubscriber = this.eventManager.subscribe('orderListModification', () => this.loadAll());
    }
    delete(order) {
        const modalRef = this.modalService.open(order_delete_dialog_component_1.OrderDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.order = order;
    }
};
OrderComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-order',
        template: require('./order.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [order_service_1.OrderService, ng_jhipster_1.JhiEventManager, ng_bootstrap_1.NgbModal])
], OrderComponent);
exports.OrderComponent = OrderComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvb3JkZXIvb3JkZXIuY29tcG9uZW50LnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUE2RDtBQUc3RCw2Q0FBOEM7QUFDOUMsNkRBQXNEO0FBR3RELG1EQUErQztBQUMvQyxtRkFBNkU7QUFNN0UsSUFBYSxjQUFjLEdBQTNCLE1BQWEsY0FBYztJQUl6QixZQUFzQixZQUEwQixFQUFZLFlBQTZCLEVBQVksWUFBc0I7UUFBckcsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBWSxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFBWSxpQkFBWSxHQUFaLFlBQVksQ0FBVTtJQUFHLENBQUM7SUFFL0gsT0FBTztRQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBMkIsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN2RyxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNmLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7SUFFRCxPQUFPLENBQUMsS0FBYSxFQUFFLElBQVk7UUFDakMsNEVBQTRFO1FBQzVFLE9BQU8sSUFBSSxDQUFDLEVBQUcsQ0FBQztJQUNsQixDQUFDO0lBRUQsc0JBQXNCO1FBQ3BCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsdUJBQXVCLEVBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDcEcsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFhO1FBQ2xCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLDBEQUEwQixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUN4RyxRQUFRLENBQUMsaUJBQWlCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUMzQyxDQUFDO0NBQ0YsQ0FBQTtBQWxDWSxjQUFjO0lBSjFCLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsV0FBVztRQUNyQixrQkFBYSx3QkFBd0IsQ0FBQTtLQUN0QyxDQUFDOzZDQUtvQyw0QkFBWSxFQUEwQiw2QkFBZSxFQUEwQix1QkFBUTtHQUpoSCxjQUFjLENBa0MxQjtBQWxDWSx3Q0FBYyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9lbnRpdGllcy9vcmRlci9vcmRlci5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBKaGlFdmVudE1hbmFnZXIgfSBmcm9tICduZy1qaGlwc3Rlcic7XG5pbXBvcnQgeyBOZ2JNb2RhbCB9IGZyb20gJ0BuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwJztcblxuaW1wb3J0IHsgSU9yZGVyIH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9vcmRlci5tb2RlbCc7XG5pbXBvcnQgeyBPcmRlclNlcnZpY2UgfSBmcm9tICcuL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgT3JkZXJEZWxldGVEaWFsb2dDb21wb25lbnQgfSBmcm9tICcuL29yZGVyLWRlbGV0ZS1kaWFsb2cuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLW9yZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL29yZGVyLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBPcmRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgb3JkZXJzPzogSU9yZGVyW107XG4gIGV2ZW50U3Vic2NyaWJlcj86IFN1YnNjcmlwdGlvbjtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgb3JkZXJTZXJ2aWNlOiBPcmRlclNlcnZpY2UsIHByb3RlY3RlZCBldmVudE1hbmFnZXI6IEpoaUV2ZW50TWFuYWdlciwgcHJvdGVjdGVkIG1vZGFsU2VydmljZTogTmdiTW9kYWwpIHt9XG5cbiAgbG9hZEFsbCgpOiB2b2lkIHtcbiAgICB0aGlzLm9yZGVyU2VydmljZS5xdWVyeSgpLnN1YnNjcmliZSgocmVzOiBIdHRwUmVzcG9uc2U8SU9yZGVyW10+KSA9PiAodGhpcy5vcmRlcnMgPSByZXMuYm9keSB8fCBbXSkpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5sb2FkQWxsKCk7XG4gICAgdGhpcy5yZWdpc3RlckNoYW5nZUluT3JkZXJzKCk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5ldmVudFN1YnNjcmliZXIpIHtcbiAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmRlc3Ryb3kodGhpcy5ldmVudFN1YnNjcmliZXIpO1xuICAgIH1cbiAgfVxuXG4gIHRyYWNrSWQoaW5kZXg6IG51bWJlciwgaXRlbTogSU9yZGVyKTogbnVtYmVyIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVubmVjZXNzYXJ5LXR5cGUtYXNzZXJ0aW9uXG4gICAgcmV0dXJuIGl0ZW0uaWQhO1xuICB9XG5cbiAgcmVnaXN0ZXJDaGFuZ2VJbk9yZGVycygpOiB2b2lkIHtcbiAgICB0aGlzLmV2ZW50U3Vic2NyaWJlciA9IHRoaXMuZXZlbnRNYW5hZ2VyLnN1YnNjcmliZSgnb3JkZXJMaXN0TW9kaWZpY2F0aW9uJywgKCkgPT4gdGhpcy5sb2FkQWxsKCkpO1xuICB9XG5cbiAgZGVsZXRlKG9yZGVyOiBJT3JkZXIpOiB2b2lkIHtcbiAgICBjb25zdCBtb2RhbFJlZiA9IHRoaXMubW9kYWxTZXJ2aWNlLm9wZW4oT3JkZXJEZWxldGVEaWFsb2dDb21wb25lbnQsIHsgc2l6ZTogJ2xnJywgYmFja2Ryb3A6ICdzdGF0aWMnIH0pO1xuICAgIG1vZGFsUmVmLmNvbXBvbmVudEluc3RhbmNlLm9yZGVyID0gb3JkZXI7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==