"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const cooperative_model_1 = require("app/shared/model/cooperative.model");
const cooperative_service_1 = require("./cooperative.service");
const restaurant_service_1 = require("app/entities/restaurant/restaurant.service");
let CooperativeUpdateComponent = class CooperativeUpdateComponent {
    constructor(cooperativeService, restaurantService, activatedRoute, fb) {
        this.cooperativeService = cooperativeService;
        this.restaurantService = restaurantService;
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.isSaving = false;
        this.restaurants = [];
        this.editForm = this.fb.group({
            id: [],
            cooperativeId: [null, [forms_1.Validators.required]],
            name: [],
            area: [],
            restaurants: []
        });
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ cooperative }) => {
            this.updateForm(cooperative);
            this.restaurantService.query().subscribe((res) => (this.restaurants = res.body || []));
        });
    }
    updateForm(cooperative) {
        this.editForm.patchValue({
            id: cooperative.id,
            cooperativeId: cooperative.cooperativeId,
            name: cooperative.name,
            area: cooperative.area,
            restaurants: cooperative.restaurants
        });
    }
    previousState() {
        window.history.back();
    }
    save() {
        this.isSaving = true;
        const cooperative = this.createFromForm();
        if (cooperative.id !== undefined) {
            this.subscribeToSaveResponse(this.cooperativeService.update(cooperative));
        }
        else {
            this.subscribeToSaveResponse(this.cooperativeService.create(cooperative));
        }
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new cooperative_model_1.Cooperative()), { id: this.editForm.get(['id']).value, cooperativeId: this.editForm.get(['cooperativeId']).value, name: this.editForm.get(['name']).value, area: this.editForm.get(['area']).value, restaurants: this.editForm.get(['restaurants']).value });
    }
    subscribeToSaveResponse(result) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
    }
    trackById(index, item) {
        return item.id;
    }
    getSelected(selectedVals, option) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
};
CooperativeUpdateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-cooperative-update',
        template: require('./cooperative-update.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [cooperative_service_1.CooperativeService,
        restaurant_service_1.RestaurantService,
        router_1.ActivatedRoute,
        forms_1.FormBuilder])
], CooperativeUpdateComponent);
exports.CooperativeUpdateComponent = CooperativeUpdateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvY29vcGVyYXRpdmUvY29vcGVyYXRpdmUtdXBkYXRlLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBa0Q7QUFFbEQsNkRBQTZEO0FBQzdELDBDQUF5RDtBQUN6RCw0Q0FBaUQ7QUFHakQsMEVBQStFO0FBQy9FLCtEQUEyRDtBQUUzRCxtRkFBK0U7QUFNL0UsSUFBYSwwQkFBMEIsR0FBdkMsTUFBYSwwQkFBMEI7SUFZckMsWUFDWSxrQkFBc0MsRUFDdEMsaUJBQW9DLEVBQ3BDLGNBQThCLEVBQ2hDLEVBQWU7UUFIYix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQ2hDLE9BQUUsR0FBRixFQUFFLENBQWE7UUFmekIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixnQkFBVyxHQUFrQixFQUFFLENBQUM7UUFFaEMsYUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLEVBQUUsRUFBRSxFQUFFO1lBQ04sYUFBYSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM1QyxJQUFJLEVBQUUsRUFBRTtZQUNSLElBQUksRUFBRSxFQUFFO1lBQ1IsV0FBVyxFQUFFLEVBQUU7U0FDaEIsQ0FBQyxDQUFDO0lBT0EsQ0FBQztJQUVKLFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUU7WUFDckQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUU3QixJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBZ0MsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0SCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsV0FBeUI7UUFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7WUFDdkIsRUFBRSxFQUFFLFdBQVcsQ0FBQyxFQUFFO1lBQ2xCLGFBQWEsRUFBRSxXQUFXLENBQUMsYUFBYTtZQUN4QyxJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUk7WUFDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJO1lBQ3RCLFdBQVcsRUFBRSxXQUFXLENBQUMsV0FBVztTQUNyQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNYLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUMsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLFNBQVMsRUFBRTtZQUNoQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQzNFO2FBQU07WUFDTCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQzNFO0lBQ0gsQ0FBQztJQUVPLGNBQWM7UUFDcEIsdUNBQ0ssSUFBSSwrQkFBVyxFQUFFLEtBQ3BCLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUNwQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFDMUQsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUUsQ0FBQyxLQUFLLEVBQ3hDLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUN4QyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBRSxDQUFDLEtBQUssSUFDdEQ7SUFDSixDQUFDO0lBRVMsdUJBQXVCLENBQUMsTUFBOEM7UUFDOUUsTUFBTSxDQUFDLFNBQVMsQ0FDZCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQzFCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDekIsQ0FBQztJQUNKLENBQUM7SUFFUyxhQUFhO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQWEsRUFBRSxJQUFpQjtRQUN4QyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQUVELFdBQVcsQ0FBQyxZQUEyQixFQUFFLE1BQW1CO1FBQzFELElBQUksWUFBWSxFQUFFO1lBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QyxJQUFJLE1BQU0sQ0FBQyxFQUFFLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDcEMsT0FBTyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hCO2FBQ0Y7U0FDRjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7Q0FDRixDQUFBO0FBNUZZLDBCQUEwQjtJQUp0QyxnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLHdCQUF3QjtRQUNsQyxrQkFBYSxxQ0FBcUMsQ0FBQTtLQUNuRCxDQUFDOzZDQWNnQyx3Q0FBa0I7UUFDbkIsc0NBQWlCO1FBQ3BCLHVCQUFjO1FBQzVCLG1CQUFXO0dBaEJkLDBCQUEwQixDQTRGdEM7QUE1RlksZ0VBQTBCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2VudGl0aWVzL2Nvb3BlcmF0aXZlL2Nvb3BlcmF0aXZlLXVwZGF0ZS5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBAdHlwZXNjcmlwdC1lc2xpbnQvbm8tdW51c2VkLXZhcnNcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBJQ29vcGVyYXRpdmUsIENvb3BlcmF0aXZlIH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9jb29wZXJhdGl2ZS5tb2RlbCc7XG5pbXBvcnQgeyBDb29wZXJhdGl2ZVNlcnZpY2UgfSBmcm9tICcuL2Nvb3BlcmF0aXZlLnNlcnZpY2UnO1xuaW1wb3J0IHsgSVJlc3RhdXJhbnQgfSBmcm9tICdhcHAvc2hhcmVkL21vZGVsL3Jlc3RhdXJhbnQubW9kZWwnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICdhcHAvZW50aXRpZXMvcmVzdGF1cmFudC9yZXN0YXVyYW50LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktY29vcGVyYXRpdmUtdXBkYXRlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Nvb3BlcmF0aXZlLXVwZGF0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQ29vcGVyYXRpdmVVcGRhdGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpc1NhdmluZyA9IGZhbHNlO1xuICByZXN0YXVyYW50czogSVJlc3RhdXJhbnRbXSA9IFtdO1xuXG4gIGVkaXRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgaWQ6IFtdLFxuICAgIGNvb3BlcmF0aXZlSWQ6IFtudWxsLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF1dLFxuICAgIG5hbWU6IFtdLFxuICAgIGFyZWE6IFtdLFxuICAgIHJlc3RhdXJhbnRzOiBbXVxuICB9KTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgY29vcGVyYXRpdmVTZXJ2aWNlOiBDb29wZXJhdGl2ZVNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJlc3RhdXJhbnRTZXJ2aWNlOiBSZXN0YXVyYW50U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyXG4gICkge31cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLmRhdGEuc3Vic2NyaWJlKCh7IGNvb3BlcmF0aXZlIH0pID0+IHtcbiAgICAgIHRoaXMudXBkYXRlRm9ybShjb29wZXJhdGl2ZSk7XG5cbiAgICAgIHRoaXMucmVzdGF1cmFudFNlcnZpY2UucXVlcnkoKS5zdWJzY3JpYmUoKHJlczogSHR0cFJlc3BvbnNlPElSZXN0YXVyYW50W10+KSA9PiAodGhpcy5yZXN0YXVyYW50cyA9IHJlcy5ib2R5IHx8IFtdKSk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVGb3JtKGNvb3BlcmF0aXZlOiBJQ29vcGVyYXRpdmUpOiB2b2lkIHtcbiAgICB0aGlzLmVkaXRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgaWQ6IGNvb3BlcmF0aXZlLmlkLFxuICAgICAgY29vcGVyYXRpdmVJZDogY29vcGVyYXRpdmUuY29vcGVyYXRpdmVJZCxcbiAgICAgIG5hbWU6IGNvb3BlcmF0aXZlLm5hbWUsXG4gICAgICBhcmVhOiBjb29wZXJhdGl2ZS5hcmVhLFxuICAgICAgcmVzdGF1cmFudHM6IGNvb3BlcmF0aXZlLnJlc3RhdXJhbnRzXG4gICAgfSk7XG4gIH1cblxuICBwcmV2aW91c1N0YXRlKCk6IHZvaWQge1xuICAgIHdpbmRvdy5oaXN0b3J5LmJhY2soKTtcbiAgfVxuXG4gIHNhdmUoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IHRydWU7XG4gICAgY29uc3QgY29vcGVyYXRpdmUgPSB0aGlzLmNyZWF0ZUZyb21Gb3JtKCk7XG4gICAgaWYgKGNvb3BlcmF0aXZlLmlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UodGhpcy5jb29wZXJhdGl2ZVNlcnZpY2UudXBkYXRlKGNvb3BlcmF0aXZlKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UodGhpcy5jb29wZXJhdGl2ZVNlcnZpY2UuY3JlYXRlKGNvb3BlcmF0aXZlKSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVGcm9tRm9ybSgpOiBJQ29vcGVyYXRpdmUge1xuICAgIHJldHVybiB7XG4gICAgICAuLi5uZXcgQ29vcGVyYXRpdmUoKSxcbiAgICAgIGlkOiB0aGlzLmVkaXRGb3JtLmdldChbJ2lkJ10pIS52YWx1ZSxcbiAgICAgIGNvb3BlcmF0aXZlSWQ6IHRoaXMuZWRpdEZvcm0uZ2V0KFsnY29vcGVyYXRpdmVJZCddKSEudmFsdWUsXG4gICAgICBuYW1lOiB0aGlzLmVkaXRGb3JtLmdldChbJ25hbWUnXSkhLnZhbHVlLFxuICAgICAgYXJlYTogdGhpcy5lZGl0Rm9ybS5nZXQoWydhcmVhJ10pIS52YWx1ZSxcbiAgICAgIHJlc3RhdXJhbnRzOiB0aGlzLmVkaXRGb3JtLmdldChbJ3Jlc3RhdXJhbnRzJ10pIS52YWx1ZVxuICAgIH07XG4gIH1cblxuICBwcm90ZWN0ZWQgc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UocmVzdWx0OiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxJQ29vcGVyYXRpdmU+Pik6IHZvaWQge1xuICAgIHJlc3VsdC5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB0aGlzLm9uU2F2ZVN1Y2Nlc3MoKSxcbiAgICAgICgpID0+IHRoaXMub25TYXZlRXJyb3IoKVxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgb25TYXZlU3VjY2VzcygpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gZmFsc2U7XG4gICAgdGhpcy5wcmV2aW91c1N0YXRlKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgb25TYXZlRXJyb3IoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xuICB9XG5cbiAgdHJhY2tCeUlkKGluZGV4OiBudW1iZXIsIGl0ZW06IElSZXN0YXVyYW50KTogYW55IHtcbiAgICByZXR1cm4gaXRlbS5pZDtcbiAgfVxuXG4gIGdldFNlbGVjdGVkKHNlbGVjdGVkVmFsczogSVJlc3RhdXJhbnRbXSwgb3B0aW9uOiBJUmVzdGF1cmFudCk6IElSZXN0YXVyYW50IHtcbiAgICBpZiAoc2VsZWN0ZWRWYWxzKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNlbGVjdGVkVmFscy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAob3B0aW9uLmlkID09PSBzZWxlY3RlZFZhbHNbaV0uaWQpIHtcbiAgICAgICAgICByZXR1cm4gc2VsZWN0ZWRWYWxzW2ldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvcHRpb247XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==