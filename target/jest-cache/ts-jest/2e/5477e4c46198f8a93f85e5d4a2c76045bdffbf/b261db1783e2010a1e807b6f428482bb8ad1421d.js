"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const password_reset_init_service_1 = require("./password-reset-init.service");
let PasswordResetInitComponent = class PasswordResetInitComponent {
    constructor(passwordResetInitService, fb) {
        this.passwordResetInitService = passwordResetInitService;
        this.fb = fb;
        this.success = false;
        this.resetRequestForm = this.fb.group({
            email: ['', [forms_1.Validators.required, forms_1.Validators.minLength(5), forms_1.Validators.maxLength(254), forms_1.Validators.email]]
        });
    }
    ngAfterViewInit() {
        if (this.email) {
            this.email.nativeElement.focus();
        }
    }
    requestReset() {
        this.passwordResetInitService.save(this.resetRequestForm.get(['email']).value).subscribe(() => (this.success = true));
    }
};
tslib_1.__decorate([
    core_1.ViewChild('email', { static: false }),
    tslib_1.__metadata("design:type", core_1.ElementRef)
], PasswordResetInitComponent.prototype, "email", void 0);
PasswordResetInitComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-password-reset-init',
        template: require('./password-reset-init.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [password_reset_init_service_1.PasswordResetInitService, forms_1.FormBuilder])
], PasswordResetInitComponent);
exports.PasswordResetInitComponent = PasswordResetInitComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9pbml0L3Bhc3N3b3JkLXJlc2V0LWluaXQuY29tcG9uZW50LnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUFnRjtBQUNoRiwwQ0FBeUQ7QUFFekQsK0VBQXlFO0FBTXpFLElBQWEsMEJBQTBCLEdBQXZDLE1BQWEsMEJBQTBCO0lBU3JDLFlBQW9CLHdCQUFrRCxFQUFVLEVBQWU7UUFBM0UsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUFVLE9BQUUsR0FBRixFQUFFLENBQWE7UUFML0YsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixxQkFBZ0IsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUMvQixLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxrQkFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pHLENBQUMsQ0FBQztJQUUrRixDQUFDO0lBRW5HLGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDekgsQ0FBQztDQUNGLENBQUE7QUFsQkM7SUFEQyxnQkFBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztzQ0FDOUIsaUJBQVU7eURBQUM7QUFGUiwwQkFBMEI7SUFKdEMsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSx5QkFBeUI7UUFDbkMsa0JBQWEsc0NBQXNDLENBQUE7S0FDcEQsQ0FBQzs2Q0FVOEMsc0RBQXdCLEVBQWMsbUJBQVc7R0FUcEYsMEJBQTBCLENBb0J0QztBQXBCWSxnRUFBMEIiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9pbml0L3Bhc3N3b3JkLXJlc2V0LWluaXQuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgQWZ0ZXJWaWV3SW5pdCwgRWxlbWVudFJlZiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgUGFzc3dvcmRSZXNldEluaXRTZXJ2aWNlIH0gZnJvbSAnLi9wYXNzd29yZC1yZXNldC1pbml0LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktcGFzc3dvcmQtcmVzZXQtaW5pdCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9wYXNzd29yZC1yZXNldC1pbml0LmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBQYXNzd29yZFJlc2V0SW5pdENvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICBAVmlld0NoaWxkKCdlbWFpbCcsIHsgc3RhdGljOiBmYWxzZSB9KVxuICBlbWFpbD86IEVsZW1lbnRSZWY7XG5cbiAgc3VjY2VzcyA9IGZhbHNlO1xuICByZXNldFJlcXVlc3RGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgZW1haWw6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDUpLCBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTQpLCBWYWxpZGF0b3JzLmVtYWlsXV1cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYXNzd29yZFJlc2V0SW5pdFNlcnZpY2U6IFBhc3N3b3JkUmVzZXRJbml0U2VydmljZSwgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIpIHt9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmVtYWlsKSB7XG4gICAgICB0aGlzLmVtYWlsLm5hdGl2ZUVsZW1lbnQuZm9jdXMoKTtcbiAgICB9XG4gIH1cblxuICByZXF1ZXN0UmVzZXQoKTogdm9pZCB7XG4gICAgdGhpcy5wYXNzd29yZFJlc2V0SW5pdFNlcnZpY2Uuc2F2ZSh0aGlzLnJlc2V0UmVxdWVzdEZvcm0uZ2V0KFsnZW1haWwnXSkhLnZhbHVlKS5zdWJzY3JpYmUoKCkgPT4gKHRoaXMuc3VjY2VzcyA9IHRydWUpKTtcbiAgfVxufVxuIl0sInZlcnNpb24iOjN9