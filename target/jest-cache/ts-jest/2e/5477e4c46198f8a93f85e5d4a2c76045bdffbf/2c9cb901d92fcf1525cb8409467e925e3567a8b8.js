"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const payment_model_1 = require("app/shared/model/payment.model");
const payment_service_1 = require("./payment.service");
let PaymentUpdateComponent = class PaymentUpdateComponent {
    constructor(paymentService, activatedRoute, fb) {
        this.paymentService = paymentService;
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.isSaving = false;
        this.editForm = this.fb.group({
            id: [],
            paymentMethod: [null, [forms_1.Validators.required]]
        });
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ payment }) => {
            this.updateForm(payment);
        });
    }
    updateForm(payment) {
        this.editForm.patchValue({
            id: payment.id,
            paymentMethod: payment.paymentMethod
        });
    }
    previousState() {
        window.history.back();
    }
    save() {
        this.isSaving = true;
        const payment = this.createFromForm();
        if (payment.id !== undefined) {
            this.subscribeToSaveResponse(this.paymentService.update(payment));
        }
        else {
            this.subscribeToSaveResponse(this.paymentService.create(payment));
        }
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new payment_model_1.Payment()), { id: this.editForm.get(['id']).value, paymentMethod: this.editForm.get(['paymentMethod']).value });
    }
    subscribeToSaveResponse(result) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
    }
};
PaymentUpdateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-payment-update',
        template: require('./payment-update.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [payment_service_1.PaymentService, router_1.ActivatedRoute, forms_1.FormBuilder])
], PaymentUpdateComponent);
exports.PaymentUpdateComponent = PaymentUpdateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvcGF5bWVudC9wYXltZW50LXVwZGF0ZS5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBRWxELDZEQUE2RDtBQUM3RCwwQ0FBeUQ7QUFDekQsNENBQWlEO0FBR2pELGtFQUFtRTtBQUNuRSx1REFBbUQ7QUFNbkQsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7SUFRakMsWUFBc0IsY0FBOEIsRUFBWSxjQUE4QixFQUFVLEVBQWU7UUFBakcsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVksbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQVB2SCxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBRWpCLGFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUN2QixFQUFFLEVBQUUsRUFBRTtZQUNOLGFBQWEsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDN0MsQ0FBQyxDQUFDO0lBRXVILENBQUM7SUFFM0gsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRTtZQUNqRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVUsQ0FBQyxPQUFpQjtRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztZQUN2QixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDZCxhQUFhLEVBQUUsT0FBTyxDQUFDLGFBQWE7U0FDckMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGFBQWE7UUFDWCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RDLElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7WUFDNUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7U0FDbkU7YUFBTTtZQUNMLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQ25FO0lBQ0gsQ0FBQztJQUVPLGNBQWM7UUFDcEIsdUNBQ0ssSUFBSSx1QkFBTyxFQUFFLEtBQ2hCLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUNwQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBRSxDQUFDLEtBQUssSUFDMUQ7SUFDSixDQUFDO0lBRVMsdUJBQXVCLENBQUMsTUFBMEM7UUFDMUUsTUFBTSxDQUFDLFNBQVMsQ0FDZCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQzFCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDekIsQ0FBQztJQUNKLENBQUM7SUFFUyxhQUFhO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0NBQ0YsQ0FBQTtBQTVEWSxzQkFBc0I7SUFKbEMsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxvQkFBb0I7UUFDOUIsa0JBQWEsaUNBQWlDLENBQUE7S0FDL0MsQ0FBQzs2Q0FTc0MsZ0NBQWMsRUFBNEIsdUJBQWMsRUFBYyxtQkFBVztHQVI1RyxzQkFBc0IsQ0E0RGxDO0FBNURZLHdEQUFzQiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9lbnRpdGllcy9wYXltZW50L3BheW1lbnQtdXBkYXRlLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIEB0eXBlc2NyaXB0LWVzbGludC9uby11bnVzZWQtdmFyc1xuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IElQYXltZW50LCBQYXltZW50IH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9wYXltZW50Lm1vZGVsJztcbmltcG9ydCB7IFBheW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi9wYXltZW50LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktcGF5bWVudC11cGRhdGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vcGF5bWVudC11cGRhdGUuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFBheW1lbnRVcGRhdGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpc1NhdmluZyA9IGZhbHNlO1xuXG4gIGVkaXRGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgaWQ6IFtdLFxuICAgIHBheW1lbnRNZXRob2Q6IFtudWxsLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF1dXG4gIH0pO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBwYXltZW50U2VydmljZTogUGF5bWVudFNlcnZpY2UsIHByb3RlY3RlZCBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUuZGF0YS5zdWJzY3JpYmUoKHsgcGF5bWVudCB9KSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZUZvcm0ocGF5bWVudCk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVGb3JtKHBheW1lbnQ6IElQYXltZW50KTogdm9pZCB7XG4gICAgdGhpcy5lZGl0Rm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgIGlkOiBwYXltZW50LmlkLFxuICAgICAgcGF5bWVudE1ldGhvZDogcGF5bWVudC5wYXltZW50TWV0aG9kXG4gICAgfSk7XG4gIH1cblxuICBwcmV2aW91c1N0YXRlKCk6IHZvaWQge1xuICAgIHdpbmRvdy5oaXN0b3J5LmJhY2soKTtcbiAgfVxuXG4gIHNhdmUoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IHRydWU7XG4gICAgY29uc3QgcGF5bWVudCA9IHRoaXMuY3JlYXRlRnJvbUZvcm0oKTtcbiAgICBpZiAocGF5bWVudC5pZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLnN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHRoaXMucGF5bWVudFNlcnZpY2UudXBkYXRlKHBheW1lbnQpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zdWJzY3JpYmVUb1NhdmVSZXNwb25zZSh0aGlzLnBheW1lbnRTZXJ2aWNlLmNyZWF0ZShwYXltZW50KSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVGcm9tRm9ybSgpOiBJUGF5bWVudCB7XG4gICAgcmV0dXJuIHtcbiAgICAgIC4uLm5ldyBQYXltZW50KCksXG4gICAgICBpZDogdGhpcy5lZGl0Rm9ybS5nZXQoWydpZCddKSEudmFsdWUsXG4gICAgICBwYXltZW50TWV0aG9kOiB0aGlzLmVkaXRGb3JtLmdldChbJ3BheW1lbnRNZXRob2QnXSkhLnZhbHVlXG4gICAgfTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzdWJzY3JpYmVUb1NhdmVSZXNwb25zZShyZXN1bHQ6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPElQYXltZW50Pj4pOiB2b2lkIHtcbiAgICByZXN1bHQuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4gdGhpcy5vblNhdmVTdWNjZXNzKCksXG4gICAgICAoKSA9PiB0aGlzLm9uU2F2ZUVycm9yKClcbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIG9uU2F2ZVN1Y2Nlc3MoKTogdm9pZCB7XG4gICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xuICAgIHRoaXMucHJldmlvdXNTdGF0ZSgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIG9uU2F2ZUVycm9yKCk6IHZvaWQge1xuICAgIHRoaXMuaXNTYXZpbmcgPSBmYWxzZTtcbiAgfVxufVxuIl0sInZlcnNpb24iOjN9