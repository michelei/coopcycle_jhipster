"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const configuration_component_1 = require("app/admin/configuration/configuration.component");
const configuration_service_1 = require("app/admin/configuration/configuration.service");
describe('Component Tests', () => {
    describe('ConfigurationComponent', () => {
        let comp;
        let fixture;
        let service;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [configuration_component_1.ConfigurationComponent],
                providers: [configuration_service_1.ConfigurationService]
            })
                .overrideTemplate(configuration_component_1.ConfigurationComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(configuration_component_1.ConfigurationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(configuration_service_1.ConfigurationService);
        });
        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const beans = [
                    {
                        prefix: 'jhipster',
                        properties: {
                            clientApp: {
                                name: 'jhipsterApp'
                            }
                        }
                    }
                ];
                const propertySources = [
                    {
                        name: 'server.ports',
                        properties: {
                            'local.server.port': {
                                value: '8080'
                            }
                        }
                    }
                ];
                spyOn(service, 'getBeans').and.returnValue(rxjs_1.of(beans));
                spyOn(service, 'getPropertySources').and.returnValue(rxjs_1.of(propertySources));
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(service.getBeans).toHaveBeenCalled();
                expect(service.getPropertySources).toHaveBeenCalled();
                expect(comp.allBeans).toEqual(beans);
                expect(comp.beans).toEqual(beans);
                expect(comp.propertySources).toEqual(propertySources);
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vY29uZmlndXJhdGlvbi9jb25maWd1cmF0aW9uLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlFO0FBQ3pFLCtCQUEwQjtBQUUxQixzREFBMkQ7QUFDM0QsNkZBQXlGO0FBQ3pGLHlGQUEyRztBQUUzRyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyx3QkFBd0IsRUFBRSxHQUFHLEVBQUU7UUFDdEMsSUFBSSxJQUE0QixDQUFDO1FBQ2pDLElBQUksT0FBaUQsQ0FBQztRQUN0RCxJQUFJLE9BQTZCLENBQUM7UUFFbEMsVUFBVSxDQUFDLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDcEIsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQW1CLENBQUM7Z0JBQzlCLFlBQVksRUFBRSxDQUFDLGdEQUFzQixDQUFDO2dCQUN0QyxTQUFTLEVBQUUsQ0FBQyw0Q0FBb0IsQ0FBQzthQUNsQyxDQUFDO2lCQUNDLGdCQUFnQixDQUFDLGdEQUFzQixFQUFFLEVBQUUsQ0FBQztpQkFDNUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRUosVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxnREFBc0IsQ0FBQyxDQUFDO1lBQzFELElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7WUFDakMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyw0Q0FBb0IsQ0FBQyxDQUFDO1FBQ3BFLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUU7WUFDdEIsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEdBQUcsRUFBRTtnQkFDdEMsUUFBUTtnQkFDUixNQUFNLEtBQUssR0FBVztvQkFDcEI7d0JBQ0UsTUFBTSxFQUFFLFVBQVU7d0JBQ2xCLFVBQVUsRUFBRTs0QkFDVixTQUFTLEVBQUU7Z0NBQ1QsSUFBSSxFQUFFLGFBQWE7NkJBQ3BCO3lCQUNGO3FCQUNGO2lCQUNGLENBQUM7Z0JBQ0YsTUFBTSxlQUFlLEdBQXFCO29CQUN4Qzt3QkFDRSxJQUFJLEVBQUUsY0FBYzt3QkFDcEIsVUFBVSxFQUFFOzRCQUNWLG1CQUFtQixFQUFFO2dDQUNuQixLQUFLLEVBQUUsTUFBTTs2QkFDZDt5QkFDRjtxQkFDRjtpQkFDRixDQUFDO2dCQUNGLEtBQUssQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDdEQsS0FBSyxDQUFDLE9BQU8sRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsU0FBRSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBRTFFLE9BQU87Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUVoQixPQUFPO2dCQUNQLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDNUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3RELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vY29uZmlndXJhdGlvbi9jb25maWd1cmF0aW9uLmNvbXBvbmVudC5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQsIGFzeW5jIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IENvb3BjeWNsZVRlc3RNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi90ZXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBDb25maWd1cmF0aW9uQ29tcG9uZW50IH0gZnJvbSAnYXBwL2FkbWluL2NvbmZpZ3VyYXRpb24vY29uZmlndXJhdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29uZmlndXJhdGlvblNlcnZpY2UsIEJlYW4sIFByb3BlcnR5U291cmNlIH0gZnJvbSAnYXBwL2FkbWluL2NvbmZpZ3VyYXRpb24vY29uZmlndXJhdGlvbi5zZXJ2aWNlJztcblxuZGVzY3JpYmUoJ0NvbXBvbmVudCBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ0NvbmZpZ3VyYXRpb25Db21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IENvbmZpZ3VyYXRpb25Db21wb25lbnQ7XG4gICAgbGV0IGZpeHR1cmU6IENvbXBvbmVudEZpeHR1cmU8Q29uZmlndXJhdGlvbkNvbXBvbmVudD47XG4gICAgbGV0IHNlcnZpY2U6IENvbmZpZ3VyYXRpb25TZXJ2aWNlO1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW0NvbmZpZ3VyYXRpb25Db21wb25lbnRdLFxuICAgICAgICBwcm92aWRlcnM6IFtDb25maWd1cmF0aW9uU2VydmljZV1cbiAgICAgIH0pXG4gICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKENvbmZpZ3VyYXRpb25Db21wb25lbnQsICcnKVxuICAgICAgICAuY29tcGlsZUNvbXBvbmVudHMoKTtcbiAgICB9KSk7XG5cbiAgICBiZWZvcmVFYWNoKCgpID0+IHtcbiAgICAgIGZpeHR1cmUgPSBUZXN0QmVkLmNyZWF0ZUNvbXBvbmVudChDb25maWd1cmF0aW9uQ29tcG9uZW50KTtcbiAgICAgIGNvbXAgPSBmaXh0dXJlLmNvbXBvbmVudEluc3RhbmNlO1xuICAgICAgc2VydmljZSA9IGZpeHR1cmUuZGVidWdFbGVtZW50LmluamVjdG9yLmdldChDb25maWd1cmF0aW9uU2VydmljZSk7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnT25Jbml0JywgKCkgPT4ge1xuICAgICAgaXQoJ1Nob3VsZCBjYWxsIGxvYWQgYWxsIG9uIGluaXQnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGJlYW5zOiBCZWFuW10gPSBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgcHJlZml4OiAnamhpcHN0ZXInLFxuICAgICAgICAgICAgcHJvcGVydGllczoge1xuICAgICAgICAgICAgICBjbGllbnRBcHA6IHtcbiAgICAgICAgICAgICAgICBuYW1lOiAnamhpcHN0ZXJBcHAnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIF07XG4gICAgICAgIGNvbnN0IHByb3BlcnR5U291cmNlczogUHJvcGVydHlTb3VyY2VbXSA9IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnc2VydmVyLnBvcnRzJyxcbiAgICAgICAgICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgICAgJ2xvY2FsLnNlcnZlci5wb3J0Jzoge1xuICAgICAgICAgICAgICAgIHZhbHVlOiAnODA4MCdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgXTtcbiAgICAgICAgc3B5T24oc2VydmljZSwgJ2dldEJlYW5zJykuYW5kLnJldHVyblZhbHVlKG9mKGJlYW5zKSk7XG4gICAgICAgIHNweU9uKHNlcnZpY2UsICdnZXRQcm9wZXJ0eVNvdXJjZXMnKS5hbmQucmV0dXJuVmFsdWUob2YocHJvcGVydHlTb3VyY2VzKSk7XG5cbiAgICAgICAgLy8gV0hFTlxuICAgICAgICBjb21wLm5nT25Jbml0KCk7XG5cbiAgICAgICAgLy8gVEhFTlxuICAgICAgICBleHBlY3Qoc2VydmljZS5nZXRCZWFucykudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICBleHBlY3Qoc2VydmljZS5nZXRQcm9wZXJ0eVNvdXJjZXMpLnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuYWxsQmVhbnMpLnRvRXF1YWwoYmVhbnMpO1xuICAgICAgICBleHBlY3QoY29tcC5iZWFucykudG9FcXVhbChiZWFucyk7XG4gICAgICAgIGV4cGVjdChjb21wLnByb3BlcnR5U291cmNlcykudG9FcXVhbChwcm9wZXJ0eVNvdXJjZXMpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=