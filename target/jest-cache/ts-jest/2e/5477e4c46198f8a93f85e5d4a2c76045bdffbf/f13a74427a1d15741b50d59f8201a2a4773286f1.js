"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const spyobject_1 = require("./spyobject");
class MockActivatedRoute extends router_1.ActivatedRoute {
    constructor(parameters) {
        super();
        this.queryParams = rxjs_1.of(parameters);
        this.params = rxjs_1.of(parameters);
        this.data = rxjs_1.of(Object.assign(Object.assign({}, parameters), { pagingParams: {
                page: 10,
                ascending: false,
                predicate: 'id'
            } }));
    }
}
exports.MockActivatedRoute = MockActivatedRoute;
class MockRouter extends spyobject_1.SpyObject {
    constructor() {
        super(router_1.Router);
        this.events = null;
        this.url = '';
        this.navigateSpy = this.spy('navigate');
        this.navigateByUrlSpy = this.spy('navigateByUrl');
    }
    setEvents(events) {
        this.events = events;
    }
    setRouterState(routerState) {
        this.routerState = routerState;
    }
}
exports.MockRouter = MockRouter;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9oZWxwZXJzL21vY2stcm91dGUuc2VydmljZS50cyIsIm1hcHBpbmdzIjoiOztBQUNBLDRDQUFzRTtBQUN0RSwrQkFBc0M7QUFFdEMsMkNBQXdDO0FBRXhDLE1BQWEsa0JBQW1CLFNBQVEsdUJBQWM7SUFDcEQsWUFBWSxVQUFnQjtRQUMxQixLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBRSxpQ0FDVCxVQUFVLEtBQ2IsWUFBWSxFQUFFO2dCQUNaLElBQUksRUFBRSxFQUFFO2dCQUNSLFNBQVMsRUFBRSxLQUFLO2dCQUNoQixTQUFTLEVBQUUsSUFBSTthQUNoQixJQUNELENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUFkRCxnREFjQztBQUVELE1BQWEsVUFBVyxTQUFRLHFCQUFTO0lBT3ZDO1FBQ0UsS0FBSyxDQUFDLGVBQU0sQ0FBQyxDQUFDO1FBTGhCLFdBQU0sR0FBbUMsSUFBSSxDQUFDO1FBRTlDLFFBQUcsR0FBRyxFQUFFLENBQUM7UUFJUCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELFNBQVMsQ0FBQyxNQUErQjtRQUN2QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRUQsY0FBYyxDQUFDLFdBQWdCO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO0lBQ2pDLENBQUM7Q0FDRjtBQXBCRCxnQ0FvQkMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9oZWxwZXJzL21vY2stcm91dGUuc2VydmljZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU3B5ID0gamFzbWluZS5TcHk7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyLCBSb3V0ZXJFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBTcHlPYmplY3QgfSBmcm9tICcuL3NweW9iamVjdCc7XG5cbmV4cG9ydCBjbGFzcyBNb2NrQWN0aXZhdGVkUm91dGUgZXh0ZW5kcyBBY3RpdmF0ZWRSb3V0ZSB7XG4gIGNvbnN0cnVjdG9yKHBhcmFtZXRlcnM/OiBhbnkpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMucXVlcnlQYXJhbXMgPSBvZihwYXJhbWV0ZXJzKTtcbiAgICB0aGlzLnBhcmFtcyA9IG9mKHBhcmFtZXRlcnMpO1xuICAgIHRoaXMuZGF0YSA9IG9mKHtcbiAgICAgIC4uLnBhcmFtZXRlcnMsXG4gICAgICBwYWdpbmdQYXJhbXM6IHtcbiAgICAgICAgcGFnZTogMTAsXG4gICAgICAgIGFzY2VuZGluZzogZmFsc2UsXG4gICAgICAgIHByZWRpY2F0ZTogJ2lkJ1xuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBNb2NrUm91dGVyIGV4dGVuZHMgU3B5T2JqZWN0IHtcbiAgbmF2aWdhdGVTcHk6IFNweTtcbiAgbmF2aWdhdGVCeVVybFNweTogU3B5O1xuICBldmVudHM6IE9ic2VydmFibGU8Um91dGVyRXZlbnQ+IHwgbnVsbCA9IG51bGw7XG4gIHJvdXRlclN0YXRlOiBhbnk7XG4gIHVybCA9ICcnO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKFJvdXRlcik7XG4gICAgdGhpcy5uYXZpZ2F0ZVNweSA9IHRoaXMuc3B5KCduYXZpZ2F0ZScpO1xuICAgIHRoaXMubmF2aWdhdGVCeVVybFNweSA9IHRoaXMuc3B5KCduYXZpZ2F0ZUJ5VXJsJyk7XG4gIH1cblxuICBzZXRFdmVudHMoZXZlbnRzOiBPYnNlcnZhYmxlPFJvdXRlckV2ZW50Pik6IHZvaWQge1xuICAgIHRoaXMuZXZlbnRzID0gZXZlbnRzO1xuICB9XG5cbiAgc2V0Um91dGVyU3RhdGUocm91dGVyU3RhdGU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMucm91dGVyU3RhdGUgPSByb3V0ZXJTdGF0ZTtcbiAgfVxufVxuIl0sInZlcnNpb24iOjN9