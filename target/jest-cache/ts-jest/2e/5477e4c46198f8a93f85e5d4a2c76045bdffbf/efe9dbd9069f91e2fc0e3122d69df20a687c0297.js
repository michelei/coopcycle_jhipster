"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const http_1 = require("@angular/common/http");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const ngx_webstorage_1 = require("ngx-webstorage");
const app_constants_1 = require("app/app.constants");
let AuthServerProvider = class AuthServerProvider {
    constructor(http, $localStorage, $sessionStorage) {
        this.http = http;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
    }
    getToken() {
        return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken') || '';
    }
    login(credentials) {
        return this.http
            .post(app_constants_1.SERVER_API_URL + 'api/authenticate', credentials)
            .pipe(operators_1.map(response => this.authenticateSuccess(response, credentials.rememberMe)));
    }
    logout() {
        return new rxjs_1.Observable(observer => {
            this.$localStorage.clear('authenticationToken');
            this.$sessionStorage.clear('authenticationToken');
            observer.complete();
        });
    }
    authenticateSuccess(response, rememberMe) {
        const jwt = response.id_token;
        if (rememberMe) {
            this.$localStorage.store('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.store('authenticationToken', jwt);
        }
    }
};
AuthServerProvider = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [http_1.HttpClient, ngx_webstorage_1.LocalStorageService, ngx_webstorage_1.SessionStorageService])
], AuthServerProvider);
exports.AuthServerProvider = AuthServerProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9hdXRoL2F1dGgtand0LnNlcnZpY2UudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQTJDO0FBQzNDLCtDQUFrRDtBQUNsRCwrQkFBa0M7QUFDbEMsOENBQXFDO0FBQ3JDLG1EQUE0RTtBQUU1RSxxREFBbUQ7QUFRbkQsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFDN0IsWUFBb0IsSUFBZ0IsRUFBVSxhQUFrQyxFQUFVLGVBQXNDO1FBQTVHLFNBQUksR0FBSixJQUFJLENBQVk7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBcUI7UUFBVSxvQkFBZSxHQUFmLGVBQWUsQ0FBdUI7SUFBRyxDQUFDO0lBRXBJLFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDMUgsQ0FBQztJQUVELEtBQUssQ0FBQyxXQUFrQjtRQUN0QixPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsSUFBSSxDQUFXLDhCQUFjLEdBQUcsa0JBQWtCLEVBQUUsV0FBVyxDQUFDO2FBQ2hFLElBQUksQ0FBQyxlQUFHLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELE1BQU07UUFDSixPQUFPLElBQUksaUJBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDbEQsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLG1CQUFtQixDQUFDLFFBQWtCLEVBQUUsVUFBbUI7UUFDakUsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztRQUM5QixJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLHFCQUFxQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7Q0FDRixDQUFBO0FBN0JZLGtCQUFrQjtJQUQ5QixpQkFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFDOzZDQUVQLGlCQUFVLEVBQXlCLG9DQUFtQixFQUEyQixzQ0FBcUI7R0FEckgsa0JBQWtCLENBNkI5QjtBQTdCWSxnREFBa0IiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9hdXRoL2F1dGgtand0LnNlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC13ZWJzdG9yYWdlJztcblxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkwgfSBmcm9tICdhcHAvYXBwLmNvbnN0YW50cyc7XG5pbXBvcnQgeyBMb2dpbiB9IGZyb20gJ2FwcC9jb3JlL2xvZ2luL2xvZ2luLm1vZGVsJztcblxudHlwZSBKd3RUb2tlbiA9IHtcbiAgaWRfdG9rZW46IHN0cmluZztcbn07XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZlclByb3ZpZGVyIHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LCBwcml2YXRlICRsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsIHByaXZhdGUgJHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2UpIHt9XG5cbiAgZ2V0VG9rZW4oKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy4kbG9jYWxTdG9yYWdlLnJldHJpZXZlKCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy4kc2Vzc2lvblN0b3JhZ2UucmV0cmlldmUoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKSB8fCAnJztcbiAgfVxuXG4gIGxvZ2luKGNyZWRlbnRpYWxzOiBMb2dpbik6IE9ic2VydmFibGU8dm9pZD4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5wb3N0PEp3dFRva2VuPihTRVJWRVJfQVBJX1VSTCArICdhcGkvYXV0aGVudGljYXRlJywgY3JlZGVudGlhbHMpXG4gICAgICAucGlwZShtYXAocmVzcG9uc2UgPT4gdGhpcy5hdXRoZW50aWNhdGVTdWNjZXNzKHJlc3BvbnNlLCBjcmVkZW50aWFscy5yZW1lbWJlck1lKSkpO1xuICB9XG5cbiAgbG9nb3V0KCk6IE9ic2VydmFibGU8dm9pZD4ge1xuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XG4gICAgICB0aGlzLiRsb2NhbFN0b3JhZ2UuY2xlYXIoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcbiAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLmNsZWFyKCdhdXRoZW50aWNhdGlvblRva2VuJyk7XG4gICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhdXRoZW50aWNhdGVTdWNjZXNzKHJlc3BvbnNlOiBKd3RUb2tlbiwgcmVtZW1iZXJNZTogYm9vbGVhbik6IHZvaWQge1xuICAgIGNvbnN0IGp3dCA9IHJlc3BvbnNlLmlkX3Rva2VuO1xuICAgIGlmIChyZW1lbWJlck1lKSB7XG4gICAgICB0aGlzLiRsb2NhbFN0b3JhZ2Uuc3RvcmUoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nLCBqd3QpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5zdG9yZSgnYXV0aGVudGljYXRpb25Ub2tlbicsIGp3dCk7XG4gICAgfVxuICB9XG59XG4iXSwidmVyc2lvbiI6M30=