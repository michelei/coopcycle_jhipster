"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const restaurant_model_1 = require("app/shared/model/restaurant.model");
const restaurant_service_1 = require("./restaurant.service");
let RestaurantUpdateComponent = class RestaurantUpdateComponent {
    constructor(restaurantService, activatedRoute, fb) {
        this.restaurantService = restaurantService;
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.isSaving = false;
        this.editForm = this.fb.group({
            id: [],
            restaurantId: [null, [forms_1.Validators.required]],
            name: [null, [forms_1.Validators.required]],
            description: []
        });
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ restaurant }) => {
            this.updateForm(restaurant);
        });
    }
    updateForm(restaurant) {
        this.editForm.patchValue({
            id: restaurant.id,
            restaurantId: restaurant.restaurantId,
            name: restaurant.name,
            description: restaurant.description
        });
    }
    previousState() {
        window.history.back();
    }
    save() {
        this.isSaving = true;
        const restaurant = this.createFromForm();
        if (restaurant.id !== undefined) {
            this.subscribeToSaveResponse(this.restaurantService.update(restaurant));
        }
        else {
            this.subscribeToSaveResponse(this.restaurantService.create(restaurant));
        }
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new restaurant_model_1.Restaurant()), { id: this.editForm.get(['id']).value, restaurantId: this.editForm.get(['restaurantId']).value, name: this.editForm.get(['name']).value, description: this.editForm.get(['description']).value });
    }
    subscribeToSaveResponse(result) {
        result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
    }
};
RestaurantUpdateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-restaurant-update',
        template: require('./restaurant-update.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [restaurant_service_1.RestaurantService, router_1.ActivatedRoute, forms_1.FormBuilder])
], RestaurantUpdateComponent);
exports.RestaurantUpdateComponent = RestaurantUpdateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvcmVzdGF1cmFudC9yZXN0YXVyYW50LXVwZGF0ZS5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBRWxELDZEQUE2RDtBQUM3RCwwQ0FBeUQ7QUFDekQsNENBQWlEO0FBR2pELHdFQUE0RTtBQUM1RSw2REFBeUQ7QUFNekQsSUFBYSx5QkFBeUIsR0FBdEMsTUFBYSx5QkFBeUI7SUFVcEMsWUFBc0IsaUJBQW9DLEVBQVksY0FBOEIsRUFBVSxFQUFlO1FBQXZHLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFBWSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBVDdILGFBQVEsR0FBRyxLQUFLLENBQUM7UUFFakIsYUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLEVBQUUsRUFBRSxFQUFFO1lBQ04sWUFBWSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25DLFdBQVcsRUFBRSxFQUFFO1NBQ2hCLENBQUMsQ0FBQztJQUU2SCxDQUFDO0lBRWpJLFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsVUFBdUI7UUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7WUFDdkIsRUFBRSxFQUFFLFVBQVUsQ0FBQyxFQUFFO1lBQ2pCLFlBQVksRUFBRSxVQUFVLENBQUMsWUFBWTtZQUNyQyxJQUFJLEVBQUUsVUFBVSxDQUFDLElBQUk7WUFDckIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxXQUFXO1NBQ3BDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxhQUFhO1FBQ1gsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN6QyxJQUFJLFVBQVUsQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUFFO1lBQy9CLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7U0FDekU7YUFBTTtZQUNMLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7U0FDekU7SUFDSCxDQUFDO0lBRU8sY0FBYztRQUNwQix1Q0FDSyxJQUFJLDZCQUFVLEVBQUUsS0FDbkIsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUUsQ0FBQyxLQUFLLEVBQ3BDLFlBQVksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFFLENBQUMsS0FBSyxFQUN4RCxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFDeEMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUUsQ0FBQyxLQUFLLElBQ3REO0lBQ0osQ0FBQztJQUVTLHVCQUF1QixDQUFDLE1BQTZDO1FBQzdFLE1BQU0sQ0FBQyxTQUFTLENBQ2QsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxFQUMxQixHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQ3pCLENBQUM7SUFDSixDQUFDO0lBRVMsYUFBYTtRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVTLFdBQVc7UUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztDQUNGLENBQUE7QUFsRVkseUJBQXlCO0lBSnJDLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsdUJBQXVCO1FBQ2pDLGtCQUFhLG9DQUFvQyxDQUFBO0tBQ2xELENBQUM7NkNBV3lDLHNDQUFpQixFQUE0Qix1QkFBYyxFQUFjLG1CQUFXO0dBVmxILHlCQUF5QixDQWtFckM7QUFsRVksOERBQXlCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2VudGl0aWVzL3Jlc3RhdXJhbnQvcmVzdGF1cmFudC11cGRhdGUuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVudXNlZC12YXJzXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgSVJlc3RhdXJhbnQsIFJlc3RhdXJhbnQgfSBmcm9tICdhcHAvc2hhcmVkL21vZGVsL3Jlc3RhdXJhbnQubW9kZWwnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICcuL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1yZXN0YXVyYW50LXVwZGF0ZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9yZXN0YXVyYW50LXVwZGF0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgUmVzdGF1cmFudFVwZGF0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGlzU2F2aW5nID0gZmFsc2U7XG5cbiAgZWRpdEZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICBpZDogW10sXG4gICAgcmVzdGF1cmFudElkOiBbbnVsbCwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdXSxcbiAgICBuYW1lOiBbbnVsbCwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdXSxcbiAgICBkZXNjcmlwdGlvbjogW11cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHJlc3RhdXJhbnRTZXJ2aWNlOiBSZXN0YXVyYW50U2VydmljZSwgcHJvdGVjdGVkIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIpIHt9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5kYXRhLnN1YnNjcmliZSgoeyByZXN0YXVyYW50IH0pID0+IHtcbiAgICAgIHRoaXMudXBkYXRlRm9ybShyZXN0YXVyYW50KTtcbiAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZUZvcm0ocmVzdGF1cmFudDogSVJlc3RhdXJhbnQpOiB2b2lkIHtcbiAgICB0aGlzLmVkaXRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgaWQ6IHJlc3RhdXJhbnQuaWQsXG4gICAgICByZXN0YXVyYW50SWQ6IHJlc3RhdXJhbnQucmVzdGF1cmFudElkLFxuICAgICAgbmFtZTogcmVzdGF1cmFudC5uYW1lLFxuICAgICAgZGVzY3JpcHRpb246IHJlc3RhdXJhbnQuZGVzY3JpcHRpb25cbiAgICB9KTtcbiAgfVxuXG4gIHByZXZpb3VzU3RhdGUoKTogdm9pZCB7XG4gICAgd2luZG93Lmhpc3RvcnkuYmFjaygpO1xuICB9XG5cbiAgc2F2ZSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gdHJ1ZTtcbiAgICBjb25zdCByZXN0YXVyYW50ID0gdGhpcy5jcmVhdGVGcm9tRm9ybSgpO1xuICAgIGlmIChyZXN0YXVyYW50LmlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UodGhpcy5yZXN0YXVyYW50U2VydmljZS51cGRhdGUocmVzdGF1cmFudCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHRoaXMucmVzdGF1cmFudFNlcnZpY2UuY3JlYXRlKHJlc3RhdXJhbnQpKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUZyb21Gb3JtKCk6IElSZXN0YXVyYW50IHtcbiAgICByZXR1cm4ge1xuICAgICAgLi4ubmV3IFJlc3RhdXJhbnQoKSxcbiAgICAgIGlkOiB0aGlzLmVkaXRGb3JtLmdldChbJ2lkJ10pIS52YWx1ZSxcbiAgICAgIHJlc3RhdXJhbnRJZDogdGhpcy5lZGl0Rm9ybS5nZXQoWydyZXN0YXVyYW50SWQnXSkhLnZhbHVlLFxuICAgICAgbmFtZTogdGhpcy5lZGl0Rm9ybS5nZXQoWyduYW1lJ10pIS52YWx1ZSxcbiAgICAgIGRlc2NyaXB0aW9uOiB0aGlzLmVkaXRGb3JtLmdldChbJ2Rlc2NyaXB0aW9uJ10pIS52YWx1ZVxuICAgIH07XG4gIH1cblxuICBwcm90ZWN0ZWQgc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UocmVzdWx0OiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxJUmVzdGF1cmFudD4+KTogdm9pZCB7XG4gICAgcmVzdWx0LnN1YnNjcmliZShcbiAgICAgICgpID0+IHRoaXMub25TYXZlU3VjY2VzcygpLFxuICAgICAgKCkgPT4gdGhpcy5vblNhdmVFcnJvcigpXG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvblNhdmVTdWNjZXNzKCk6IHZvaWQge1xuICAgIHRoaXMuaXNTYXZpbmcgPSBmYWxzZTtcbiAgICB0aGlzLnByZXZpb3VzU3RhdGUoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvblNhdmVFcnJvcigpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2F2aW5nID0gZmFsc2U7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==