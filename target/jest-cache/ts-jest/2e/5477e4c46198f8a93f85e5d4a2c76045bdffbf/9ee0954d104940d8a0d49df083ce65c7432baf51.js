"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const operators_1 = require("rxjs/operators");
const router_1 = require("@angular/router");
const ng_jhipster_1 = require("ng-jhipster");
const pagination_constants_1 = require("app/shared/constants/pagination.constants");
const account_service_1 = require("app/core/auth/account.service");
const user_service_1 = require("app/core/user/user.service");
const user_management_delete_dialog_component_1 = require("./user-management-delete-dialog.component");
let UserManagementComponent = class UserManagementComponent {
    constructor(userService, accountService, activatedRoute, router, eventManager, modalService) {
        this.userService = userService;
        this.accountService = accountService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.eventManager = eventManager;
        this.modalService = modalService;
        this.currentAccount = null;
        this.users = null;
        this.totalItems = 0;
        this.itemsPerPage = pagination_constants_1.ITEMS_PER_PAGE;
    }
    ngOnInit() {
        this.activatedRoute.data
            .pipe(operators_1.flatMap(() => this.accountService.identity(), (data, account) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.ascending = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
            this.currentAccount = account;
            this.loadAll();
            this.userListSubscription = this.eventManager.subscribe('userListModification', () => this.loadAll());
        }))
            .subscribe();
    }
    ngOnDestroy() {
        if (this.userListSubscription) {
            this.eventManager.destroy(this.userListSubscription);
        }
    }
    setActive(user, isActivated) {
        this.userService.update(Object.assign(Object.assign({}, user), { activated: isActivated })).subscribe(() => this.loadAll());
    }
    trackIdentity(index, item) {
        return item.id;
    }
    loadPage(page) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['./'], {
            relativeTo: this.activatedRoute.parent,
            queryParams: {
                page: this.page,
                sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }
    deleteUser(user) {
        const modalRef = this.modalService.open(user_management_delete_dialog_component_1.UserManagementDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.user = user;
    }
    loadAll() {
        this.userService
            .query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        })
            .subscribe((res) => this.onSuccess(res.body, res.headers));
    }
    sort() {
        const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    onSuccess(users, headers) {
        this.totalItems = Number(headers.get('X-Total-Count'));
        this.users = users;
    }
};
UserManagementComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-user-mgmt',
        template: require('./user-management.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [user_service_1.UserService,
        account_service_1.AccountService,
        router_1.ActivatedRoute,
        router_1.Router,
        ng_jhipster_1.JhiEventManager,
        ng_bootstrap_1.NgbModal])
], UserManagementComponent);
exports.UserManagementComponent = UserManagementComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQTZEO0FBRTdELDZEQUFzRDtBQUV0RCw4Q0FBeUM7QUFDekMsNENBQXlEO0FBQ3pELDZDQUE4QztBQUU5QyxvRkFBMkU7QUFDM0UsbUVBQStEO0FBRS9ELDZEQUF5RDtBQUV6RCx1R0FBZ0c7QUFNaEcsSUFBYSx1QkFBdUIsR0FBcEMsTUFBYSx1QkFBdUI7SUFXbEMsWUFDVSxXQUF3QixFQUN4QixjQUE4QixFQUM5QixjQUE4QixFQUM5QixNQUFjLEVBQ2QsWUFBNkIsRUFDN0IsWUFBc0I7UUFMdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQzdCLGlCQUFZLEdBQVosWUFBWSxDQUFVO1FBaEJoQyxtQkFBYyxHQUFtQixJQUFJLENBQUM7UUFDdEMsVUFBSyxHQUFrQixJQUFJLENBQUM7UUFFNUIsZUFBVSxHQUFHLENBQUMsQ0FBQztRQUNmLGlCQUFZLEdBQUcscUNBQWMsQ0FBQztJQWEzQixDQUFDO0lBRUosUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSTthQUNyQixJQUFJLENBQ0gsbUJBQU8sQ0FDTCxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxFQUNwQyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtZQUNoQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDM0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO1lBQzdDLElBQUksQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDO1lBQzlCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNmLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUN4RyxDQUFDLENBQ0YsQ0FDRjthQUNBLFNBQVMsRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7U0FDdEQ7SUFDSCxDQUFDO0lBRUQsU0FBUyxDQUFDLElBQVUsRUFBRSxXQUFvQjtRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0saUNBQU0sSUFBSSxLQUFFLFNBQVMsRUFBRSxXQUFXLElBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDL0YsQ0FBQztJQUVELGFBQWEsQ0FBQyxLQUFhLEVBQUUsSUFBVTtRQUNyQyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFZO1FBQ25CLElBQUksSUFBSSxLQUFLLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzNCLFVBQVUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU07WUFDdEMsV0FBVyxFQUFFO2dCQUNYLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQzthQUMvRDtTQUNGLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNqQixDQUFDO0lBRUQsVUFBVSxDQUFDLElBQVU7UUFDbkIsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsNkVBQW1DLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ2pILFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0lBQ3pDLENBQUM7SUFFTyxPQUFPO1FBQ2IsSUFBSSxDQUFDLFdBQVc7YUFDYixLQUFLLENBQUM7WUFDTCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDO1lBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWTtZQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRTtTQUNsQixDQUFDO2FBQ0QsU0FBUyxDQUFDLENBQUMsR0FBeUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFTyxJQUFJO1FBQ1YsTUFBTSxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxFQUFFO1lBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkI7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRU8sU0FBUyxDQUFDLEtBQW9CLEVBQUUsT0FBb0I7UUFDMUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7Q0FDRixDQUFBO0FBbEdZLHVCQUF1QjtJQUpuQyxnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGVBQWU7UUFDekIsa0JBQWEsa0NBQWtDLENBQUE7S0FDaEQsQ0FBQzs2Q0FhdUIsMEJBQVc7UUFDUixnQ0FBYztRQUNkLHVCQUFjO1FBQ3RCLGVBQU07UUFDQSw2QkFBZTtRQUNmLHVCQUFRO0dBakJyQix1QkFBdUIsQ0FrR25DO0FBbEdZLDBEQUF1QiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi91c2VyLW1hbmFnZW1lbnQvdXNlci1tYW5hZ2VtZW50LmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTmdiTW9kYWwgfSBmcm9tICdAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcCc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGZsYXRNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciB9IGZyb20gJ25nLWpoaXBzdGVyJztcblxuaW1wb3J0IHsgSVRFTVNfUEVSX1BBR0UgfSBmcm9tICdhcHAvc2hhcmVkL2NvbnN0YW50cy9wYWdpbmF0aW9uLmNvbnN0YW50cyc7XG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gJ2FwcC9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcbmltcG9ydCB7IEFjY291bnQgfSBmcm9tICdhcHAvY29yZS91c2VyL2FjY291bnQubW9kZWwnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnYXBwL2NvcmUvdXNlci91c2VyLm1vZGVsJztcbmltcG9ydCB7IFVzZXJNYW5hZ2VtZW50RGVsZXRlRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi91c2VyLW1hbmFnZW1lbnQtZGVsZXRlLWRpYWxvZy5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktdXNlci1tZ210JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3VzZXItbWFuYWdlbWVudC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVXNlck1hbmFnZW1lbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIGN1cnJlbnRBY2NvdW50OiBBY2NvdW50IHwgbnVsbCA9IG51bGw7XG4gIHVzZXJzOiBVc2VyW10gfCBudWxsID0gbnVsbDtcbiAgdXNlckxpc3RTdWJzY3JpcHRpb24/OiBTdWJzY3JpcHRpb247XG4gIHRvdGFsSXRlbXMgPSAwO1xuICBpdGVtc1BlclBhZ2UgPSBJVEVNU19QRVJfUEFHRTtcbiAgcGFnZSE6IG51bWJlcjtcbiAgcHJlZGljYXRlITogc3RyaW5nO1xuICBwcmV2aW91c1BhZ2UhOiBudW1iZXI7XG4gIGFzY2VuZGluZyE6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIGV2ZW50TWFuYWdlcjogSmhpRXZlbnRNYW5hZ2VyLFxuICAgIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBOZ2JNb2RhbFxuICApIHt9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5kYXRhXG4gICAgICAucGlwZShcbiAgICAgICAgZmxhdE1hcChcbiAgICAgICAgICAoKSA9PiB0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KCksXG4gICAgICAgICAgKGRhdGEsIGFjY291bnQpID0+IHtcbiAgICAgICAgICAgIHRoaXMucGFnZSA9IGRhdGEucGFnaW5nUGFyYW1zLnBhZ2U7XG4gICAgICAgICAgICB0aGlzLnByZXZpb3VzUGFnZSA9IGRhdGEucGFnaW5nUGFyYW1zLnBhZ2U7XG4gICAgICAgICAgICB0aGlzLmFzY2VuZGluZyA9IGRhdGEucGFnaW5nUGFyYW1zLmFzY2VuZGluZztcbiAgICAgICAgICAgIHRoaXMucHJlZGljYXRlID0gZGF0YS5wYWdpbmdQYXJhbXMucHJlZGljYXRlO1xuICAgICAgICAgICAgdGhpcy5jdXJyZW50QWNjb3VudCA9IGFjY291bnQ7XG4gICAgICAgICAgICB0aGlzLmxvYWRBbGwoKTtcbiAgICAgICAgICAgIHRoaXMudXNlckxpc3RTdWJzY3JpcHRpb24gPSB0aGlzLmV2ZW50TWFuYWdlci5zdWJzY3JpYmUoJ3VzZXJMaXN0TW9kaWZpY2F0aW9uJywgKCkgPT4gdGhpcy5sb2FkQWxsKCkpO1xuICAgICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgKVxuICAgICAgLnN1YnNjcmliZSgpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMudXNlckxpc3RTdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmRlc3Ryb3kodGhpcy51c2VyTGlzdFN1YnNjcmlwdGlvbik7XG4gICAgfVxuICB9XG5cbiAgc2V0QWN0aXZlKHVzZXI6IFVzZXIsIGlzQWN0aXZhdGVkOiBib29sZWFuKTogdm9pZCB7XG4gICAgdGhpcy51c2VyU2VydmljZS51cGRhdGUoeyAuLi51c2VyLCBhY3RpdmF0ZWQ6IGlzQWN0aXZhdGVkIH0pLnN1YnNjcmliZSgoKSA9PiB0aGlzLmxvYWRBbGwoKSk7XG4gIH1cblxuICB0cmFja0lkZW50aXR5KGluZGV4OiBudW1iZXIsIGl0ZW06IFVzZXIpOiBhbnkge1xuICAgIHJldHVybiBpdGVtLmlkO1xuICB9XG5cbiAgbG9hZFBhZ2UocGFnZTogbnVtYmVyKTogdm9pZCB7XG4gICAgaWYgKHBhZ2UgIT09IHRoaXMucHJldmlvdXNQYWdlKSB7XG4gICAgICB0aGlzLnByZXZpb3VzUGFnZSA9IHBhZ2U7XG4gICAgICB0aGlzLnRyYW5zaXRpb24oKTtcbiAgICB9XG4gIH1cblxuICB0cmFuc2l0aW9uKCk6IHZvaWQge1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi8nXSwge1xuICAgICAgcmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJlbnQsXG4gICAgICBxdWVyeVBhcmFtczoge1xuICAgICAgICBwYWdlOiB0aGlzLnBhZ2UsXG4gICAgICAgIHNvcnQ6IHRoaXMucHJlZGljYXRlICsgJywnICsgKHRoaXMuYXNjZW5kaW5nID8gJ2FzYycgOiAnZGVzYycpXG4gICAgICB9XG4gICAgfSk7XG4gICAgdGhpcy5sb2FkQWxsKCk7XG4gIH1cblxuICBkZWxldGVVc2VyKHVzZXI6IFVzZXIpOiB2b2lkIHtcbiAgICBjb25zdCBtb2RhbFJlZiA9IHRoaXMubW9kYWxTZXJ2aWNlLm9wZW4oVXNlck1hbmFnZW1lbnREZWxldGVEaWFsb2dDb21wb25lbnQsIHsgc2l6ZTogJ2xnJywgYmFja2Ryb3A6ICdzdGF0aWMnIH0pO1xuICAgIG1vZGFsUmVmLmNvbXBvbmVudEluc3RhbmNlLnVzZXIgPSB1c2VyO1xuICB9XG5cbiAgcHJpdmF0ZSBsb2FkQWxsKCk6IHZvaWQge1xuICAgIHRoaXMudXNlclNlcnZpY2VcbiAgICAgIC5xdWVyeSh7XG4gICAgICAgIHBhZ2U6IHRoaXMucGFnZSAtIDEsXG4gICAgICAgIHNpemU6IHRoaXMuaXRlbXNQZXJQYWdlLFxuICAgICAgICBzb3J0OiB0aGlzLnNvcnQoKVxuICAgICAgfSlcbiAgICAgIC5zdWJzY3JpYmUoKHJlczogSHR0cFJlc3BvbnNlPFVzZXJbXT4pID0+IHRoaXMub25TdWNjZXNzKHJlcy5ib2R5LCByZXMuaGVhZGVycykpO1xuICB9XG5cbiAgcHJpdmF0ZSBzb3J0KCk6IHN0cmluZ1tdIHtcbiAgICBjb25zdCByZXN1bHQgPSBbdGhpcy5wcmVkaWNhdGUgKyAnLCcgKyAodGhpcy5hc2NlbmRpbmcgPyAnYXNjJyA6ICdkZXNjJyldO1xuICAgIGlmICh0aGlzLnByZWRpY2F0ZSAhPT0gJ2lkJykge1xuICAgICAgcmVzdWx0LnB1c2goJ2lkJyk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cblxuICBwcml2YXRlIG9uU3VjY2Vzcyh1c2VyczogVXNlcltdIHwgbnVsbCwgaGVhZGVyczogSHR0cEhlYWRlcnMpOiB2b2lkIHtcbiAgICB0aGlzLnRvdGFsSXRlbXMgPSBOdW1iZXIoaGVhZGVycy5nZXQoJ1gtVG90YWwtQ291bnQnKSk7XG4gICAgdGhpcy51c2VycyA9IHVzZXJzO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=