"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const operators_1 = require("rxjs/operators");
const login_modal_service_1 = require("app/core/login/login-modal.service");
const activate_service_1 = require("./activate.service");
let ActivateComponent = class ActivateComponent {
    constructor(activateService, loginModalService, route) {
        this.activateService = activateService;
        this.loginModalService = loginModalService;
        this.route = route;
        this.error = false;
        this.success = false;
    }
    ngOnInit() {
        this.route.queryParams.pipe(operators_1.flatMap(params => this.activateService.get(params.key))).subscribe(() => (this.success = true), () => (this.error = true));
    }
    login() {
        this.loginModalService.open();
    }
};
ActivateComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-activate',
        template: require('./activate.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [activate_service_1.ActivateService, login_modal_service_1.LoginModalService, router_1.ActivatedRoute])
], ActivateComponent);
exports.ActivateComponent = ActivateComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBQ2xELDRDQUFpRDtBQUNqRCw4Q0FBeUM7QUFFekMsNEVBQXVFO0FBQ3ZFLHlEQUFxRDtBQU1yRCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUk1QixZQUFvQixlQUFnQyxFQUFVLGlCQUFvQyxFQUFVLEtBQXFCO1FBQTdHLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUhqSSxVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2QsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUVvSCxDQUFDO0lBRXJJLFFBQVE7UUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUM1RixHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQzNCLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FDMUIsQ0FBQztJQUNKLENBQUM7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hDLENBQUM7Q0FDRixDQUFBO0FBaEJZLGlCQUFpQjtJQUo3QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7UUFDeEIsa0JBQWEsMkJBQTJCLENBQUE7S0FDekMsQ0FBQzs2Q0FLcUMsa0NBQWUsRUFBNkIsdUNBQWlCLEVBQWlCLHVCQUFjO0dBSnRILGlCQUFpQixDQWdCN0I7QUFoQlksOENBQWlCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FjY291bnQvYWN0aXZhdGUvYWN0aXZhdGUuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBmbGF0TWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5pbXBvcnQgeyBMb2dpbk1vZGFsU2VydmljZSB9IGZyb20gJ2FwcC9jb3JlL2xvZ2luL2xvZ2luLW1vZGFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0aXZhdGVTZXJ2aWNlIH0gZnJvbSAnLi9hY3RpdmF0ZS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLWFjdGl2YXRlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2FjdGl2YXRlLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBBY3RpdmF0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGVycm9yID0gZmFsc2U7XG4gIHN1Y2Nlc3MgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFjdGl2YXRlU2VydmljZTogQWN0aXZhdGVTZXJ2aWNlLCBwcml2YXRlIGxvZ2luTW9kYWxTZXJ2aWNlOiBMb2dpbk1vZGFsU2VydmljZSwgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHt9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5yb3V0ZS5xdWVyeVBhcmFtcy5waXBlKGZsYXRNYXAocGFyYW1zID0+IHRoaXMuYWN0aXZhdGVTZXJ2aWNlLmdldChwYXJhbXMua2V5KSkpLnN1YnNjcmliZShcbiAgICAgICgpID0+ICh0aGlzLnN1Y2Nlc3MgPSB0cnVlKSxcbiAgICAgICgpID0+ICh0aGlzLmVycm9yID0gdHJ1ZSlcbiAgICApO1xuICB9XG5cbiAgbG9naW4oKTogdm9pZCB7XG4gICAgdGhpcy5sb2dpbk1vZGFsU2VydmljZS5vcGVuKCk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==