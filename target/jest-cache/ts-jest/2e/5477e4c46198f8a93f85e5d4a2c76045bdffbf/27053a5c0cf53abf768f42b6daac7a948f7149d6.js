"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Audit {
    constructor(data, principal, timestamp, type) {
        this.data = data;
        this.principal = principal;
        this.timestamp = timestamp;
        this.type = type;
    }
}
exports.Audit = Audit;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vYXVkaXRzL2F1ZGl0Lm1vZGVsLnRzIiwibWFwcGluZ3MiOiI7O0FBRUEsTUFBYSxLQUFLO0lBQ2hCLFlBQW1CLElBQWUsRUFBUyxTQUFpQixFQUFTLFNBQWlCLEVBQVMsSUFBWTtRQUF4RixTQUFJLEdBQUosSUFBSSxDQUFXO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUFTLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFBUyxTQUFJLEdBQUosSUFBSSxDQUFRO0lBQUcsQ0FBQztDQUNoSDtBQUZELHNCQUVDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FkbWluL2F1ZGl0cy9hdWRpdC5tb2RlbC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdWRpdERhdGEgfSBmcm9tICcuL2F1ZGl0LWRhdGEubW9kZWwnO1xuXG5leHBvcnQgY2xhc3MgQXVkaXQge1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgZGF0YTogQXVkaXREYXRhLCBwdWJsaWMgcHJpbmNpcGFsOiBzdHJpbmcsIHB1YmxpYyB0aW1lc3RhbXA6IHN0cmluZywgcHVibGljIHR5cGU6IHN0cmluZykge31cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==