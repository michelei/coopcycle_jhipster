"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const restaurant_detail_component_1 = require("app/entities/restaurant/restaurant-detail.component");
const restaurant_model_1 = require("app/shared/model/restaurant.model");
describe('Component Tests', () => {
    describe('Restaurant Management Detail Component', () => {
        let comp;
        let fixture;
        const route = { data: rxjs_1.of({ restaurant: new restaurant_model_1.Restaurant(123) }) };
        beforeEach(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [restaurant_detail_component_1.RestaurantDetailComponent],
                providers: [{ provide: router_1.ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(restaurant_detail_component_1.RestaurantDetailComponent, '')
                .compileComponents();
            fixture = testing_1.TestBed.createComponent(restaurant_detail_component_1.RestaurantDetailComponent);
            comp = fixture.componentInstance;
        });
        describe('OnInit', () => {
            it('Should load restaurant on init', () => {
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(comp.restaurant).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvZW50aXRpZXMvcmVzdGF1cmFudC9yZXN0YXVyYW50LWRldGFpbC5jb21wb25lbnQuc3BlYy50cyIsIm1hcHBpbmdzIjoiOztBQUFBLG1EQUFrRTtBQUNsRSw0Q0FBaUQ7QUFDakQsK0JBQTBCO0FBRTFCLHNEQUEyRDtBQUMzRCxxR0FBZ0c7QUFDaEcsd0VBQStEO0FBRS9ELFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7SUFDL0IsUUFBUSxDQUFDLHdDQUF3QyxFQUFFLEdBQUcsRUFBRTtRQUN0RCxJQUFJLElBQStCLENBQUM7UUFDcEMsSUFBSSxPQUFvRCxDQUFDO1FBQ3pELE1BQU0sS0FBSyxHQUFJLEVBQUUsSUFBSSxFQUFFLFNBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxJQUFJLDZCQUFVLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUE0QixDQUFDO1FBRTNGLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUM3QixPQUFPLEVBQUUsQ0FBQyxpQ0FBbUIsQ0FBQztnQkFDOUIsWUFBWSxFQUFFLENBQUMsdURBQXlCLENBQUM7Z0JBQ3pDLFNBQVMsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLHVCQUFjLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQzFELENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMsdURBQXlCLEVBQUUsRUFBRSxDQUFDO2lCQUMvQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3ZCLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyx1REFBeUIsQ0FBQyxDQUFDO1lBQzdELElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtZQUN0QixFQUFFLENBQUMsZ0NBQWdDLEVBQUUsR0FBRyxFQUFFO2dCQUN4QyxPQUFPO2dCQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFFaEIsT0FBTztnQkFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2VudGl0aWVzL3Jlc3RhdXJhbnQvcmVzdGF1cmFudC1kZXRhaWwuY29tcG9uZW50LnNwZWMudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50Rml4dHVyZSwgVGVzdEJlZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBvZiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBDb29wY3ljbGVUZXN0TW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vdGVzdC5tb2R1bGUnO1xuaW1wb3J0IHsgUmVzdGF1cmFudERldGFpbENvbXBvbmVudCB9IGZyb20gJ2FwcC9lbnRpdGllcy9yZXN0YXVyYW50L3Jlc3RhdXJhbnQtZGV0YWlsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZXN0YXVyYW50IH0gZnJvbSAnYXBwL3NoYXJlZC9tb2RlbC9yZXN0YXVyYW50Lm1vZGVsJztcblxuZGVzY3JpYmUoJ0NvbXBvbmVudCBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ1Jlc3RhdXJhbnQgTWFuYWdlbWVudCBEZXRhaWwgQ29tcG9uZW50JywgKCkgPT4ge1xuICAgIGxldCBjb21wOiBSZXN0YXVyYW50RGV0YWlsQ29tcG9uZW50O1xuICAgIGxldCBmaXh0dXJlOiBDb21wb25lbnRGaXh0dXJlPFJlc3RhdXJhbnREZXRhaWxDb21wb25lbnQ+O1xuICAgIGNvbnN0IHJvdXRlID0gKHsgZGF0YTogb2YoeyByZXN0YXVyYW50OiBuZXcgUmVzdGF1cmFudCgxMjMpIH0pIH0gYXMgYW55KSBhcyBBY3RpdmF0ZWRSb3V0ZTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgVGVzdEJlZC5jb25maWd1cmVUZXN0aW5nTW9kdWxlKHtcbiAgICAgICAgaW1wb3J0czogW0Nvb3BjeWNsZVRlc3RNb2R1bGVdLFxuICAgICAgICBkZWNsYXJhdGlvbnM6IFtSZXN0YXVyYW50RGV0YWlsQ29tcG9uZW50XSxcbiAgICAgICAgcHJvdmlkZXJzOiBbeyBwcm92aWRlOiBBY3RpdmF0ZWRSb3V0ZSwgdXNlVmFsdWU6IHJvdXRlIH1dXG4gICAgICB9KVxuICAgICAgICAub3ZlcnJpZGVUZW1wbGF0ZShSZXN0YXVyYW50RGV0YWlsQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgICBmaXh0dXJlID0gVGVzdEJlZC5jcmVhdGVDb21wb25lbnQoUmVzdGF1cmFudERldGFpbENvbXBvbmVudCk7XG4gICAgICBjb21wID0gZml4dHVyZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdPbkluaXQnLCAoKSA9PiB7XG4gICAgICBpdCgnU2hvdWxkIGxvYWQgcmVzdGF1cmFudCBvbiBpbml0JywgKCkgPT4ge1xuICAgICAgICAvLyBXSEVOXG4gICAgICAgIGNvbXAubmdPbkluaXQoKTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdChjb21wLnJlc3RhdXJhbnQpLnRvRXF1YWwoamFzbWluZS5vYmplY3RDb250YWluaW5nKHsgaWQ6IDEyMyB9KSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfSk7XG59KTtcbiJdLCJ2ZXJzaW9uIjozfQ==