"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const testing_2 = require("@angular/common/http/testing");
const order_service_1 = require("app/entities/order/order.service");
const order_model_1 = require("app/shared/model/order.model");
describe('Service Tests', () => {
    describe('Order Service', () => {
        let injector;
        let service;
        let httpMock;
        let elemDefault;
        let expectedResult;
        beforeEach(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [testing_2.HttpClientTestingModule]
            });
            expectedResult = null;
            injector = testing_1.getTestBed();
            service = injector.get(order_service_1.OrderService);
            httpMock = injector.get(testing_2.HttpTestingController);
            elemDefault = new order_model_1.Order(0, 0);
        });
        describe('Service methods', () => {
            it('should find an element', () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service.find(123).subscribe(resp => (expectedResult = resp.body));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(returnedFromService);
                expect(expectedResult).toMatchObject(elemDefault);
            });
            it('should create a Order', () => {
                const returnedFromService = Object.assign({
                    id: 0
                }, elemDefault);
                const expected = Object.assign({}, returnedFromService);
                service.create(new order_model_1.Order()).subscribe(resp => (expectedResult = resp.body));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(returnedFromService);
                expect(expectedResult).toMatchObject(expected);
            });
            it('should update a Order', () => {
                const returnedFromService = Object.assign({
                    orderId: 1
                }, elemDefault);
                const expected = Object.assign({}, returnedFromService);
                service.update(expected).subscribe(resp => (expectedResult = resp.body));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(returnedFromService);
                expect(expectedResult).toMatchObject(expected);
            });
            it('should return a list of Order', () => {
                const returnedFromService = Object.assign({
                    orderId: 1
                }, elemDefault);
                const expected = Object.assign({}, returnedFromService);
                service.query().subscribe(resp => (expectedResult = resp.body));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush([returnedFromService]);
                httpMock.verify();
                expect(expectedResult).toContainEqual(expected);
            });
            it('should delete a Order', () => {
                service.delete(123).subscribe(resp => (expectedResult = resp.ok));
                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
                expect(expectedResult);
            });
        });
        afterEach(() => {
            httpMock.verify();
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvZW50aXRpZXMvb3JkZXIvb3JkZXIuc2VydmljZS5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsbURBQTREO0FBQzVELDBEQUE4RjtBQUM5RixvRUFBZ0U7QUFDaEUsOERBQTZEO0FBRTdELFFBQVEsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFO0lBQzdCLFFBQVEsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFO1FBQzdCLElBQUksUUFBaUIsQ0FBQztRQUN0QixJQUFJLE9BQXFCLENBQUM7UUFDMUIsSUFBSSxRQUErQixDQUFDO1FBQ3BDLElBQUksV0FBbUIsQ0FBQztRQUN4QixJQUFJLGNBQWtELENBQUM7UUFFdkQsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUF1QixDQUFDO2FBQ25DLENBQUMsQ0FBQztZQUNILGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDdEIsUUFBUSxHQUFHLG9CQUFVLEVBQUUsQ0FBQztZQUN4QixPQUFPLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyw0QkFBWSxDQUFDLENBQUM7WUFDckMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsK0JBQXFCLENBQUMsQ0FBQztZQUUvQyxXQUFXLEdBQUcsSUFBSSxtQkFBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7WUFDL0IsRUFBRSxDQUFDLHdCQUF3QixFQUFFLEdBQUcsRUFBRTtnQkFDaEMsTUFBTSxtQkFBbUIsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFFM0QsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFFbEUsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxHQUFHLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDcEQsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsdUJBQXVCLEVBQUUsR0FBRyxFQUFFO2dCQUMvQixNQUFNLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQ3ZDO29CQUNFLEVBQUUsRUFBRSxDQUFDO2lCQUNOLEVBQ0QsV0FBVyxDQUNaLENBQUM7Z0JBRUYsTUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztnQkFFeEQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLG1CQUFLLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUU1RSxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ25ELEdBQUcsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDL0IsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLEVBQUU7Z0JBQy9CLE1BQU0sbUJBQW1CLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FDdkM7b0JBQ0UsT0FBTyxFQUFFLENBQUM7aUJBQ1gsRUFDRCxXQUFXLENBQ1osQ0FBQztnQkFFRixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO2dCQUV4RCxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUV6RSxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xELEdBQUcsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDL0IsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQywrQkFBK0IsRUFBRSxHQUFHLEVBQUU7Z0JBQ3ZDLE1BQU0sbUJBQW1CLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FDdkM7b0JBQ0UsT0FBTyxFQUFFLENBQUM7aUJBQ1gsRUFDRCxXQUFXLENBQ1osQ0FBQztnQkFFRixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO2dCQUV4RCxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBRWhFLE1BQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztnQkFDakMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNsQixNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2xELENBQUMsQ0FBQyxDQUFDO1lBRUgsRUFBRSxDQUFDLHVCQUF1QixFQUFFLEdBQUcsRUFBRTtnQkFDL0IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFbEUsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dCQUNyRCxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN6QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNiLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvZW50aXRpZXMvb3JkZXIvb3JkZXIuc2VydmljZS5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRlc3RCZWQsIGdldFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgSHR0cENsaWVudFRlc3RpbmdNb2R1bGUsIEh0dHBUZXN0aW5nQ29udHJvbGxlciB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwL3Rlc3RpbmcnO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnYXBwL2VudGl0aWVzL29yZGVyL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgSU9yZGVyLCBPcmRlciB9IGZyb20gJ2FwcC9zaGFyZWQvbW9kZWwvb3JkZXIubW9kZWwnO1xuXG5kZXNjcmliZSgnU2VydmljZSBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ09yZGVyIFNlcnZpY2UnLCAoKSA9PiB7XG4gICAgbGV0IGluamVjdG9yOiBUZXN0QmVkO1xuICAgIGxldCBzZXJ2aWNlOiBPcmRlclNlcnZpY2U7XG4gICAgbGV0IGh0dHBNb2NrOiBIdHRwVGVzdGluZ0NvbnRyb2xsZXI7XG4gICAgbGV0IGVsZW1EZWZhdWx0OiBJT3JkZXI7XG4gICAgbGV0IGV4cGVjdGVkUmVzdWx0OiBJT3JkZXIgfCBJT3JkZXJbXSB8IGJvb2xlYW4gfCBudWxsO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbSHR0cENsaWVudFRlc3RpbmdNb2R1bGVdXG4gICAgICB9KTtcbiAgICAgIGV4cGVjdGVkUmVzdWx0ID0gbnVsbDtcbiAgICAgIGluamVjdG9yID0gZ2V0VGVzdEJlZCgpO1xuICAgICAgc2VydmljZSA9IGluamVjdG9yLmdldChPcmRlclNlcnZpY2UpO1xuICAgICAgaHR0cE1vY2sgPSBpbmplY3Rvci5nZXQoSHR0cFRlc3RpbmdDb250cm9sbGVyKTtcblxuICAgICAgZWxlbURlZmF1bHQgPSBuZXcgT3JkZXIoMCwgMCk7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnU2VydmljZSBtZXRob2RzJywgKCkgPT4ge1xuICAgICAgaXQoJ3Nob3VsZCBmaW5kIGFuIGVsZW1lbnQnLCAoKSA9PiB7XG4gICAgICAgIGNvbnN0IHJldHVybmVkRnJvbVNlcnZpY2UgPSBPYmplY3QuYXNzaWduKHt9LCBlbGVtRGVmYXVsdCk7XG5cbiAgICAgICAgc2VydmljZS5maW5kKDEyMykuc3Vic2NyaWJlKHJlc3AgPT4gKGV4cGVjdGVkUmVzdWx0ID0gcmVzcC5ib2R5KSk7XG5cbiAgICAgICAgY29uc3QgcmVxID0gaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KTtcbiAgICAgICAgcmVxLmZsdXNoKHJldHVybmVkRnJvbVNlcnZpY2UpO1xuICAgICAgICBleHBlY3QoZXhwZWN0ZWRSZXN1bHQpLnRvTWF0Y2hPYmplY3QoZWxlbURlZmF1bHQpO1xuICAgICAgfSk7XG5cbiAgICAgIGl0KCdzaG91bGQgY3JlYXRlIGEgT3JkZXInLCAoKSA9PiB7XG4gICAgICAgIGNvbnN0IHJldHVybmVkRnJvbVNlcnZpY2UgPSBPYmplY3QuYXNzaWduKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGlkOiAwXG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbGVtRGVmYXVsdFxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IGV4cGVjdGVkID0gT2JqZWN0LmFzc2lnbih7fSwgcmV0dXJuZWRGcm9tU2VydmljZSk7XG5cbiAgICAgICAgc2VydmljZS5jcmVhdGUobmV3IE9yZGVyKCkpLnN1YnNjcmliZShyZXNwID0+IChleHBlY3RlZFJlc3VsdCA9IHJlc3AuYm9keSkpO1xuXG4gICAgICAgIGNvbnN0IHJlcSA9IGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ1BPU1QnIH0pO1xuICAgICAgICByZXEuZmx1c2gocmV0dXJuZWRGcm9tU2VydmljZSk7XG4gICAgICAgIGV4cGVjdChleHBlY3RlZFJlc3VsdCkudG9NYXRjaE9iamVjdChleHBlY3RlZCk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCB1cGRhdGUgYSBPcmRlcicsICgpID0+IHtcbiAgICAgICAgY29uc3QgcmV0dXJuZWRGcm9tU2VydmljZSA9IE9iamVjdC5hc3NpZ24oXG4gICAgICAgICAge1xuICAgICAgICAgICAgb3JkZXJJZDogMVxuICAgICAgICAgIH0sXG4gICAgICAgICAgZWxlbURlZmF1bHRcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCBleHBlY3RlZCA9IE9iamVjdC5hc3NpZ24oe30sIHJldHVybmVkRnJvbVNlcnZpY2UpO1xuXG4gICAgICAgIHNlcnZpY2UudXBkYXRlKGV4cGVjdGVkKS5zdWJzY3JpYmUocmVzcCA9PiAoZXhwZWN0ZWRSZXN1bHQgPSByZXNwLmJvZHkpKTtcblxuICAgICAgICBjb25zdCByZXEgPSBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdQVVQnIH0pO1xuICAgICAgICByZXEuZmx1c2gocmV0dXJuZWRGcm9tU2VydmljZSk7XG4gICAgICAgIGV4cGVjdChleHBlY3RlZFJlc3VsdCkudG9NYXRjaE9iamVjdChleHBlY3RlZCk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCByZXR1cm4gYSBsaXN0IG9mIE9yZGVyJywgKCkgPT4ge1xuICAgICAgICBjb25zdCByZXR1cm5lZEZyb21TZXJ2aWNlID0gT2JqZWN0LmFzc2lnbihcbiAgICAgICAgICB7XG4gICAgICAgICAgICBvcmRlcklkOiAxXG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbGVtRGVmYXVsdFxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IGV4cGVjdGVkID0gT2JqZWN0LmFzc2lnbih7fSwgcmV0dXJuZWRGcm9tU2VydmljZSk7XG5cbiAgICAgICAgc2VydmljZS5xdWVyeSgpLnN1YnNjcmliZShyZXNwID0+IChleHBlY3RlZFJlc3VsdCA9IHJlc3AuYm9keSkpO1xuXG4gICAgICAgIGNvbnN0IHJlcSA9IGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSk7XG4gICAgICAgIHJlcS5mbHVzaChbcmV0dXJuZWRGcm9tU2VydmljZV0pO1xuICAgICAgICBodHRwTW9jay52ZXJpZnkoKTtcbiAgICAgICAgZXhwZWN0KGV4cGVjdGVkUmVzdWx0KS50b0NvbnRhaW5FcXVhbChleHBlY3RlZCk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCBkZWxldGUgYSBPcmRlcicsICgpID0+IHtcbiAgICAgICAgc2VydmljZS5kZWxldGUoMTIzKS5zdWJzY3JpYmUocmVzcCA9PiAoZXhwZWN0ZWRSZXN1bHQgPSByZXNwLm9rKSk7XG5cbiAgICAgICAgY29uc3QgcmVxID0gaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnREVMRVRFJyB9KTtcbiAgICAgICAgcmVxLmZsdXNoKHsgc3RhdHVzOiAyMDAgfSk7XG4gICAgICAgIGV4cGVjdChleHBlY3RlZFJlc3VsdCk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIGFmdGVyRWFjaCgoKSA9PiB7XG4gICAgICBodHRwTW9jay52ZXJpZnkoKTtcbiAgICB9KTtcbiAgfSk7XG59KTtcbiJdLCJ2ZXJzaW9uIjozfQ==