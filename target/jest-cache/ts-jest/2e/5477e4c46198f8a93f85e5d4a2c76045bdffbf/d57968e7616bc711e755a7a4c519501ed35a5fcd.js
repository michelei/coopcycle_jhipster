"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const login_component_1 = require("app/shared/login/login.component");
let LoginModalService = class LoginModalService {
    constructor(modalService) {
        this.modalService = modalService;
        this.isOpen = false;
    }
    open() {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(login_component_1.LoginModalComponent);
        modalRef.result.finally(() => (this.isOpen = false));
    }
};
LoginModalService = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [ng_bootstrap_1.NgbModal])
], LoginModalService);
exports.LoginModalService = LoginModalService;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9sb2dpbi9sb2dpbi1tb2RhbC5zZXJ2aWNlLnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUEyQztBQUMzQyw2REFBbUU7QUFFbkUsc0VBQXVFO0FBR3ZFLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRzVCLFlBQW9CLFlBQXNCO1FBQXRCLGlCQUFZLEdBQVosWUFBWSxDQUFVO1FBRmxDLFdBQU0sR0FBRyxLQUFLLENBQUM7SUFFc0IsQ0FBQztJQUU5QyxJQUFJO1FBQ0YsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsTUFBTSxRQUFRLEdBQWdCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLHFDQUFtQixDQUFDLENBQUM7UUFDMUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztDQUNGLENBQUE7QUFiWSxpQkFBaUI7SUFEN0IsaUJBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzs2Q0FJQyx1QkFBUTtHQUgvQixpQkFBaUIsQ0FhN0I7QUFiWSw4Q0FBaUIiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9sb2dpbi9sb2dpbi1tb2RhbC5zZXJ2aWNlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5nYk1vZGFsLCBOZ2JNb2RhbFJlZiB9IGZyb20gJ0BuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwJztcblxuaW1wb3J0IHsgTG9naW5Nb2RhbENvbXBvbmVudCB9IGZyb20gJ2FwcC9zaGFyZWQvbG9naW4vbG9naW4uY29tcG9uZW50JztcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBMb2dpbk1vZGFsU2VydmljZSB7XG4gIHByaXZhdGUgaXNPcGVuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2RhbFNlcnZpY2U6IE5nYk1vZGFsKSB7fVxuXG4gIG9wZW4oKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuaXNPcGVuKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuaXNPcGVuID0gdHJ1ZTtcbiAgICBjb25zdCBtb2RhbFJlZjogTmdiTW9kYWxSZWYgPSB0aGlzLm1vZGFsU2VydmljZS5vcGVuKExvZ2luTW9kYWxDb21wb25lbnQpO1xuICAgIG1vZGFsUmVmLnJlc3VsdC5maW5hbGx5KCgpID0+ICh0aGlzLmlzT3BlbiA9IGZhbHNlKSk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==