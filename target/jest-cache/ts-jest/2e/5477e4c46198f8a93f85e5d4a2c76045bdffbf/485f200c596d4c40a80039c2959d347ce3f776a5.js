"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@angular/common");
const router_1 = require("@angular/router");
const core_1 = require("@angular/core");
const testing_1 = require("@angular/common/http/testing");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const ng_jhipster_1 = require("ng-jhipster");
const mock_language_service_1 = require("./helpers/mock-language.service");
const account_service_1 = require("app/core/auth/account.service");
const login_modal_service_1 = require("app/core/login/login-modal.service");
const mock_account_service_1 = require("./helpers/mock-account.service");
const mock_route_service_1 = require("./helpers/mock-route.service");
const mock_active_modal_service_1 = require("./helpers/mock-active-modal.service");
const mock_event_manager_service_1 = require("./helpers/mock-event-manager.service");
let CoopcycleTestModule = class CoopcycleTestModule {
};
CoopcycleTestModule = tslib_1.__decorate([
    core_1.NgModule({
        providers: [
            common_1.DatePipe,
            ng_jhipster_1.JhiDataUtils,
            ng_jhipster_1.JhiDateUtils,
            ng_jhipster_1.JhiParseLinks,
            {
                provide: ng_jhipster_1.JhiLanguageService,
                useClass: mock_language_service_1.MockLanguageService
            },
            {
                provide: ng_jhipster_1.JhiEventManager,
                useClass: mock_event_manager_service_1.MockEventManager
            },
            {
                provide: ng_bootstrap_1.NgbActiveModal,
                useClass: mock_active_modal_service_1.MockActiveModal
            },
            {
                provide: router_1.ActivatedRoute,
                useValue: new mock_route_service_1.MockActivatedRoute({ id: 123 })
            },
            {
                provide: router_1.Router,
                useClass: mock_route_service_1.MockRouter
            },
            {
                provide: account_service_1.AccountService,
                useClass: mock_account_service_1.MockAccountService
            },
            {
                provide: login_modal_service_1.LoginModalService,
                useValue: null
            },
            {
                provide: ng_jhipster_1.JhiAlertService,
                useValue: null
            },
            {
                provide: ng_bootstrap_1.NgbModal,
                useValue: null
            }
        ],
        imports: [testing_1.HttpClientTestingModule]
    })
], CoopcycleTestModule);
exports.CoopcycleTestModule = CoopcycleTestModule;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy90ZXN0Lm1vZHVsZS50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSw0Q0FBMkM7QUFDM0MsNENBQXlEO0FBQ3pELHdDQUF5QztBQUN6QywwREFBdUU7QUFDdkUsNkRBQXNFO0FBQ3RFLDZDQUE4SDtBQUU5SCwyRUFBc0U7QUFDdEUsbUVBQStEO0FBQy9ELDRFQUF1RTtBQUN2RSx5RUFBb0U7QUFDcEUscUVBQThFO0FBQzlFLG1GQUFzRTtBQUN0RSxxRkFBd0U7QUErQ3hFLElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0NBQUcsQ0FBQTtBQUF0QixtQkFBbUI7SUE3Qy9CLGVBQVEsQ0FBQztRQUNSLFNBQVMsRUFBRTtZQUNULGlCQUFRO1lBQ1IsMEJBQVk7WUFDWiwwQkFBWTtZQUNaLDJCQUFhO1lBQ2I7Z0JBQ0UsT0FBTyxFQUFFLGdDQUFrQjtnQkFDM0IsUUFBUSxFQUFFLDJDQUFtQjthQUM5QjtZQUNEO2dCQUNFLE9BQU8sRUFBRSw2QkFBZTtnQkFDeEIsUUFBUSxFQUFFLDZDQUFnQjthQUMzQjtZQUNEO2dCQUNFLE9BQU8sRUFBRSw2QkFBYztnQkFDdkIsUUFBUSxFQUFFLDJDQUFlO2FBQzFCO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLHVCQUFjO2dCQUN2QixRQUFRLEVBQUUsSUFBSSx1Q0FBa0IsQ0FBQyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQzthQUM5QztZQUNEO2dCQUNFLE9BQU8sRUFBRSxlQUFNO2dCQUNmLFFBQVEsRUFBRSwrQkFBVTthQUNyQjtZQUNEO2dCQUNFLE9BQU8sRUFBRSxnQ0FBYztnQkFDdkIsUUFBUSxFQUFFLHlDQUFrQjthQUM3QjtZQUNEO2dCQUNFLE9BQU8sRUFBRSx1Q0FBaUI7Z0JBQzFCLFFBQVEsRUFBRSxJQUFJO2FBQ2Y7WUFDRDtnQkFDRSxPQUFPLEVBQUUsNkJBQWU7Z0JBQ3hCLFFBQVEsRUFBRSxJQUFJO2FBQ2Y7WUFDRDtnQkFDRSxPQUFPLEVBQUUsdUJBQVE7Z0JBQ2pCLFFBQVEsRUFBRSxJQUFJO2FBQ2Y7U0FDRjtRQUNELE9BQU8sRUFBRSxDQUFDLGlDQUF1QixDQUFDO0tBQ25DLENBQUM7R0FDVyxtQkFBbUIsQ0FBRztBQUF0QixrREFBbUIiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy90ZXN0Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50VGVzdGluZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwL3Rlc3RpbmcnO1xuaW1wb3J0IHsgTmdiQWN0aXZlTW9kYWwsIE5nYk1vZGFsIH0gZnJvbSAnQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAnO1xuaW1wb3J0IHsgSmhpTGFuZ3VhZ2VTZXJ2aWNlLCBKaGlEYXRhVXRpbHMsIEpoaURhdGVVdGlscywgSmhpRXZlbnRNYW5hZ2VyLCBKaGlBbGVydFNlcnZpY2UsIEpoaVBhcnNlTGlua3MgfSBmcm9tICduZy1qaGlwc3Rlcic7XG5cbmltcG9ydCB7IE1vY2tMYW5ndWFnZVNlcnZpY2UgfSBmcm9tICcuL2hlbHBlcnMvbW9jay1sYW5ndWFnZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9hY2NvdW50LnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9naW5Nb2RhbFNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS9sb2dpbi9sb2dpbi1tb2RhbC5zZXJ2aWNlJztcbmltcG9ydCB7IE1vY2tBY2NvdW50U2VydmljZSB9IGZyb20gJy4vaGVscGVycy9tb2NrLWFjY291bnQuc2VydmljZSc7XG5pbXBvcnQgeyBNb2NrQWN0aXZhdGVkUm91dGUsIE1vY2tSb3V0ZXIgfSBmcm9tICcuL2hlbHBlcnMvbW9jay1yb3V0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IE1vY2tBY3RpdmVNb2RhbCB9IGZyb20gJy4vaGVscGVycy9tb2NrLWFjdGl2ZS1tb2RhbC5zZXJ2aWNlJztcbmltcG9ydCB7IE1vY2tFdmVudE1hbmFnZXIgfSBmcm9tICcuL2hlbHBlcnMvbW9jay1ldmVudC1tYW5hZ2VyLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBwcm92aWRlcnM6IFtcbiAgICBEYXRlUGlwZSxcbiAgICBKaGlEYXRhVXRpbHMsXG4gICAgSmhpRGF0ZVV0aWxzLFxuICAgIEpoaVBhcnNlTGlua3MsXG4gICAge1xuICAgICAgcHJvdmlkZTogSmhpTGFuZ3VhZ2VTZXJ2aWNlLFxuICAgICAgdXNlQ2xhc3M6IE1vY2tMYW5ndWFnZVNlcnZpY2VcbiAgICB9LFxuICAgIHtcbiAgICAgIHByb3ZpZGU6IEpoaUV2ZW50TWFuYWdlcixcbiAgICAgIHVzZUNsYXNzOiBNb2NrRXZlbnRNYW5hZ2VyXG4gICAgfSxcbiAgICB7XG4gICAgICBwcm92aWRlOiBOZ2JBY3RpdmVNb2RhbCxcbiAgICAgIHVzZUNsYXNzOiBNb2NrQWN0aXZlTW9kYWxcbiAgICB9LFxuICAgIHtcbiAgICAgIHByb3ZpZGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgICAgdXNlVmFsdWU6IG5ldyBNb2NrQWN0aXZhdGVkUm91dGUoeyBpZDogMTIzIH0pXG4gICAgfSxcbiAgICB7XG4gICAgICBwcm92aWRlOiBSb3V0ZXIsXG4gICAgICB1c2VDbGFzczogTW9ja1JvdXRlclxuICAgIH0sXG4gICAge1xuICAgICAgcHJvdmlkZTogQWNjb3VudFNlcnZpY2UsXG4gICAgICB1c2VDbGFzczogTW9ja0FjY291bnRTZXJ2aWNlXG4gICAgfSxcbiAgICB7XG4gICAgICBwcm92aWRlOiBMb2dpbk1vZGFsU2VydmljZSxcbiAgICAgIHVzZVZhbHVlOiBudWxsXG4gICAgfSxcbiAgICB7XG4gICAgICBwcm92aWRlOiBKaGlBbGVydFNlcnZpY2UsXG4gICAgICB1c2VWYWx1ZTogbnVsbFxuICAgIH0sXG4gICAge1xuICAgICAgcHJvdmlkZTogTmdiTW9kYWwsXG4gICAgICB1c2VWYWx1ZTogbnVsbFxuICAgIH1cbiAgXSxcbiAgaW1wb3J0czogW0h0dHBDbGllbnRUZXN0aW5nTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBDb29wY3ljbGVUZXN0TW9kdWxlIHt9XG4iXSwidmVyc2lvbiI6M30=