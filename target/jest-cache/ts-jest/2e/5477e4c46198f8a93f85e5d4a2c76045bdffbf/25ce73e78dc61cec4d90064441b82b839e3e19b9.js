"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const logs_component_1 = require("app/admin/logs/logs.component");
const logs_service_1 = require("app/admin/logs/logs.service");
const log_model_1 = require("app/admin/logs/log.model");
describe('Component Tests', () => {
    describe('LogsComponent', () => {
        let comp;
        let fixture;
        let service;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [logs_component_1.LogsComponent],
                providers: [logs_service_1.LogsService]
            })
                .overrideTemplate(logs_component_1.LogsComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(logs_component_1.LogsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(logs_service_1.LogsService);
        });
        describe('OnInit', () => {
            it('should set all default values correctly', () => {
                expect(comp.filter).toBe('');
                expect(comp.orderProp).toBe('name');
                expect(comp.reverse).toBe(false);
            });
            it('Should call load all on init', () => {
                // GIVEN
                const log = new log_model_1.Log('main', 'WARN');
                spyOn(service, 'findAll').and.returnValue(rxjs_1.of({
                    loggers: {
                        main: {
                            effectiveLevel: 'WARN'
                        }
                    }
                }));
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(service.findAll).toHaveBeenCalled();
                expect(comp.loggers && comp.loggers[0]).toEqual(jasmine.objectContaining(log));
            });
        });
        describe('change log level', () => {
            it('should change log level correctly', () => {
                // GIVEN
                const log = new log_model_1.Log('main', 'ERROR');
                spyOn(service, 'changeLevel').and.returnValue(rxjs_1.of({}));
                spyOn(service, 'findAll').and.returnValue(rxjs_1.of({
                    loggers: {
                        main: {
                            effectiveLevel: 'ERROR'
                        }
                    }
                }));
                // WHEN
                comp.changeLevel('main', 'ERROR');
                // THEN
                expect(service.changeLevel).toHaveBeenCalled();
                expect(service.findAll).toHaveBeenCalled();
                expect(comp.loggers && comp.loggers[0]).toEqual(jasmine.objectContaining(log));
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vbG9ncy9sb2dzLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlFO0FBQ3pFLCtCQUEwQjtBQUUxQixzREFBMkQ7QUFDM0Qsa0VBQThEO0FBQzlELDhEQUEwRDtBQUMxRCx3REFBK0M7QUFFL0MsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsZUFBZSxFQUFFLEdBQUcsRUFBRTtRQUM3QixJQUFJLElBQW1CLENBQUM7UUFDeEIsSUFBSSxPQUF3QyxDQUFDO1FBQzdDLElBQUksT0FBb0IsQ0FBQztRQUV6QixVQUFVLENBQUMsZUFBSyxDQUFDLEdBQUcsRUFBRTtZQUNwQixpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUM3QixPQUFPLEVBQUUsQ0FBQyxpQ0FBbUIsQ0FBQztnQkFDOUIsWUFBWSxFQUFFLENBQUMsOEJBQWEsQ0FBQztnQkFDN0IsU0FBUyxFQUFFLENBQUMsMEJBQVcsQ0FBQzthQUN6QixDQUFDO2lCQUNDLGdCQUFnQixDQUFDLDhCQUFhLEVBQUUsRUFBRSxDQUFDO2lCQUNuQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLDhCQUFhLENBQUMsQ0FBQztZQUNqRCxJQUFJLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO1lBQ2pDLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsMEJBQVcsQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUU7WUFDdEIsRUFBRSxDQUFDLHlDQUF5QyxFQUFFLEdBQUcsRUFBRTtnQkFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyw4QkFBOEIsRUFBRSxHQUFHLEVBQUU7Z0JBQ3RDLFFBQVE7Z0JBQ1IsTUFBTSxHQUFHLEdBQUcsSUFBSSxlQUFHLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQyxLQUFLLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQ3ZDLFNBQUUsQ0FBQztvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsSUFBSSxFQUFFOzRCQUNKLGNBQWMsRUFBRSxNQUFNO3lCQUN2QjtxQkFDRjtpQkFDRixDQUFDLENBQ0gsQ0FBQztnQkFFRixPQUFPO2dCQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFFaEIsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDakYsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLEVBQUU7WUFDaEMsRUFBRSxDQUFDLG1DQUFtQyxFQUFFLEdBQUcsRUFBRTtnQkFDM0MsUUFBUTtnQkFDUixNQUFNLEdBQUcsR0FBRyxJQUFJLGVBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3JDLEtBQUssQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdEQsS0FBSyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUN2QyxTQUFFLENBQUM7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLElBQUksRUFBRTs0QkFDSixjQUFjLEVBQUUsT0FBTzt5QkFDeEI7cUJBQ0Y7aUJBQ0YsQ0FBQyxDQUNILENBQUM7Z0JBRUYsT0FBTztnQkFDUCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFFbEMsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQy9DLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNqRixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL3Rlc3QvamF2YXNjcmlwdC9zcGVjL2FwcC9hZG1pbi9sb2dzL2xvZ3MuY29tcG9uZW50LnNwZWMudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50Rml4dHVyZSwgVGVzdEJlZCwgYXN5bmMgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgb2YgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgQ29vcGN5Y2xlVGVzdE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3Rlc3QubW9kdWxlJztcbmltcG9ydCB7IExvZ3NDb21wb25lbnQgfSBmcm9tICdhcHAvYWRtaW4vbG9ncy9sb2dzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBMb2dzU2VydmljZSB9IGZyb20gJ2FwcC9hZG1pbi9sb2dzL2xvZ3Muc2VydmljZSc7XG5pbXBvcnQgeyBMb2cgfSBmcm9tICdhcHAvYWRtaW4vbG9ncy9sb2cubW9kZWwnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnTG9nc0NvbXBvbmVudCcsICgpID0+IHtcbiAgICBsZXQgY29tcDogTG9nc0NvbXBvbmVudDtcbiAgICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxMb2dzQ29tcG9uZW50PjtcbiAgICBsZXQgc2VydmljZTogTG9nc1NlcnZpY2U7XG5cbiAgICBiZWZvcmVFYWNoKGFzeW5jKCgpID0+IHtcbiAgICAgIFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgIGltcG9ydHM6IFtDb29wY3ljbGVUZXN0TW9kdWxlXSxcbiAgICAgICAgZGVjbGFyYXRpb25zOiBbTG9nc0NvbXBvbmVudF0sXG4gICAgICAgIHByb3ZpZGVyczogW0xvZ3NTZXJ2aWNlXVxuICAgICAgfSlcbiAgICAgICAgLm92ZXJyaWRlVGVtcGxhdGUoTG9nc0NvbXBvbmVudCwgJycpXG4gICAgICAgIC5jb21waWxlQ29tcG9uZW50cygpO1xuICAgIH0pKTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KExvZ3NDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgICBzZXJ2aWNlID0gZml4dHVyZS5kZWJ1Z0VsZW1lbnQuaW5qZWN0b3IuZ2V0KExvZ3NTZXJ2aWNlKTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdPbkluaXQnLCAoKSA9PiB7XG4gICAgICBpdCgnc2hvdWxkIHNldCBhbGwgZGVmYXVsdCB2YWx1ZXMgY29ycmVjdGx5JywgKCkgPT4ge1xuICAgICAgICBleHBlY3QoY29tcC5maWx0ZXIpLnRvQmUoJycpO1xuICAgICAgICBleHBlY3QoY29tcC5vcmRlclByb3ApLnRvQmUoJ25hbWUnKTtcbiAgICAgICAgZXhwZWN0KGNvbXAucmV2ZXJzZSkudG9CZShmYWxzZSk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ1Nob3VsZCBjYWxsIGxvYWQgYWxsIG9uIGluaXQnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGxvZyA9IG5ldyBMb2coJ21haW4nLCAnV0FSTicpO1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnZmluZEFsbCcpLmFuZC5yZXR1cm5WYWx1ZShcbiAgICAgICAgICBvZih7XG4gICAgICAgICAgICBsb2dnZXJzOiB7XG4gICAgICAgICAgICAgIG1haW46IHtcbiAgICAgICAgICAgICAgICBlZmZlY3RpdmVMZXZlbDogJ1dBUk4nXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICApO1xuXG4gICAgICAgIC8vIFdIRU5cbiAgICAgICAgY29tcC5uZ09uSW5pdCgpO1xuXG4gICAgICAgIC8vIFRIRU5cbiAgICAgICAgZXhwZWN0KHNlcnZpY2UuZmluZEFsbCkudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICBleHBlY3QoY29tcC5sb2dnZXJzICYmIGNvbXAubG9nZ2Vyc1swXSkudG9FcXVhbChqYXNtaW5lLm9iamVjdENvbnRhaW5pbmcobG9nKSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdjaGFuZ2UgbG9nIGxldmVsJywgKCkgPT4ge1xuICAgICAgaXQoJ3Nob3VsZCBjaGFuZ2UgbG9nIGxldmVsIGNvcnJlY3RseScsICgpID0+IHtcbiAgICAgICAgLy8gR0lWRU5cbiAgICAgICAgY29uc3QgbG9nID0gbmV3IExvZygnbWFpbicsICdFUlJPUicpO1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnY2hhbmdlTGV2ZWwnKS5hbmQucmV0dXJuVmFsdWUob2Yoe30pKTtcbiAgICAgICAgc3B5T24oc2VydmljZSwgJ2ZpbmRBbGwnKS5hbmQucmV0dXJuVmFsdWUoXG4gICAgICAgICAgb2Yoe1xuICAgICAgICAgICAgbG9nZ2Vyczoge1xuICAgICAgICAgICAgICBtYWluOiB7XG4gICAgICAgICAgICAgICAgZWZmZWN0aXZlTGV2ZWw6ICdFUlJPUidcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gV0hFTlxuICAgICAgICBjb21wLmNoYW5nZUxldmVsKCdtYWluJywgJ0VSUk9SJyk7XG5cbiAgICAgICAgLy8gVEhFTlxuICAgICAgICBleHBlY3Qoc2VydmljZS5jaGFuZ2VMZXZlbCkudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICBleHBlY3Qoc2VydmljZS5maW5kQWxsKS50b0hhdmVCZWVuQ2FsbGVkKCk7XG4gICAgICAgIGV4cGVjdChjb21wLmxvZ2dlcnMgJiYgY29tcC5sb2dnZXJzWzBdKS50b0VxdWFsKGphc21pbmUub2JqZWN0Q29udGFpbmluZyhsb2cpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9