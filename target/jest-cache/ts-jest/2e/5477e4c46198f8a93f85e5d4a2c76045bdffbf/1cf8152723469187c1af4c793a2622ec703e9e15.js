"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const testing_2 = require("@angular/common/http/testing");
const metrics_service_1 = require("app/admin/metrics/metrics.service");
const app_constants_1 = require("app/app.constants");
describe('Service Tests', () => {
    describe('Logs Service', () => {
        let service;
        let httpMock;
        beforeEach(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [testing_2.HttpClientTestingModule]
            });
            service = testing_1.TestBed.get(metrics_service_1.MetricsService);
            httpMock = testing_1.TestBed.get(testing_2.HttpTestingController);
        });
        afterEach(() => {
            httpMock.verify();
        });
        describe('Service methods', () => {
            it('should call correct URL', () => {
                service.getMetrics().subscribe();
                const req = httpMock.expectOne({ method: 'GET' });
                const resourceUrl = app_constants_1.SERVER_API_URL + 'management/jhimetrics';
                expect(req.request.url).toEqual(resourceUrl);
            });
            it('should return Metrics', () => {
                let expectedResult = null;
                const metrics = {
                    jvm: {},
                    'http.server.requests': {},
                    cache: {},
                    services: {},
                    databases: {},
                    garbageCollector: {},
                    processMetrics: {}
                };
                service.getMetrics().subscribe(received => {
                    expectedResult = received;
                });
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(metrics);
                expect(expectedResult).toEqual(metrics);
            });
            it('should return Thread Dump', () => {
                let expectedResult = null;
                const dump = { threads: [{ name: 'test1', threadState: 'RUNNABLE' }] };
                service.threadDump().subscribe(received => {
                    expectedResult = received;
                });
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(dump);
                expect(expectedResult).toEqual(dump);
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLnNlcnZpY2Uuc3BlYy50cyIsIm1hcHBpbmdzIjoiOztBQUFBLG1EQUFnRDtBQUNoRCwwREFBOEY7QUFFOUYsdUVBQXdGO0FBQ3hGLHFEQUFtRDtBQUVuRCxRQUFRLENBQUMsZUFBZSxFQUFFLEdBQUcsRUFBRTtJQUM3QixRQUFRLENBQUMsY0FBYyxFQUFFLEdBQUcsRUFBRTtRQUM1QixJQUFJLE9BQXVCLENBQUM7UUFDNUIsSUFBSSxRQUErQixDQUFDO1FBRXBDLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUM3QixPQUFPLEVBQUUsQ0FBQyxpQ0FBdUIsQ0FBQzthQUNuQyxDQUFDLENBQUM7WUFDSCxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWMsQ0FBQyxDQUFDO1lBQ3RDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLEdBQUcsQ0FBQywrQkFBcUIsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNiLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7WUFDL0IsRUFBRSxDQUFDLHlCQUF5QixFQUFFLEdBQUcsRUFBRTtnQkFDakMsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUVqQyxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sV0FBVyxHQUFHLDhCQUFjLEdBQUcsdUJBQXVCLENBQUM7Z0JBQzdELE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLEVBQUU7Z0JBQy9CLElBQUksY0FBYyxHQUFtQixJQUFJLENBQUM7Z0JBQzFDLE1BQU0sT0FBTyxHQUFZO29CQUN2QixHQUFHLEVBQUUsRUFBRTtvQkFDUCxzQkFBc0IsRUFBRSxFQUFFO29CQUMxQixLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLEVBQUUsRUFBRTtvQkFDWixTQUFTLEVBQUUsRUFBRTtvQkFDYixnQkFBZ0IsRUFBRSxFQUFFO29CQUNwQixjQUFjLEVBQUUsRUFBRTtpQkFDbkIsQ0FBQztnQkFFRixPQUFPLENBQUMsVUFBVSxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN4QyxjQUFjLEdBQUcsUUFBUSxDQUFDO2dCQUM1QixDQUFDLENBQUMsQ0FBQztnQkFFSCxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xELEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsMkJBQTJCLEVBQUUsR0FBRyxFQUFFO2dCQUNuQyxJQUFJLGNBQWMsR0FBc0IsSUFBSSxDQUFDO2dCQUM3QyxNQUFNLElBQUksR0FBZSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxDQUFDO2dCQUVuRixPQUFPLENBQUMsVUFBVSxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN4QyxjQUFjLEdBQUcsUUFBUSxDQUFDO2dCQUM1QixDQUFDLENBQUMsQ0FBQztnQkFFSCxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xELEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hCLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLnNlcnZpY2Uuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUZXN0QmVkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IEh0dHBDbGllbnRUZXN0aW5nTW9kdWxlLCBIdHRwVGVzdGluZ0NvbnRyb2xsZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cC90ZXN0aW5nJztcblxuaW1wb3J0IHsgTWV0cmljc1NlcnZpY2UsIE1ldHJpY3MsIFRocmVhZER1bXAgfSBmcm9tICdhcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLnNlcnZpY2UnO1xuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkwgfSBmcm9tICdhcHAvYXBwLmNvbnN0YW50cyc7XG5cbmRlc2NyaWJlKCdTZXJ2aWNlIFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnTG9ncyBTZXJ2aWNlJywgKCkgPT4ge1xuICAgIGxldCBzZXJ2aWNlOiBNZXRyaWNzU2VydmljZTtcbiAgICBsZXQgaHR0cE1vY2s6IEh0dHBUZXN0aW5nQ29udHJvbGxlcjtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgVGVzdEJlZC5jb25maWd1cmVUZXN0aW5nTW9kdWxlKHtcbiAgICAgICAgaW1wb3J0czogW0h0dHBDbGllbnRUZXN0aW5nTW9kdWxlXVxuICAgICAgfSk7XG4gICAgICBzZXJ2aWNlID0gVGVzdEJlZC5nZXQoTWV0cmljc1NlcnZpY2UpO1xuICAgICAgaHR0cE1vY2sgPSBUZXN0QmVkLmdldChIdHRwVGVzdGluZ0NvbnRyb2xsZXIpO1xuICAgIH0pO1xuXG4gICAgYWZ0ZXJFYWNoKCgpID0+IHtcbiAgICAgIGh0dHBNb2NrLnZlcmlmeSgpO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ1NlcnZpY2UgbWV0aG9kcycsICgpID0+IHtcbiAgICAgIGl0KCdzaG91bGQgY2FsbCBjb3JyZWN0IFVSTCcsICgpID0+IHtcbiAgICAgICAgc2VydmljZS5nZXRNZXRyaWNzKCkuc3Vic2NyaWJlKCk7XG5cbiAgICAgICAgY29uc3QgcmVxID0gaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KTtcbiAgICAgICAgY29uc3QgcmVzb3VyY2VVcmwgPSBTRVJWRVJfQVBJX1VSTCArICdtYW5hZ2VtZW50L2poaW1ldHJpY3MnO1xuICAgICAgICBleHBlY3QocmVxLnJlcXVlc3QudXJsKS50b0VxdWFsKHJlc291cmNlVXJsKTtcbiAgICAgIH0pO1xuXG4gICAgICBpdCgnc2hvdWxkIHJldHVybiBNZXRyaWNzJywgKCkgPT4ge1xuICAgICAgICBsZXQgZXhwZWN0ZWRSZXN1bHQ6IE1ldHJpY3MgfCBudWxsID0gbnVsbDtcbiAgICAgICAgY29uc3QgbWV0cmljczogTWV0cmljcyA9IHtcbiAgICAgICAgICBqdm06IHt9LFxuICAgICAgICAgICdodHRwLnNlcnZlci5yZXF1ZXN0cyc6IHt9LFxuICAgICAgICAgIGNhY2hlOiB7fSxcbiAgICAgICAgICBzZXJ2aWNlczoge30sXG4gICAgICAgICAgZGF0YWJhc2VzOiB7fSxcbiAgICAgICAgICBnYXJiYWdlQ29sbGVjdG9yOiB7fSxcbiAgICAgICAgICBwcm9jZXNzTWV0cmljczoge31cbiAgICAgICAgfTtcblxuICAgICAgICBzZXJ2aWNlLmdldE1ldHJpY3MoKS5zdWJzY3JpYmUocmVjZWl2ZWQgPT4ge1xuICAgICAgICAgIGV4cGVjdGVkUmVzdWx0ID0gcmVjZWl2ZWQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbnN0IHJlcSA9IGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSk7XG4gICAgICAgIHJlcS5mbHVzaChtZXRyaWNzKTtcbiAgICAgICAgZXhwZWN0KGV4cGVjdGVkUmVzdWx0KS50b0VxdWFsKG1ldHJpY3MpO1xuICAgICAgfSk7XG5cbiAgICAgIGl0KCdzaG91bGQgcmV0dXJuIFRocmVhZCBEdW1wJywgKCkgPT4ge1xuICAgICAgICBsZXQgZXhwZWN0ZWRSZXN1bHQ6IFRocmVhZER1bXAgfCBudWxsID0gbnVsbDtcbiAgICAgICAgY29uc3QgZHVtcDogVGhyZWFkRHVtcCA9IHsgdGhyZWFkczogW3sgbmFtZTogJ3Rlc3QxJywgdGhyZWFkU3RhdGU6ICdSVU5OQUJMRScgfV0gfTtcblxuICAgICAgICBzZXJ2aWNlLnRocmVhZER1bXAoKS5zdWJzY3JpYmUocmVjZWl2ZWQgPT4ge1xuICAgICAgICAgIGV4cGVjdGVkUmVzdWx0ID0gcmVjZWl2ZWQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbnN0IHJlcSA9IGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSk7XG4gICAgICAgIHJlcS5mbHVzaChkdW1wKTtcbiAgICAgICAgZXhwZWN0KGV4cGVjdGVkUmVzdWx0KS50b0VxdWFsKGR1bXApO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=