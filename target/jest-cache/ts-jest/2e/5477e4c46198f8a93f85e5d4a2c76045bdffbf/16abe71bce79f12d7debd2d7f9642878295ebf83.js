"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const http_1 = require("@angular/common/http");
const app_constants_1 = require("app/app.constants");
let ActivateService = class ActivateService {
    constructor(http) {
        this.http = http;
    }
    get(key) {
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/activate', {
            params: new http_1.HttpParams().set('key', key)
        });
    }
};
ActivateService = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [http_1.HttpClient])
], ActivateService);
exports.ActivateService = ActivateService;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5zZXJ2aWNlLnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUEyQztBQUMzQywrQ0FBOEQ7QUFHOUQscURBQW1EO0FBR25ELElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFDMUIsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUFHLENBQUM7SUFFeEMsR0FBRyxDQUFDLEdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLDhCQUFjLEdBQUcsY0FBYyxFQUFFO1lBQ3BELE1BQU0sRUFBRSxJQUFJLGlCQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQztTQUN6QyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YsQ0FBQTtBQVJZLGVBQWU7SUFEM0IsaUJBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzs2Q0FFUCxpQkFBVTtHQUR6QixlQUFlLENBUTNCO0FBUlksMENBQWUiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5zZXJ2aWNlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IFNFUlZFUl9BUElfVVJMIH0gZnJvbSAnYXBwL2FwcC5jb25zdGFudHMnO1xuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIEFjdGl2YXRlU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkge31cblxuICBnZXQoa2V5OiBzdHJpbmcpOiBPYnNlcnZhYmxlPHt9PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoU0VSVkVSX0FQSV9VUkwgKyAnYXBpL2FjdGl2YXRlJywge1xuICAgICAgcGFyYW1zOiBuZXcgSHR0cFBhcmFtcygpLnNldCgna2V5Jywga2V5KVxuICAgIH0pO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=