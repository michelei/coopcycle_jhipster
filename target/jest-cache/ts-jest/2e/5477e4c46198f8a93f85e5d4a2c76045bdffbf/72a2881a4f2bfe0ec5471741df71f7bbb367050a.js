"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_jhipster_1 = require("ng-jhipster");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const restaurant_service_1 = require("./restaurant.service");
const restaurant_delete_dialog_component_1 = require("./restaurant-delete-dialog.component");
let RestaurantComponent = class RestaurantComponent {
    constructor(restaurantService, eventManager, modalService) {
        this.restaurantService = restaurantService;
        this.eventManager = eventManager;
        this.modalService = modalService;
    }
    loadAll() {
        this.restaurantService.query().subscribe((res) => (this.restaurants = res.body || []));
    }
    ngOnInit() {
        this.loadAll();
        this.registerChangeInRestaurants();
    }
    ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventManager.destroy(this.eventSubscriber);
        }
    }
    trackId(index, item) {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
        return item.id;
    }
    registerChangeInRestaurants() {
        this.eventSubscriber = this.eventManager.subscribe('restaurantListModification', () => this.loadAll());
    }
    delete(restaurant) {
        const modalRef = this.modalService.open(restaurant_delete_dialog_component_1.RestaurantDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.restaurant = restaurant;
    }
};
RestaurantComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-restaurant',
        template: require('./restaurant.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [restaurant_service_1.RestaurantService, ng_jhipster_1.JhiEventManager, ng_bootstrap_1.NgbModal])
], RestaurantComponent);
exports.RestaurantComponent = RestaurantComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvcmVzdGF1cmFudC9yZXN0YXVyYW50LmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBNkQ7QUFHN0QsNkNBQThDO0FBQzlDLDZEQUFzRDtBQUd0RCw2REFBeUQ7QUFDekQsNkZBQXVGO0FBTXZGLElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBSTlCLFlBQXNCLGlCQUFvQyxFQUFZLFlBQTZCLEVBQVksWUFBc0I7UUFBL0csc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUFZLGlCQUFZLEdBQVosWUFBWSxDQUFpQjtRQUFZLGlCQUFZLEdBQVosWUFBWSxDQUFVO0lBQUcsQ0FBQztJQUV6SSxPQUFPO1FBQ0wsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQWdDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDdEgsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDZixJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDakQ7SUFDSCxDQUFDO0lBRUQsT0FBTyxDQUFDLEtBQWEsRUFBRSxJQUFpQjtRQUN0Qyw0RUFBNEU7UUFDNUUsT0FBTyxJQUFJLENBQUMsRUFBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRCwyQkFBMkI7UUFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyw0QkFBNEIsRUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUN6RyxDQUFDO0lBRUQsTUFBTSxDQUFDLFVBQXVCO1FBQzVCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9FQUErQixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUM3RyxRQUFRLENBQUMsaUJBQWlCLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztJQUNyRCxDQUFDO0NBQ0YsQ0FBQTtBQWxDWSxtQkFBbUI7SUFKL0IsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxnQkFBZ0I7UUFDMUIsa0JBQWEsNkJBQTZCLENBQUE7S0FDM0MsQ0FBQzs2Q0FLeUMsc0NBQWlCLEVBQTBCLDZCQUFlLEVBQTBCLHVCQUFRO0dBSjFILG1CQUFtQixDQWtDL0I7QUFsQ1ksa0RBQW1CIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2VudGl0aWVzL3Jlc3RhdXJhbnQvcmVzdGF1cmFudC5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBKaGlFdmVudE1hbmFnZXIgfSBmcm9tICduZy1qaGlwc3Rlcic7XG5pbXBvcnQgeyBOZ2JNb2RhbCB9IGZyb20gJ0BuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwJztcblxuaW1wb3J0IHsgSVJlc3RhdXJhbnQgfSBmcm9tICdhcHAvc2hhcmVkL21vZGVsL3Jlc3RhdXJhbnQubW9kZWwnO1xuaW1wb3J0IHsgUmVzdGF1cmFudFNlcnZpY2UgfSBmcm9tICcuL3Jlc3RhdXJhbnQuc2VydmljZSc7XG5pbXBvcnQgeyBSZXN0YXVyYW50RGVsZXRlRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi9yZXN0YXVyYW50LWRlbGV0ZS1kaWFsb2cuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLXJlc3RhdXJhbnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vcmVzdGF1cmFudC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgUmVzdGF1cmFudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgcmVzdGF1cmFudHM/OiBJUmVzdGF1cmFudFtdO1xuICBldmVudFN1YnNjcmliZXI/OiBTdWJzY3JpcHRpb247XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHJlc3RhdXJhbnRTZXJ2aWNlOiBSZXN0YXVyYW50U2VydmljZSwgcHJvdGVjdGVkIGV2ZW50TWFuYWdlcjogSmhpRXZlbnRNYW5hZ2VyLCBwcm90ZWN0ZWQgbW9kYWxTZXJ2aWNlOiBOZ2JNb2RhbCkge31cblxuICBsb2FkQWxsKCk6IHZvaWQge1xuICAgIHRoaXMucmVzdGF1cmFudFNlcnZpY2UucXVlcnkoKS5zdWJzY3JpYmUoKHJlczogSHR0cFJlc3BvbnNlPElSZXN0YXVyYW50W10+KSA9PiAodGhpcy5yZXN0YXVyYW50cyA9IHJlcy5ib2R5IHx8IFtdKSk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmxvYWRBbGwoKTtcbiAgICB0aGlzLnJlZ2lzdGVyQ2hhbmdlSW5SZXN0YXVyYW50cygpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZXZlbnRTdWJzY3JpYmVyKSB7XG4gICAgICB0aGlzLmV2ZW50TWFuYWdlci5kZXN0cm95KHRoaXMuZXZlbnRTdWJzY3JpYmVyKTtcbiAgICB9XG4gIH1cblxuICB0cmFja0lkKGluZGV4OiBudW1iZXIsIGl0ZW06IElSZXN0YXVyYW50KTogbnVtYmVyIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVubmVjZXNzYXJ5LXR5cGUtYXNzZXJ0aW9uXG4gICAgcmV0dXJuIGl0ZW0uaWQhO1xuICB9XG5cbiAgcmVnaXN0ZXJDaGFuZ2VJblJlc3RhdXJhbnRzKCk6IHZvaWQge1xuICAgIHRoaXMuZXZlbnRTdWJzY3JpYmVyID0gdGhpcy5ldmVudE1hbmFnZXIuc3Vic2NyaWJlKCdyZXN0YXVyYW50TGlzdE1vZGlmaWNhdGlvbicsICgpID0+IHRoaXMubG9hZEFsbCgpKTtcbiAgfVxuXG4gIGRlbGV0ZShyZXN0YXVyYW50OiBJUmVzdGF1cmFudCk6IHZvaWQge1xuICAgIGNvbnN0IG1vZGFsUmVmID0gdGhpcy5tb2RhbFNlcnZpY2Uub3BlbihSZXN0YXVyYW50RGVsZXRlRGlhbG9nQ29tcG9uZW50LCB7IHNpemU6ICdsZycsIGJhY2tkcm9wOiAnc3RhdGljJyB9KTtcbiAgICBtb2RhbFJlZi5jb21wb25lbnRJbnN0YW5jZS5yZXN0YXVyYW50ID0gcmVzdGF1cmFudDtcbiAgfVxufVxuIl0sInZlcnNpb24iOjN9