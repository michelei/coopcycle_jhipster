"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const http_1 = require("@angular/common/http");
const app_constants_1 = require("app/app.constants");
let MetricsService = class MetricsService {
    constructor(http) {
        this.http = http;
    }
    getMetrics() {
        return this.http.get(app_constants_1.SERVER_API_URL + 'management/jhimetrics');
    }
    threadDump() {
        return this.http.get(app_constants_1.SERVER_API_URL + 'management/threaddump');
    }
};
MetricsService = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [http_1.HttpClient])
], MetricsService);
exports.MetricsService = MetricsService;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLnNlcnZpY2UudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQTJDO0FBQzNDLCtDQUFrRDtBQUdsRCxxREFBbUQ7QUFRbkQsSUFBYSxjQUFjLEdBQTNCLE1BQWEsY0FBYztJQUN6QixZQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO0lBQUcsQ0FBQztJQUV4QyxVQUFVO1FBQ1IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSw4QkFBYyxHQUFHLHVCQUF1QixDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELFVBQVU7UUFDUixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFhLDhCQUFjLEdBQUcsdUJBQXVCLENBQUMsQ0FBQztJQUM3RSxDQUFDO0NBQ0YsQ0FBQTtBQVZZLGNBQWM7SUFEMUIsaUJBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzs2Q0FFUCxpQkFBVTtHQUR6QixjQUFjLENBVTFCO0FBVlksd0NBQWMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLnNlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkwgfSBmcm9tICdhcHAvYXBwLmNvbnN0YW50cyc7XG5cbmV4cG9ydCB0eXBlIE1ldHJpY3NLZXkgPSAnanZtJyB8ICdodHRwLnNlcnZlci5yZXF1ZXN0cycgfCAnY2FjaGUnIHwgJ3NlcnZpY2VzJyB8ICdkYXRhYmFzZXMnIHwgJ2dhcmJhZ2VDb2xsZWN0b3InIHwgJ3Byb2Nlc3NNZXRyaWNzJztcbmV4cG9ydCB0eXBlIE1ldHJpY3MgPSB7IFtrZXkgaW4gTWV0cmljc0tleV06IGFueSB9O1xuZXhwb3J0IHR5cGUgVGhyZWFkID0gYW55O1xuZXhwb3J0IHR5cGUgVGhyZWFkRHVtcCA9IHsgdGhyZWFkczogVGhyZWFkW10gfTtcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBNZXRyaWNzU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkge31cblxuICBnZXRNZXRyaWNzKCk6IE9ic2VydmFibGU8TWV0cmljcz4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PE1ldHJpY3M+KFNFUlZFUl9BUElfVVJMICsgJ21hbmFnZW1lbnQvamhpbWV0cmljcycpO1xuICB9XG5cbiAgdGhyZWFkRHVtcCgpOiBPYnNlcnZhYmxlPFRocmVhZER1bXA+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxUaHJlYWREdW1wPihTRVJWRVJfQVBJX1VSTCArICdtYW5hZ2VtZW50L3RocmVhZGR1bXAnKTtcbiAgfVxufVxuIl0sInZlcnNpb24iOjN9