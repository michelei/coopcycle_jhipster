"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const ng_jhipster_1 = require("ng-jhipster");
const error_constants_1 = require("app/shared/constants/error.constants");
const login_modal_service_1 = require("app/core/login/login-modal.service");
const register_service_1 = require("./register.service");
let RegisterComponent = class RegisterComponent {
    constructor(languageService, loginModalService, registerService, fb) {
        this.languageService = languageService;
        this.loginModalService = loginModalService;
        this.registerService = registerService;
        this.fb = fb;
        this.doNotMatch = false;
        this.error = false;
        this.errorEmailExists = false;
        this.errorUserExists = false;
        this.success = false;
        this.registerForm = this.fb.group({
            login: ['', [forms_1.Validators.required, forms_1.Validators.minLength(1), forms_1.Validators.maxLength(50), forms_1.Validators.pattern('^[_.@A-Za-z0-9-]*$')]],
            email: ['', [forms_1.Validators.required, forms_1.Validators.minLength(5), forms_1.Validators.maxLength(254), forms_1.Validators.email]],
            password: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]],
            confirmPassword: ['', [forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(50)]]
        });
    }
    ngAfterViewInit() {
        if (this.login) {
            this.login.nativeElement.focus();
        }
    }
    register() {
        this.doNotMatch = false;
        this.error = false;
        this.errorEmailExists = false;
        this.errorUserExists = false;
        const password = this.registerForm.get(['password']).value;
        if (password !== this.registerForm.get(['confirmPassword']).value) {
            this.doNotMatch = true;
        }
        else {
            const login = this.registerForm.get(['login']).value;
            const email = this.registerForm.get(['email']).value;
            this.registerService.save({ login, email, password, langKey: this.languageService.getCurrentLanguage() }).subscribe(() => (this.success = true), response => this.processError(response));
        }
    }
    openLogin() {
        this.loginModalService.open();
    }
    processError(response) {
        if (response.status === 400 && response.error.type === error_constants_1.LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = true;
        }
        else if (response.status === 400 && response.error.type === error_constants_1.EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = true;
        }
        else {
            this.error = true;
        }
    }
};
tslib_1.__decorate([
    core_1.ViewChild('login', { static: false }),
    tslib_1.__metadata("design:type", core_1.ElementRef)
], RegisterComponent.prototype, "login", void 0);
RegisterComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-register',
        template: require('./register.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [ng_jhipster_1.JhiLanguageService,
        login_modal_service_1.LoginModalService,
        register_service_1.RegisterService,
        forms_1.FormBuilder])
], RegisterComponent);
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWdGO0FBRWhGLDBDQUF5RDtBQUN6RCw2Q0FBaUQ7QUFFakQsMEVBQXdHO0FBQ3hHLDRFQUF1RTtBQUN2RSx5REFBcUQ7QUFNckQsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFpQjVCLFlBQ1UsZUFBbUMsRUFDbkMsaUJBQW9DLEVBQ3BDLGVBQWdDLEVBQ2hDLEVBQWU7UUFIZixvQkFBZSxHQUFmLGVBQWUsQ0FBb0I7UUFDbkMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQWpCekIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2QscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFFaEIsaUJBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUMzQixLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDL0gsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEcsQ0FBQyxDQUFDO0lBT0EsQ0FBQztJQUVKLGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUU3QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFFLENBQUMsS0FBSyxDQUFDO1FBQzVELElBQUksUUFBUSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBRSxDQUFDLEtBQUssRUFBRTtZQUNsRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztTQUN4QjthQUFNO1lBQ0wsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBRSxDQUFDLEtBQUssQ0FBQztZQUN0RCxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3RELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUNqSCxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQzNCLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FDeEMsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQUVELFNBQVM7UUFDUCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVPLFlBQVksQ0FBQyxRQUEyQjtRQUM5QyxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHlDQUF1QixFQUFFO1lBQzlFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQzdCO2FBQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyx5Q0FBdUIsRUFBRTtZQUNyRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjtJQUNILENBQUM7Q0FDRixDQUFBO0FBNURDO0lBREMsZ0JBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7c0NBQzlCLGlCQUFVO2dEQUFDO0FBRlIsaUJBQWlCO0lBSjdCLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsY0FBYztRQUN4QixrQkFBYSwyQkFBMkIsQ0FBQTtLQUN6QyxDQUFDOzZDQW1CMkIsZ0NBQWtCO1FBQ2hCLHVDQUFpQjtRQUNuQixrQ0FBZTtRQUM1QixtQkFBVztHQXJCZCxpQkFBaUIsQ0E4RDdCO0FBOURZLDhDQUFpQiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9hY2NvdW50L3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEFmdGVyVmlld0luaXQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEpoaUxhbmd1YWdlU2VydmljZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcblxuaW1wb3J0IHsgRU1BSUxfQUxSRUFEWV9VU0VEX1RZUEUsIExPR0lOX0FMUkVBRFlfVVNFRF9UWVBFIH0gZnJvbSAnYXBwL3NoYXJlZC9jb25zdGFudHMvZXJyb3IuY29uc3RhbnRzJztcbmltcG9ydCB7IExvZ2luTW9kYWxTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvbG9naW4vbG9naW4tbW9kYWwuc2VydmljZSc7XG5pbXBvcnQgeyBSZWdpc3RlclNlcnZpY2UgfSBmcm9tICcuL3JlZ2lzdGVyLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktcmVnaXN0ZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vcmVnaXN0ZXIuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFJlZ2lzdGVyQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoJ2xvZ2luJywgeyBzdGF0aWM6IGZhbHNlIH0pXG4gIGxvZ2luPzogRWxlbWVudFJlZjtcblxuICBkb05vdE1hdGNoID0gZmFsc2U7XG4gIGVycm9yID0gZmFsc2U7XG4gIGVycm9yRW1haWxFeGlzdHMgPSBmYWxzZTtcbiAgZXJyb3JVc2VyRXhpc3RzID0gZmFsc2U7XG4gIHN1Y2Nlc3MgPSBmYWxzZTtcblxuICByZWdpc3RlckZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICBsb2dpbjogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoMSksIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKCdeW18uQEEtWmEtejAtOS1dKiQnKV1dLFxuICAgIGVtYWlsOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg1KSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjU0KSwgVmFsaWRhdG9ycy5lbWFpbF1dLFxuICAgIHBhc3N3b3JkOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg0KSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXV0sXG4gICAgY29uZmlybVBhc3N3b3JkOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg0KSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXV1cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBsYW5ndWFnZVNlcnZpY2U6IEpoaUxhbmd1YWdlU2VydmljZSxcbiAgICBwcml2YXRlIGxvZ2luTW9kYWxTZXJ2aWNlOiBMb2dpbk1vZGFsU2VydmljZSxcbiAgICBwcml2YXRlIHJlZ2lzdGVyU2VydmljZTogUmVnaXN0ZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyXG4gICkge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMubG9naW4pIHtcbiAgICAgIHRoaXMubG9naW4ubmF0aXZlRWxlbWVudC5mb2N1cygpO1xuICAgIH1cbiAgfVxuXG4gIHJlZ2lzdGVyKCk6IHZvaWQge1xuICAgIHRoaXMuZG9Ob3RNYXRjaCA9IGZhbHNlO1xuICAgIHRoaXMuZXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmVycm9yRW1haWxFeGlzdHMgPSBmYWxzZTtcbiAgICB0aGlzLmVycm9yVXNlckV4aXN0cyA9IGZhbHNlO1xuXG4gICAgY29uc3QgcGFzc3dvcmQgPSB0aGlzLnJlZ2lzdGVyRm9ybS5nZXQoWydwYXNzd29yZCddKSEudmFsdWU7XG4gICAgaWYgKHBhc3N3b3JkICE9PSB0aGlzLnJlZ2lzdGVyRm9ybS5nZXQoWydjb25maXJtUGFzc3dvcmQnXSkhLnZhbHVlKSB7XG4gICAgICB0aGlzLmRvTm90TWF0Y2ggPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCBsb2dpbiA9IHRoaXMucmVnaXN0ZXJGb3JtLmdldChbJ2xvZ2luJ10pIS52YWx1ZTtcbiAgICAgIGNvbnN0IGVtYWlsID0gdGhpcy5yZWdpc3RlckZvcm0uZ2V0KFsnZW1haWwnXSkhLnZhbHVlO1xuICAgICAgdGhpcy5yZWdpc3RlclNlcnZpY2Uuc2F2ZSh7IGxvZ2luLCBlbWFpbCwgcGFzc3dvcmQsIGxhbmdLZXk6IHRoaXMubGFuZ3VhZ2VTZXJ2aWNlLmdldEN1cnJlbnRMYW5ndWFnZSgpIH0pLnN1YnNjcmliZShcbiAgICAgICAgKCkgPT4gKHRoaXMuc3VjY2VzcyA9IHRydWUpLFxuICAgICAgICByZXNwb25zZSA9PiB0aGlzLnByb2Nlc3NFcnJvcihyZXNwb25zZSlcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgb3BlbkxvZ2luKCk6IHZvaWQge1xuICAgIHRoaXMubG9naW5Nb2RhbFNlcnZpY2Uub3BlbigpO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzRXJyb3IocmVzcG9uc2U6IEh0dHBFcnJvclJlc3BvbnNlKTogdm9pZCB7XG4gICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gNDAwICYmIHJlc3BvbnNlLmVycm9yLnR5cGUgPT09IExPR0lOX0FMUkVBRFlfVVNFRF9UWVBFKSB7XG4gICAgICB0aGlzLmVycm9yVXNlckV4aXN0cyA9IHRydWU7XG4gICAgfSBlbHNlIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDQwMCAmJiByZXNwb25zZS5lcnJvci50eXBlID09PSBFTUFJTF9BTFJFQURZX1VTRURfVFlQRSkge1xuICAgICAgdGhpcy5lcnJvckVtYWlsRXhpc3RzID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lcnJvciA9IHRydWU7XG4gICAgfVxuICB9XG59XG4iXSwidmVyc2lvbiI6M30=