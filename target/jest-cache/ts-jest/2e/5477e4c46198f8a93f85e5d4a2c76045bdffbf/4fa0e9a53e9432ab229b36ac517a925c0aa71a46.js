"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const core_2 = require("@ngx-translate/core");
const ng_jhipster_1 = require("ng-jhipster");
let AlertErrorComponent = class AlertErrorComponent {
    constructor(alertService, eventManager, translateService) {
        this.alertService = alertService;
        this.eventManager = eventManager;
        this.alerts = [];
        this.errorListener = eventManager.subscribe('coopcycleApp.error', (response) => {
            const errorResponse = response.content;
            this.addErrorAlert(errorResponse.message, errorResponse.key, errorResponse.params);
        });
        this.httpErrorListener = eventManager.subscribe('coopcycleApp.httpError', (response) => {
            const httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400: {
                    const arr = httpErrorResponse.headers.keys();
                    let errorHeader = null;
                    let entityKey = null;
                    arr.forEach(entry => {
                        if (entry.toLowerCase().endsWith('app-error')) {
                            errorHeader = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.toLowerCase().endsWith('app-params')) {
                            entityKey = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader) {
                        const entityName = translateService.instant('global.menu.entities.' + entityKey);
                        this.addErrorAlert(errorHeader, errorHeader, { entityName });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        const fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (const fieldError of fieldErrors) {
                            if (['Min', 'Max', 'DecimalMin', 'DecimalMax'].includes(fieldError.message)) {
                                fieldError.message = 'Size';
                            }
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = translateService.instant('coopcycleApp.' + fieldError.objectName + '.' + convertedField);
                            this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                }
                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    setClasses(alert) {
        const classes = { 'jhi-toast': Boolean(alert.toast) };
        if (alert.position) {
            return Object.assign(Object.assign({}, classes), { [alert.position]: true });
        }
        return classes;
    }
    ngOnDestroy() {
        if (this.errorListener) {
            this.eventManager.destroy(this.errorListener);
        }
        if (this.httpErrorListener) {
            this.eventManager.destroy(this.httpErrorListener);
        }
    }
    addErrorAlert(message, key, data) {
        message = key && key !== null ? key : message;
        const newAlert = {
            type: 'danger',
            msg: message,
            params: data,
            timeout: 5000,
            toast: this.alertService.isToast(),
            scoped: true
        };
        this.alerts.push(this.alertService.addAlert(newAlert, this.alerts));
    }
};
AlertErrorComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-alert-error',
        template: `
    <div class="alerts" role="alert">
      <div *ngFor="let alert of alerts" [ngClass]="setClasses(alert)">
        <ngb-alert *ngIf="alert && alert.type && alert.msg" [type]="alert.type" (close)="alert.close(alerts)">
          <pre [innerHTML]="alert.msg"></pre>
        </ngb-alert>
      </div>
    </div>
  `
    }),
    tslib_1.__metadata("design:paramtypes", [ng_jhipster_1.JhiAlertService, ng_jhipster_1.JhiEventManager, core_2.TranslateService])
], AlertErrorComponent);
exports.AlertErrorComponent = AlertErrorComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvc2hhcmVkL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBcUQ7QUFFckQsOENBQXVEO0FBQ3ZELDZDQUE4RjtBQWlCOUYsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFLOUIsWUFBb0IsWUFBNkIsRUFBVSxZQUE2QixFQUFFLGdCQUFrQztRQUF4RyxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFKeEYsV0FBTSxHQUFlLEVBQUUsQ0FBQztRQUt0QixJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxRQUF5QyxFQUFFLEVBQUU7WUFDOUcsTUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUN2QyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLEdBQUcsRUFBRSxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckYsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLFFBQWdELEVBQUUsRUFBRTtZQUM3SCxNQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDM0MsUUFBUSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hDLDJDQUEyQztnQkFDM0MsS0FBSyxDQUFDO29CQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztvQkFDekUsTUFBTTtnQkFFUixLQUFLLEdBQUcsQ0FBQyxDQUFDO29CQUNSLE1BQU0sR0FBRyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDN0MsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ2xCLElBQUksS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRTs0QkFDN0MsV0FBVyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ3BEOzZCQUFNLElBQUksS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRTs0QkFDckQsU0FBUyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ2xEO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksV0FBVyxFQUFFO3dCQUNmLE1BQU0sVUFBVSxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsR0FBRyxTQUFTLENBQUMsQ0FBQzt3QkFDakYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsV0FBVyxFQUFFLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztxQkFDOUQ7eUJBQU0sSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7d0JBQ2hGLE1BQU0sV0FBVyxHQUFHLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7d0JBQ3hELEtBQUssTUFBTSxVQUFVLElBQUksV0FBVyxFQUFFOzRCQUNwQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQ0FDM0UsVUFBVSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7NkJBQzdCOzRCQUNELHVHQUF1Rzs0QkFDdkcsTUFBTSxjQUFjLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUNsRSxNQUFNLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxVQUFVLEdBQUcsR0FBRyxHQUFHLGNBQWMsQ0FBQyxDQUFDOzRCQUMzRyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixHQUFHLFNBQVMsR0FBRyxHQUFHLEVBQUUsUUFBUSxHQUFHLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO3lCQUN4RztxQkFDRjt5QkFBTSxJQUFJLGlCQUFpQixDQUFDLEtBQUssS0FBSyxFQUFFLElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTt3QkFDNUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN0SDt5QkFBTTt3QkFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM3QztvQkFDRCxNQUFNO2lCQUNQO2dCQUVELEtBQUssR0FBRztvQkFDTixJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO29CQUN2RCxNQUFNO2dCQUVSO29CQUNFLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO3dCQUNyRSxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDckQ7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDN0M7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFlO1FBQ3hCLE1BQU0sT0FBTyxHQUFHLEVBQUUsV0FBVyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUN0RCxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDbEIsdUNBQVksT0FBTyxLQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLElBQUksSUFBRztTQUMvQztRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUMvQztRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ25EO0lBQ0gsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUFlLEVBQUUsR0FBWSxFQUFFLElBQVU7UUFDckQsT0FBTyxHQUFHLEdBQUcsSUFBSSxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztRQUU5QyxNQUFNLFFBQVEsR0FBYTtZQUN6QixJQUFJLEVBQUUsUUFBUTtZQUNkLEdBQUcsRUFBRSxPQUFPO1lBQ1osTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsSUFBSTtZQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRTtZQUNsQyxNQUFNLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdEUsQ0FBQztDQUNGLENBQUE7QUFqR1ksbUJBQW1CO0lBWi9CLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLFFBQVEsRUFBRTs7Ozs7Ozs7R0FRVDtLQUNGLENBQUM7NkNBTWtDLDZCQUFlLEVBQXdCLDZCQUFlLEVBQW9CLHVCQUFnQjtHQUxqSCxtQkFBbUIsQ0FpRy9CO0FBakdZLGtEQUFtQiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9zaGFyZWQvYWxlcnQvYWxlcnQtZXJyb3IuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciwgSmhpQWxlcnQsIEpoaUFsZXJ0U2VydmljZSwgSmhpRXZlbnRXaXRoQ29udGVudCB9IGZyb20gJ25nLWpoaXBzdGVyJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBBbGVydEVycm9yIH0gZnJvbSAnLi9hbGVydC1lcnJvci5tb2RlbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1hbGVydC1lcnJvcicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGRpdiBjbGFzcz1cImFsZXJ0c1wiIHJvbGU9XCJhbGVydFwiPlxuICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgYWxlcnQgb2YgYWxlcnRzXCIgW25nQ2xhc3NdPVwic2V0Q2xhc3NlcyhhbGVydClcIj5cbiAgICAgICAgPG5nYi1hbGVydCAqbmdJZj1cImFsZXJ0ICYmIGFsZXJ0LnR5cGUgJiYgYWxlcnQubXNnXCIgW3R5cGVdPVwiYWxlcnQudHlwZVwiIChjbG9zZSk9XCJhbGVydC5jbG9zZShhbGVydHMpXCI+XG4gICAgICAgICAgPHByZSBbaW5uZXJIVE1MXT1cImFsZXJ0Lm1zZ1wiPjwvcHJlPlxuICAgICAgICA8L25nYi1hbGVydD5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICBgXG59KVxuZXhwb3J0IGNsYXNzIEFsZXJ0RXJyb3JDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xuICBhbGVydHM6IEpoaUFsZXJ0W10gPSBbXTtcbiAgZXJyb3JMaXN0ZW5lcjogU3Vic2NyaXB0aW9uO1xuICBodHRwRXJyb3JMaXN0ZW5lcjogU3Vic2NyaXB0aW9uO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWxlcnRTZXJ2aWNlOiBKaGlBbGVydFNlcnZpY2UsIHByaXZhdGUgZXZlbnRNYW5hZ2VyOiBKaGlFdmVudE1hbmFnZXIsIHRyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcbiAgICB0aGlzLmVycm9yTGlzdGVuZXIgPSBldmVudE1hbmFnZXIuc3Vic2NyaWJlKCdjb29wY3ljbGVBcHAuZXJyb3InLCAocmVzcG9uc2U6IEpoaUV2ZW50V2l0aENvbnRlbnQ8QWxlcnRFcnJvcj4pID0+IHtcbiAgICAgIGNvbnN0IGVycm9yUmVzcG9uc2UgPSByZXNwb25zZS5jb250ZW50O1xuICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGVycm9yUmVzcG9uc2UubWVzc2FnZSwgZXJyb3JSZXNwb25zZS5rZXksIGVycm9yUmVzcG9uc2UucGFyYW1zKTtcbiAgICB9KTtcblxuICAgIHRoaXMuaHR0cEVycm9yTGlzdGVuZXIgPSBldmVudE1hbmFnZXIuc3Vic2NyaWJlKCdjb29wY3ljbGVBcHAuaHR0cEVycm9yJywgKHJlc3BvbnNlOiBKaGlFdmVudFdpdGhDb250ZW50PEh0dHBFcnJvclJlc3BvbnNlPikgPT4ge1xuICAgICAgY29uc3QgaHR0cEVycm9yUmVzcG9uc2UgPSByZXNwb25zZS5jb250ZW50O1xuICAgICAgc3dpdGNoIChodHRwRXJyb3JSZXNwb25zZS5zdGF0dXMpIHtcbiAgICAgICAgLy8gY29ubmVjdGlvbiByZWZ1c2VkLCBzZXJ2ZXIgbm90IHJlYWNoYWJsZVxuICAgICAgICBjYXNlIDA6XG4gICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdTZXJ2ZXIgbm90IHJlYWNoYWJsZScsICdlcnJvci5zZXJ2ZXIubm90LnJlYWNoYWJsZScpO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgNDAwOiB7XG4gICAgICAgICAgY29uc3QgYXJyID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5rZXlzKCk7XG4gICAgICAgICAgbGV0IGVycm9ySGVhZGVyID0gbnVsbDtcbiAgICAgICAgICBsZXQgZW50aXR5S2V5ID0gbnVsbDtcbiAgICAgICAgICBhcnIuZm9yRWFjaChlbnRyeSA9PiB7XG4gICAgICAgICAgICBpZiAoZW50cnkudG9Mb3dlckNhc2UoKS5lbmRzV2l0aCgnYXBwLWVycm9yJykpIHtcbiAgICAgICAgICAgICAgZXJyb3JIZWFkZXIgPSBodHRwRXJyb3JSZXNwb25zZS5oZWFkZXJzLmdldChlbnRyeSk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGVudHJ5LnRvTG93ZXJDYXNlKCkuZW5kc1dpdGgoJ2FwcC1wYXJhbXMnKSkge1xuICAgICAgICAgICAgICBlbnRpdHlLZXkgPSBodHRwRXJyb3JSZXNwb25zZS5oZWFkZXJzLmdldChlbnRyeSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYgKGVycm9ySGVhZGVyKSB7XG4gICAgICAgICAgICBjb25zdCBlbnRpdHlOYW1lID0gdHJhbnNsYXRlU2VydmljZS5pbnN0YW50KCdnbG9iYWwubWVudS5lbnRpdGllcy4nICsgZW50aXR5S2V5KTtcbiAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChlcnJvckhlYWRlciwgZXJyb3JIZWFkZXIsIHsgZW50aXR5TmFtZSB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5maWVsZEVycm9ycykge1xuICAgICAgICAgICAgY29uc3QgZmllbGRFcnJvcnMgPSBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5maWVsZEVycm9ycztcbiAgICAgICAgICAgIGZvciAoY29uc3QgZmllbGRFcnJvciBvZiBmaWVsZEVycm9ycykge1xuICAgICAgICAgICAgICBpZiAoWydNaW4nLCAnTWF4JywgJ0RlY2ltYWxNaW4nLCAnRGVjaW1hbE1heCddLmluY2x1ZGVzKGZpZWxkRXJyb3IubWVzc2FnZSkpIHtcbiAgICAgICAgICAgICAgICBmaWVsZEVycm9yLm1lc3NhZ2UgPSAnU2l6ZSc7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgLy8gY29udmVydCAnc29tZXRoaW5nWzE0XS5vdGhlcls0XS5pZCcgdG8gJ3NvbWV0aGluZ1tdLm90aGVyW10uaWQnIHNvIHRyYW5zbGF0aW9ucyBjYW4gYmUgd3JpdHRlbiB0byBpdFxuICAgICAgICAgICAgICBjb25zdCBjb252ZXJ0ZWRGaWVsZCA9IGZpZWxkRXJyb3IuZmllbGQucmVwbGFjZSgvXFxbXFxkKlxcXS9nLCAnW10nKTtcbiAgICAgICAgICAgICAgY29uc3QgZmllbGROYW1lID0gdHJhbnNsYXRlU2VydmljZS5pbnN0YW50KCdjb29wY3ljbGVBcHAuJyArIGZpZWxkRXJyb3Iub2JqZWN0TmFtZSArICcuJyArIGNvbnZlcnRlZEZpZWxkKTtcbiAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdFcnJvciBvbiBmaWVsZCBcIicgKyBmaWVsZE5hbWUgKyAnXCInLCAnZXJyb3IuJyArIGZpZWxkRXJyb3IubWVzc2FnZSwgeyBmaWVsZE5hbWUgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBlbHNlIGlmIChodHRwRXJyb3JSZXNwb25zZS5lcnJvciAhPT0gJycgJiYgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSkge1xuICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UsIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UsIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLnBhcmFtcyk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvcik7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FzZSA0MDQ6XG4gICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdOb3QgZm91bmQnLCAnZXJyb3IudXJsLm5vdC5mb3VuZCcpO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKSB7XG4gICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvcik7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgc2V0Q2xhc3NlcyhhbGVydDogSmhpQWxlcnQpOiB7IFtrZXk6IHN0cmluZ106IGJvb2xlYW4gfSB7XG4gICAgY29uc3QgY2xhc3NlcyA9IHsgJ2poaS10b2FzdCc6IEJvb2xlYW4oYWxlcnQudG9hc3QpIH07XG4gICAgaWYgKGFsZXJ0LnBvc2l0aW9uKSB7XG4gICAgICByZXR1cm4geyAuLi5jbGFzc2VzLCBbYWxlcnQucG9zaXRpb25dOiB0cnVlIH07XG4gICAgfVxuICAgIHJldHVybiBjbGFzc2VzO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZXJyb3JMaXN0ZW5lcikge1xuICAgICAgdGhpcy5ldmVudE1hbmFnZXIuZGVzdHJveSh0aGlzLmVycm9yTGlzdGVuZXIpO1xuICAgIH1cbiAgICBpZiAodGhpcy5odHRwRXJyb3JMaXN0ZW5lcikge1xuICAgICAgdGhpcy5ldmVudE1hbmFnZXIuZGVzdHJveSh0aGlzLmh0dHBFcnJvckxpc3RlbmVyKTtcbiAgICB9XG4gIH1cblxuICBhZGRFcnJvckFsZXJ0KG1lc3NhZ2U6IHN0cmluZywga2V5Pzogc3RyaW5nLCBkYXRhPzogYW55KTogdm9pZCB7XG4gICAgbWVzc2FnZSA9IGtleSAmJiBrZXkgIT09IG51bGwgPyBrZXkgOiBtZXNzYWdlO1xuXG4gICAgY29uc3QgbmV3QWxlcnQ6IEpoaUFsZXJ0ID0ge1xuICAgICAgdHlwZTogJ2RhbmdlcicsXG4gICAgICBtc2c6IG1lc3NhZ2UsXG4gICAgICBwYXJhbXM6IGRhdGEsXG4gICAgICB0aW1lb3V0OiA1MDAwLFxuICAgICAgdG9hc3Q6IHRoaXMuYWxlcnRTZXJ2aWNlLmlzVG9hc3QoKSxcbiAgICAgIHNjb3BlZDogdHJ1ZVxuICAgIH07XG5cbiAgICB0aGlzLmFsZXJ0cy5wdXNoKHRoaXMuYWxlcnRTZXJ2aWNlLmFkZEFsZXJ0KG5ld0FsZXJ0LCB0aGlzLmFsZXJ0cykpO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=