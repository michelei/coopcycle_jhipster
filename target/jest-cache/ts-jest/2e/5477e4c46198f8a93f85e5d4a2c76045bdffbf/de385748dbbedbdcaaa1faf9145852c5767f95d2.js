"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const testing_1 = require("@angular/core/testing");
const forms_1 = require("@angular/forms");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../../test.module");
const password_reset_init_component_1 = require("app/account/password-reset/init/password-reset-init.component");
const password_reset_init_service_1 = require("app/account/password-reset/init/password-reset-init.service");
describe('Component Tests', () => {
    describe('PasswordResetInitComponent', () => {
        let fixture;
        let comp;
        beforeEach(() => {
            fixture = testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [password_reset_init_component_1.PasswordResetInitComponent],
                providers: [forms_1.FormBuilder]
            })
                .overrideTemplate(password_reset_init_component_1.PasswordResetInitComponent, '')
                .createComponent(password_reset_init_component_1.PasswordResetInitComponent);
            comp = fixture.componentInstance;
        });
        it('sets focus after the view has been initialized', () => {
            const node = {
                focus() { }
            };
            comp.email = new core_1.ElementRef(node);
            spyOn(node, 'focus');
            comp.ngAfterViewInit();
            expect(node.focus).toHaveBeenCalled();
        });
        it('notifies of success upon successful requestReset', testing_1.inject([password_reset_init_service_1.PasswordResetInitService], (service) => {
            spyOn(service, 'save').and.returnValue(rxjs_1.of({}));
            comp.resetRequestForm.patchValue({
                email: 'user@domain.com'
            });
            comp.requestReset();
            expect(service.save).toHaveBeenCalledWith('user@domain.com');
            expect(comp.success).toBe(true);
        }));
        it('no notification of success upon error response', testing_1.inject([password_reset_init_service_1.PasswordResetInitService], (service) => {
            spyOn(service, 'save').and.returnValue(rxjs_1.throwError({
                status: 503,
                data: 'something else'
            }));
            comp.resetRequestForm.patchValue({
                email: 'user@domain.com'
            });
            comp.requestReset();
            expect(service.save).toHaveBeenCalledWith('user@domain.com');
            expect(comp.success).toBe(false);
        }));
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9pbml0L3Bhc3N3b3JkLXJlc2V0LWluaXQuY29tcG9uZW50LnNwZWMudHMiLCJtYXBwaW5ncyI6Ijs7QUFBQSx3Q0FBMkM7QUFDM0MsbURBQTBFO0FBQzFFLDBDQUE2QztBQUM3QywrQkFBc0M7QUFFdEMseURBQThEO0FBQzlELGlIQUEyRztBQUMzRyw2R0FBdUc7QUFFdkcsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsNEJBQTRCLEVBQUUsR0FBRyxFQUFFO1FBQzFDLElBQUksT0FBcUQsQ0FBQztRQUMxRCxJQUFJLElBQWdDLENBQUM7UUFFckMsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUN2QyxPQUFPLEVBQUUsQ0FBQyxpQ0FBbUIsQ0FBQztnQkFDOUIsWUFBWSxFQUFFLENBQUMsMERBQTBCLENBQUM7Z0JBQzFDLFNBQVMsRUFBRSxDQUFDLG1CQUFXLENBQUM7YUFDekIsQ0FBQztpQkFDQyxnQkFBZ0IsQ0FBQywwREFBMEIsRUFBRSxFQUFFLENBQUM7aUJBQ2hELGVBQWUsQ0FBQywwREFBMEIsQ0FBQyxDQUFDO1lBQy9DLElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsZ0RBQWdELEVBQUUsR0FBRyxFQUFFO1lBQ3hELE1BQU0sSUFBSSxHQUFHO2dCQUNYLEtBQUssS0FBVSxDQUFDO2FBQ2pCLENBQUM7WUFDRixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksaUJBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQyxLQUFLLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRXJCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUV2QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsa0RBQWtELEVBQUUsZ0JBQU0sQ0FBQyxDQUFDLHNEQUF3QixDQUFDLEVBQUUsQ0FBQyxPQUFpQyxFQUFFLEVBQUU7WUFDOUgsS0FBSyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7Z0JBQy9CLEtBQUssRUFBRSxpQkFBaUI7YUFDekIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBRXBCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRUosRUFBRSxDQUFDLGdEQUFnRCxFQUFFLGdCQUFNLENBQUMsQ0FBQyxzREFBd0IsQ0FBQyxFQUFFLENBQUMsT0FBaUMsRUFBRSxFQUFFO1lBQzVILEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FDcEMsaUJBQVUsQ0FBQztnQkFDVCxNQUFNLEVBQUUsR0FBRztnQkFDWCxJQUFJLEVBQUUsZ0JBQWdCO2FBQ3ZCLENBQUMsQ0FDSCxDQUFDO1lBQ0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztnQkFDL0IsS0FBSyxFQUFFLGlCQUFpQjthQUN6QixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFFcEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzdELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDTixDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2FjY291bnQvcGFzc3dvcmQtcmVzZXQvaW5pdC9wYXNzd29yZC1yZXNldC1pbml0LmNvbXBvbmVudC5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQsIGluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IENvb3BjeWNsZVRlc3RNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi8uLi90ZXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBQYXNzd29yZFJlc2V0SW5pdENvbXBvbmVudCB9IGZyb20gJ2FwcC9hY2NvdW50L3Bhc3N3b3JkLXJlc2V0L2luaXQvcGFzc3dvcmQtcmVzZXQtaW5pdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGFzc3dvcmRSZXNldEluaXRTZXJ2aWNlIH0gZnJvbSAnYXBwL2FjY291bnQvcGFzc3dvcmQtcmVzZXQvaW5pdC9wYXNzd29yZC1yZXNldC1pbml0LnNlcnZpY2UnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnUGFzc3dvcmRSZXNldEluaXRDb21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGZpeHR1cmU6IENvbXBvbmVudEZpeHR1cmU8UGFzc3dvcmRSZXNldEluaXRDb21wb25lbnQ+O1xuICAgIGxldCBjb21wOiBQYXNzd29yZFJlc2V0SW5pdENvbXBvbmVudDtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgIGltcG9ydHM6IFtDb29wY3ljbGVUZXN0TW9kdWxlXSxcbiAgICAgICAgZGVjbGFyYXRpb25zOiBbUGFzc3dvcmRSZXNldEluaXRDb21wb25lbnRdLFxuICAgICAgICBwcm92aWRlcnM6IFtGb3JtQnVpbGRlcl1cbiAgICAgIH0pXG4gICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKFBhc3N3b3JkUmVzZXRJbml0Q29tcG9uZW50LCAnJylcbiAgICAgICAgLmNyZWF0ZUNvbXBvbmVudChQYXNzd29yZFJlc2V0SW5pdENvbXBvbmVudCk7XG4gICAgICBjb21wID0gZml4dHVyZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICB9KTtcblxuICAgIGl0KCdzZXRzIGZvY3VzIGFmdGVyIHRoZSB2aWV3IGhhcyBiZWVuIGluaXRpYWxpemVkJywgKCkgPT4ge1xuICAgICAgY29uc3Qgbm9kZSA9IHtcbiAgICAgICAgZm9jdXMoKTogdm9pZCB7fVxuICAgICAgfTtcbiAgICAgIGNvbXAuZW1haWwgPSBuZXcgRWxlbWVudFJlZihub2RlKTtcbiAgICAgIHNweU9uKG5vZGUsICdmb2N1cycpO1xuXG4gICAgICBjb21wLm5nQWZ0ZXJWaWV3SW5pdCgpO1xuXG4gICAgICBleHBlY3Qobm9kZS5mb2N1cykudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgIH0pO1xuXG4gICAgaXQoJ25vdGlmaWVzIG9mIHN1Y2Nlc3MgdXBvbiBzdWNjZXNzZnVsIHJlcXVlc3RSZXNldCcsIGluamVjdChbUGFzc3dvcmRSZXNldEluaXRTZXJ2aWNlXSwgKHNlcnZpY2U6IFBhc3N3b3JkUmVzZXRJbml0U2VydmljZSkgPT4ge1xuICAgICAgc3B5T24oc2VydmljZSwgJ3NhdmUnKS5hbmQucmV0dXJuVmFsdWUob2Yoe30pKTtcbiAgICAgIGNvbXAucmVzZXRSZXF1ZXN0Rm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgZW1haWw6ICd1c2VyQGRvbWFpbi5jb20nXG4gICAgICB9KTtcblxuICAgICAgY29tcC5yZXF1ZXN0UmVzZXQoKTtcblxuICAgICAgZXhwZWN0KHNlcnZpY2Uuc2F2ZSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoJ3VzZXJAZG9tYWluLmNvbScpO1xuICAgICAgZXhwZWN0KGNvbXAuc3VjY2VzcykudG9CZSh0cnVlKTtcbiAgICB9KSk7XG5cbiAgICBpdCgnbm8gbm90aWZpY2F0aW9uIG9mIHN1Y2Nlc3MgdXBvbiBlcnJvciByZXNwb25zZScsIGluamVjdChbUGFzc3dvcmRSZXNldEluaXRTZXJ2aWNlXSwgKHNlcnZpY2U6IFBhc3N3b3JkUmVzZXRJbml0U2VydmljZSkgPT4ge1xuICAgICAgc3B5T24oc2VydmljZSwgJ3NhdmUnKS5hbmQucmV0dXJuVmFsdWUoXG4gICAgICAgIHRocm93RXJyb3Ioe1xuICAgICAgICAgIHN0YXR1czogNTAzLFxuICAgICAgICAgIGRhdGE6ICdzb21ldGhpbmcgZWxzZSdcbiAgICAgICAgfSlcbiAgICAgICk7XG4gICAgICBjb21wLnJlc2V0UmVxdWVzdEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgIGVtYWlsOiAndXNlckBkb21haW4uY29tJ1xuICAgICAgfSk7XG4gICAgICBjb21wLnJlcXVlc3RSZXNldCgpO1xuXG4gICAgICBleHBlY3Qoc2VydmljZS5zYXZlKS50b0hhdmVCZWVuQ2FsbGVkV2l0aCgndXNlckBkb21haW4uY29tJyk7XG4gICAgICBleHBlY3QoY29tcC5zdWNjZXNzKS50b0JlKGZhbHNlKTtcbiAgICB9KSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=