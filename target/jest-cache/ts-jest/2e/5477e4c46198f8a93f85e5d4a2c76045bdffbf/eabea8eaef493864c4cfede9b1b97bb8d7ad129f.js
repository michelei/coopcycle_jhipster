"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
let ProductDetailComponent = class ProductDetailComponent {
    constructor(activatedRoute) {
        this.activatedRoute = activatedRoute;
        this.product = null;
    }
    ngOnInit() {
        this.activatedRoute.data.subscribe(({ product }) => (this.product = product));
    }
    previousState() {
        window.history.back();
    }
};
ProductDetailComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-product-detail',
        template: require('./product-detail.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [router_1.ActivatedRoute])
], ProductDetailComponent);
exports.ProductDetailComponent = ProductDetailComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvcHJvZHVjdC9wcm9kdWN0LWRldGFpbC5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBQ2xELDRDQUFpRDtBQVFqRCxJQUFhLHNCQUFzQixHQUFuQyxNQUFhLHNCQUFzQjtJQUdqQyxZQUFzQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFGcEQsWUFBTyxHQUFvQixJQUFJLENBQUM7SUFFdUIsQ0FBQztJQUV4RCxRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELGFBQWE7UUFDWCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7Q0FDRixDQUFBO0FBWlksc0JBQXNCO0lBSmxDLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLGtCQUFhLGlDQUFpQyxDQUFBO0tBQy9DLENBQUM7NkNBSXNDLHVCQUFjO0dBSHpDLHNCQUFzQixDQVlsQztBQVpZLHdEQUFzQiIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9lbnRpdGllcy9wcm9kdWN0L3Byb2R1Y3QtZGV0YWlsLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBJUHJvZHVjdCB9IGZyb20gJ2FwcC9zaGFyZWQvbW9kZWwvcHJvZHVjdC5tb2RlbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1wcm9kdWN0LWRldGFpbCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9kdWN0LWRldGFpbC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgUHJvZHVjdERldGFpbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHByb2R1Y3Q6IElQcm9kdWN0IHwgbnVsbCA9IG51bGw7XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge31cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLmRhdGEuc3Vic2NyaWJlKCh7IHByb2R1Y3QgfSkgPT4gKHRoaXMucHJvZHVjdCA9IHByb2R1Y3QpKTtcbiAgfVxuXG4gIHByZXZpb3VzU3RhdGUoKTogdm9pZCB7XG4gICAgd2luZG93Lmhpc3RvcnkuYmFjaygpO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=