"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const log_model_1 = require("./log.model");
const logs_service_1 = require("./logs.service");
let LogsComponent = class LogsComponent {
    constructor(logsService) {
        this.logsService = logsService;
        this.filter = '';
        this.orderProp = 'name';
        this.reverse = false;
    }
    ngOnInit() {
        this.findAndExtractLoggers();
    }
    changeLevel(name, level) {
        this.logsService.changeLevel(name, level).subscribe(() => this.findAndExtractLoggers());
    }
    findAndExtractLoggers() {
        this.logsService
            .findAll()
            .subscribe((response) => (this.loggers = Object.entries(response.loggers).map((logger) => new log_model_1.Log(logger[0], logger[1].effectiveLevel))));
    }
};
LogsComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-logs',
        template: require('./logs.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [logs_service_1.LogsService])
], LogsComponent);
exports.LogsComponent = LogsComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vbG9ncy9sb2dzLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBa0Q7QUFFbEQsMkNBQWtFO0FBQ2xFLGlEQUE2QztBQU03QyxJQUFhLGFBQWEsR0FBMUIsTUFBYSxhQUFhO0lBTXhCLFlBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBSjVDLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFDWixjQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ25CLFlBQU8sR0FBRyxLQUFLLENBQUM7SUFFK0IsQ0FBQztJQUVoRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFZLEVBQUUsS0FBWTtRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLENBQUM7SUFDMUYsQ0FBQztJQUVPLHFCQUFxQjtRQUMzQixJQUFJLENBQUMsV0FBVzthQUNiLE9BQU8sRUFBRTthQUNULFNBQVMsQ0FDUixDQUFDLFFBQXlCLEVBQUUsRUFBRSxDQUM1QixDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBd0IsRUFBRSxFQUFFLENBQUMsSUFBSSxlQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQ3BJLENBQUM7SUFDTixDQUFDO0NBQ0YsQ0FBQTtBQXhCWSxhQUFhO0lBSnpCLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsVUFBVTtRQUNwQixrQkFBYSx1QkFBdUIsQ0FBQTtLQUNyQyxDQUFDOzZDQU9pQywwQkFBVztHQU5qQyxhQUFhLENBd0J6QjtBQXhCWSxzQ0FBYSIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi9sb2dzL2xvZ3MuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IExvZywgTG9nZ2Vyc1Jlc3BvbnNlLCBMb2dnZXIsIExldmVsIH0gZnJvbSAnLi9sb2cubW9kZWwnO1xuaW1wb3J0IHsgTG9nc1NlcnZpY2UgfSBmcm9tICcuL2xvZ3Muc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1sb2dzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvZ3MuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIExvZ3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBsb2dnZXJzPzogTG9nW107XG4gIGZpbHRlciA9ICcnO1xuICBvcmRlclByb3AgPSAnbmFtZSc7XG4gIHJldmVyc2UgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvZ3NTZXJ2aWNlOiBMb2dzU2VydmljZSkge31cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmZpbmRBbmRFeHRyYWN0TG9nZ2VycygpO1xuICB9XG5cbiAgY2hhbmdlTGV2ZWwobmFtZTogc3RyaW5nLCBsZXZlbDogTGV2ZWwpOiB2b2lkIHtcbiAgICB0aGlzLmxvZ3NTZXJ2aWNlLmNoYW5nZUxldmVsKG5hbWUsIGxldmVsKS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5maW5kQW5kRXh0cmFjdExvZ2dlcnMoKSk7XG4gIH1cblxuICBwcml2YXRlIGZpbmRBbmRFeHRyYWN0TG9nZ2VycygpOiB2b2lkIHtcbiAgICB0aGlzLmxvZ3NTZXJ2aWNlXG4gICAgICAuZmluZEFsbCgpXG4gICAgICAuc3Vic2NyaWJlKFxuICAgICAgICAocmVzcG9uc2U6IExvZ2dlcnNSZXNwb25zZSkgPT5cbiAgICAgICAgICAodGhpcy5sb2dnZXJzID0gT2JqZWN0LmVudHJpZXMocmVzcG9uc2UubG9nZ2VycykubWFwKChsb2dnZXI6IFtzdHJpbmcsIExvZ2dlcl0pID0+IG5ldyBMb2cobG9nZ2VyWzBdLCBsb2dnZXJbMV0uZWZmZWN0aXZlTGV2ZWwpKSlcbiAgICAgICk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==