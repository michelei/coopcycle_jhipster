"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const http_1 = require("@angular/common/http");
const ng_jhipster_1 = require("ng-jhipster");
const ngx_webstorage_1 = require("ngx-webstorage");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const state_storage_service_1 = require("app/core/auth/state-storage.service");
const app_constants_1 = require("app/app.constants");
let AccountService = class AccountService {
    constructor(languageService, sessionStorage, http, stateStorageService, router) {
        this.languageService = languageService;
        this.sessionStorage = sessionStorage;
        this.http = http;
        this.stateStorageService = stateStorageService;
        this.router = router;
        this.userIdentity = null;
        this.authenticationState = new rxjs_1.ReplaySubject(1);
    }
    save(account) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/account', account);
    }
    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticationState.next(this.userIdentity);
    }
    hasAnyAuthority(authorities) {
        if (!this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        if (!Array.isArray(authorities)) {
            authorities = [authorities];
        }
        return this.userIdentity.authorities.some((authority) => authorities.includes(authority));
    }
    identity(force) {
        if (!this.accountCache$ || force || !this.isAuthenticated()) {
            this.accountCache$ = this.fetch().pipe(operators_1.catchError(() => {
                return rxjs_1.of(null);
            }), operators_1.tap((account) => {
                this.authenticate(account);
                // After retrieve the account info, the language will be changed to
                // the user's preferred language configured in the account setting
                if (account && account.langKey) {
                    const langKey = this.sessionStorage.retrieve('locale') || account.langKey;
                    this.languageService.changeLanguage(langKey);
                }
                if (account) {
                    this.navigateToStoredUrl();
                }
            }), operators_1.shareReplay());
        }
        return this.accountCache$;
    }
    isAuthenticated() {
        return this.userIdentity !== null;
    }
    getAuthenticationState() {
        return this.authenticationState.asObservable();
    }
    getImageUrl() {
        return this.userIdentity ? this.userIdentity.imageUrl : '';
    }
    fetch() {
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/account');
    }
    navigateToStoredUrl() {
        // previousState can be set in the authExpiredInterceptor and in the userRouteAccessService
        // if login is successful, go to stored previousState and clear previousState
        const previousUrl = this.stateStorageService.getUrl();
        if (previousUrl) {
            this.stateStorageService.clearUrl();
            this.router.navigateByUrl(previousUrl);
        }
    }
};
AccountService = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [ng_jhipster_1.JhiLanguageService,
        ngx_webstorage_1.SessionStorageService,
        http_1.HttpClient,
        state_storage_service_1.StateStorageService,
        router_1.Router])
], AccountService);
exports.AccountService = AccountService;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9hdXRoL2FjY291bnQuc2VydmljZS50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBMkM7QUFDM0MsNENBQXlDO0FBQ3pDLCtDQUFrRDtBQUNsRCw2Q0FBaUQ7QUFDakQsbURBQXVEO0FBQ3ZELCtCQUFxRDtBQUNyRCw4Q0FBOEQ7QUFDOUQsK0VBQTBFO0FBRTFFLHFEQUFtRDtBQUluRCxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBS3pCLFlBQ1UsZUFBbUMsRUFDbkMsY0FBcUMsRUFDckMsSUFBZ0IsRUFDaEIsbUJBQXdDLEVBQ3hDLE1BQWM7UUFKZCxvQkFBZSxHQUFmLGVBQWUsQ0FBb0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQXVCO1FBQ3JDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBVGhCLGlCQUFZLEdBQW1CLElBQUksQ0FBQztRQUNwQyx3QkFBbUIsR0FBRyxJQUFJLG9CQUFhLENBQWlCLENBQUMsQ0FBQyxDQUFDO0lBU2hFLENBQUM7SUFFSixJQUFJLENBQUMsT0FBZ0I7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4QkFBYyxHQUFHLGFBQWEsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsWUFBWSxDQUFDLFFBQXdCO1FBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO1FBQzdCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxlQUFlLENBQUMsV0FBOEI7UUFDNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUN4RCxPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDL0IsV0FBVyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDN0I7UUFDRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQWlCLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNwRyxDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQWU7UUFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFO1lBQzNELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FDcEMsc0JBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsT0FBTyxTQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsQ0FBQyxDQUFDLEVBQ0YsZUFBRyxDQUFDLENBQUMsT0FBdUIsRUFBRSxFQUFFO2dCQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUUzQixtRUFBbUU7Z0JBQ25FLGtFQUFrRTtnQkFDbEUsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtvQkFDOUIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQztvQkFDMUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzlDO2dCQUVELElBQUksT0FBTyxFQUFFO29CQUNYLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2lCQUM1QjtZQUNILENBQUMsQ0FBQyxFQUNGLHVCQUFXLEVBQUUsQ0FDZCxDQUFDO1NBQ0g7UUFDRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztJQUVELGVBQWU7UUFDYixPQUFPLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDO0lBQ3BDLENBQUM7SUFFRCxzQkFBc0I7UUFDcEIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDakQsQ0FBQztJQUVELFdBQVc7UUFDVCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVPLEtBQUs7UUFDWCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFVLDhCQUFjLEdBQUcsYUFBYSxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVPLG1CQUFtQjtRQUN6QiwyRkFBMkY7UUFDM0YsNkVBQTZFO1FBQzdFLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0RCxJQUFJLFdBQVcsRUFBRTtZQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7Q0FDRixDQUFBO0FBbkZZLGNBQWM7SUFEMUIsaUJBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzs2Q0FPTixnQ0FBa0I7UUFDbkIsc0NBQXFCO1FBQy9CLGlCQUFVO1FBQ0ssMkNBQW1CO1FBQ2hDLGVBQU07R0FWYixjQUFjLENBbUYxQjtBQW5GWSx3Q0FBYyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSmhpTGFuZ3VhZ2VTZXJ2aWNlIH0gZnJvbSAnbmctamhpcHN0ZXInO1xuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXdlYnN0b3JhZ2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgUmVwbGF5U3ViamVjdCwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHNoYXJlUmVwbGF5LCB0YXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnO1xuXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTCB9IGZyb20gJ2FwcC9hcHAuY29uc3RhbnRzJztcbmltcG9ydCB7IEFjY291bnQgfSBmcm9tICdhcHAvY29yZS91c2VyL2FjY291bnQubW9kZWwnO1xuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIEFjY291bnRTZXJ2aWNlIHtcbiAgcHJpdmF0ZSB1c2VySWRlbnRpdHk6IEFjY291bnQgfCBudWxsID0gbnVsbDtcbiAgcHJpdmF0ZSBhdXRoZW50aWNhdGlvblN0YXRlID0gbmV3IFJlcGxheVN1YmplY3Q8QWNjb3VudCB8IG51bGw+KDEpO1xuICBwcml2YXRlIGFjY291bnRDYWNoZSQ/OiBPYnNlcnZhYmxlPEFjY291bnQgfCBudWxsPjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGxhbmd1YWdlU2VydmljZTogSmhpTGFuZ3VhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZSxcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSBzdGF0ZVN0b3JhZ2VTZXJ2aWNlOiBTdGF0ZVN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcbiAgKSB7fVxuXG4gIHNhdmUoYWNjb3VudDogQWNjb3VudCk6IE9ic2VydmFibGU8e30+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoU0VSVkVSX0FQSV9VUkwgKyAnYXBpL2FjY291bnQnLCBhY2NvdW50KTtcbiAgfVxuXG4gIGF1dGhlbnRpY2F0ZShpZGVudGl0eTogQWNjb3VudCB8IG51bGwpOiB2b2lkIHtcbiAgICB0aGlzLnVzZXJJZGVudGl0eSA9IGlkZW50aXR5O1xuICAgIHRoaXMuYXV0aGVudGljYXRpb25TdGF0ZS5uZXh0KHRoaXMudXNlcklkZW50aXR5KTtcbiAgfVxuXG4gIGhhc0FueUF1dGhvcml0eShhdXRob3JpdGllczogc3RyaW5nW10gfCBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBpZiAoIXRoaXMudXNlcklkZW50aXR5IHx8ICF0aGlzLnVzZXJJZGVudGl0eS5hdXRob3JpdGllcykge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBpZiAoIUFycmF5LmlzQXJyYXkoYXV0aG9yaXRpZXMpKSB7XG4gICAgICBhdXRob3JpdGllcyA9IFthdXRob3JpdGllc107XG4gICAgfVxuICAgIHJldHVybiB0aGlzLnVzZXJJZGVudGl0eS5hdXRob3JpdGllcy5zb21lKChhdXRob3JpdHk6IHN0cmluZykgPT4gYXV0aG9yaXRpZXMuaW5jbHVkZXMoYXV0aG9yaXR5KSk7XG4gIH1cblxuICBpZGVudGl0eShmb3JjZT86IGJvb2xlYW4pOiBPYnNlcnZhYmxlPEFjY291bnQgfCBudWxsPiB7XG4gICAgaWYgKCF0aGlzLmFjY291bnRDYWNoZSQgfHwgZm9yY2UgfHwgIXRoaXMuaXNBdXRoZW50aWNhdGVkKCkpIHtcbiAgICAgIHRoaXMuYWNjb3VudENhY2hlJCA9IHRoaXMuZmV0Y2goKS5waXBlKFxuICAgICAgICBjYXRjaEVycm9yKCgpID0+IHtcbiAgICAgICAgICByZXR1cm4gb2YobnVsbCk7XG4gICAgICAgIH0pLFxuICAgICAgICB0YXAoKGFjY291bnQ6IEFjY291bnQgfCBudWxsKSA9PiB7XG4gICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGUoYWNjb3VudCk7XG5cbiAgICAgICAgICAvLyBBZnRlciByZXRyaWV2ZSB0aGUgYWNjb3VudCBpbmZvLCB0aGUgbGFuZ3VhZ2Ugd2lsbCBiZSBjaGFuZ2VkIHRvXG4gICAgICAgICAgLy8gdGhlIHVzZXIncyBwcmVmZXJyZWQgbGFuZ3VhZ2UgY29uZmlndXJlZCBpbiB0aGUgYWNjb3VudCBzZXR0aW5nXG4gICAgICAgICAgaWYgKGFjY291bnQgJiYgYWNjb3VudC5sYW5nS2V5KSB7XG4gICAgICAgICAgICBjb25zdCBsYW5nS2V5ID0gdGhpcy5zZXNzaW9uU3RvcmFnZS5yZXRyaWV2ZSgnbG9jYWxlJykgfHwgYWNjb3VudC5sYW5nS2V5O1xuICAgICAgICAgICAgdGhpcy5sYW5ndWFnZVNlcnZpY2UuY2hhbmdlTGFuZ3VhZ2UobGFuZ0tleSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGFjY291bnQpIHtcbiAgICAgICAgICAgIHRoaXMubmF2aWdhdGVUb1N0b3JlZFVybCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSksXG4gICAgICAgIHNoYXJlUmVwbGF5KClcbiAgICAgICk7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmFjY291bnRDYWNoZSQ7XG4gIH1cblxuICBpc0F1dGhlbnRpY2F0ZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMudXNlcklkZW50aXR5ICE9PSBudWxsO1xuICB9XG5cbiAgZ2V0QXV0aGVudGljYXRpb25TdGF0ZSgpOiBPYnNlcnZhYmxlPEFjY291bnQgfCBudWxsPiB7XG4gICAgcmV0dXJuIHRoaXMuYXV0aGVudGljYXRpb25TdGF0ZS5hc09ic2VydmFibGUoKTtcbiAgfVxuXG4gIGdldEltYWdlVXJsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudXNlcklkZW50aXR5ID8gdGhpcy51c2VySWRlbnRpdHkuaW1hZ2VVcmwgOiAnJztcbiAgfVxuXG4gIHByaXZhdGUgZmV0Y2goKTogT2JzZXJ2YWJsZTxBY2NvdW50PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8QWNjb3VudD4oU0VSVkVSX0FQSV9VUkwgKyAnYXBpL2FjY291bnQnKTtcbiAgfVxuXG4gIHByaXZhdGUgbmF2aWdhdGVUb1N0b3JlZFVybCgpOiB2b2lkIHtcbiAgICAvLyBwcmV2aW91c1N0YXRlIGNhbiBiZSBzZXQgaW4gdGhlIGF1dGhFeHBpcmVkSW50ZXJjZXB0b3IgYW5kIGluIHRoZSB1c2VyUm91dGVBY2Nlc3NTZXJ2aWNlXG4gICAgLy8gaWYgbG9naW4gaXMgc3VjY2Vzc2Z1bCwgZ28gdG8gc3RvcmVkIHByZXZpb3VzU3RhdGUgYW5kIGNsZWFyIHByZXZpb3VzU3RhdGVcbiAgICBjb25zdCBwcmV2aW91c1VybCA9IHRoaXMuc3RhdGVTdG9yYWdlU2VydmljZS5nZXRVcmwoKTtcbiAgICBpZiAocHJldmlvdXNVcmwpIHtcbiAgICAgIHRoaXMuc3RhdGVTdG9yYWdlU2VydmljZS5jbGVhclVybCgpO1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChwcmV2aW91c1VybCk7XG4gICAgfVxuICB9XG59XG4iXSwidmVyc2lvbiI6M30=