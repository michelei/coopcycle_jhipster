"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const mock_route_service_1 = require("../../../helpers/mock-route.service");
const activate_service_1 = require("app/account/activate/activate.service");
const activate_component_1 = require("app/account/activate/activate.component");
describe('Component Tests', () => {
    describe('ActivateComponent', () => {
        let comp;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [activate_component_1.ActivateComponent],
                providers: [
                    {
                        provide: router_1.ActivatedRoute,
                        useValue: new mock_route_service_1.MockActivatedRoute({ key: 'ABC123' })
                    }
                ]
            })
                .overrideTemplate(activate_component_1.ActivateComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            const fixture = testing_1.TestBed.createComponent(activate_component_1.ActivateComponent);
            comp = fixture.componentInstance;
        });
        it('calls activate.get with the key from params', testing_1.inject([activate_service_1.ActivateService], testing_1.fakeAsync((service) => {
            spyOn(service, 'get').and.returnValue(rxjs_1.of());
            comp.ngOnInit();
            testing_1.tick();
            expect(service.get).toHaveBeenCalledWith('ABC123');
        })));
        it('should set set success to true upon successful activation', testing_1.inject([activate_service_1.ActivateService], testing_1.fakeAsync((service) => {
            spyOn(service, 'get').and.returnValue(rxjs_1.of({}));
            comp.ngOnInit();
            testing_1.tick();
            expect(comp.error).toBe(false);
            expect(comp.success).toBe(true);
        })));
        it('should set set error to true upon activation failure', testing_1.inject([activate_service_1.ActivateService], testing_1.fakeAsync((service) => {
            spyOn(service, 'get').and.returnValue(rxjs_1.throwError('ERROR'));
            comp.ngOnInit();
            testing_1.tick();
            expect(comp.error).toBe(true);
            expect(comp.success).toBe(false);
        })));
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5jb21wb25lbnQuc3BlYy50cyIsIm1hcHBpbmdzIjoiOztBQUFBLG1EQUFnRjtBQUNoRiw0Q0FBaUQ7QUFDakQsK0JBQXNDO0FBRXRDLHNEQUEyRDtBQUMzRCw0RUFBeUU7QUFDekUsNEVBQXdFO0FBQ3hFLGdGQUE0RTtBQUU1RSxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLEVBQUU7UUFDakMsSUFBSSxJQUF1QixDQUFDO1FBRTVCLFVBQVUsQ0FBQyxlQUFLLENBQUMsR0FBRyxFQUFFO1lBQ3BCLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUFtQixDQUFDO2dCQUM5QixZQUFZLEVBQUUsQ0FBQyxzQ0FBaUIsQ0FBQztnQkFDakMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSx1QkFBYzt3QkFDdkIsUUFBUSxFQUFFLElBQUksdUNBQWtCLENBQUMsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLENBQUM7cUJBQ3BEO2lCQUNGO2FBQ0YsQ0FBQztpQkFDQyxnQkFBZ0IsQ0FBQyxzQ0FBaUIsRUFBRSxFQUFFLENBQUM7aUJBQ3ZDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVKLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxNQUFNLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxzQ0FBaUIsQ0FBQyxDQUFDO1lBQzNELElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsNkNBQTZDLEVBQUUsZ0JBQU0sQ0FDdEQsQ0FBQyxrQ0FBZSxDQUFDLEVBQ2pCLG1CQUFTLENBQUMsQ0FBQyxPQUF3QixFQUFFLEVBQUU7WUFDckMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsRUFBRSxDQUFDLENBQUM7WUFFNUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLGNBQUksRUFBRSxDQUFDO1lBRVAsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNyRCxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMkRBQTJELEVBQUUsZ0JBQU0sQ0FDcEUsQ0FBQyxrQ0FBZSxDQUFDLEVBQ2pCLG1CQUFTLENBQUMsQ0FBQyxPQUF3QixFQUFFLEVBQUU7WUFDckMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTlDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixjQUFJLEVBQUUsQ0FBQztZQUVQLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9CLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUNILENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxzREFBc0QsRUFBRSxnQkFBTSxDQUMvRCxDQUFDLGtDQUFlLENBQUMsRUFDakIsbUJBQVMsQ0FBQyxDQUFDLE9BQXdCLEVBQUUsRUFBRTtZQUNyQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsaUJBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBRTNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixjQUFJLEVBQUUsQ0FBQztZQUVQLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUNILENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUZXN0QmVkLCBhc3luYywgdGljaywgZmFrZUFzeW5jLCBpbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgb2YsIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgQ29vcGN5Y2xlVGVzdE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3Rlc3QubW9kdWxlJztcbmltcG9ydCB7IE1vY2tBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJy4uLy4uLy4uL2hlbHBlcnMvbW9jay1yb3V0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdGl2YXRlU2VydmljZSB9IGZyb20gJ2FwcC9hY2NvdW50L2FjdGl2YXRlL2FjdGl2YXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0aXZhdGVDb21wb25lbnQgfSBmcm9tICdhcHAvYWNjb3VudC9hY3RpdmF0ZS9hY3RpdmF0ZS5jb21wb25lbnQnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnQWN0aXZhdGVDb21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IEFjdGl2YXRlQ29tcG9uZW50O1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW0FjdGl2YXRlQ29tcG9uZW50XSxcbiAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgcHJvdmlkZTogQWN0aXZhdGVkUm91dGUsXG4gICAgICAgICAgICB1c2VWYWx1ZTogbmV3IE1vY2tBY3RpdmF0ZWRSb3V0ZSh7IGtleTogJ0FCQzEyMycgfSlcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0pXG4gICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKEFjdGl2YXRlQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgfSkpO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBjb25zdCBmaXh0dXJlID0gVGVzdEJlZC5jcmVhdGVDb21wb25lbnQoQWN0aXZhdGVDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgfSk7XG5cbiAgICBpdCgnY2FsbHMgYWN0aXZhdGUuZ2V0IHdpdGggdGhlIGtleSBmcm9tIHBhcmFtcycsIGluamVjdChcbiAgICAgIFtBY3RpdmF0ZVNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBBY3RpdmF0ZVNlcnZpY2UpID0+IHtcbiAgICAgICAgc3B5T24oc2VydmljZSwgJ2dldCcpLmFuZC5yZXR1cm5WYWx1ZShvZigpKTtcblxuICAgICAgICBjb21wLm5nT25Jbml0KCk7XG4gICAgICAgIHRpY2soKTtcblxuICAgICAgICBleHBlY3Qoc2VydmljZS5nZXQpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKCdBQkMxMjMnKTtcbiAgICAgIH0pXG4gICAgKSk7XG5cbiAgICBpdCgnc2hvdWxkIHNldCBzZXQgc3VjY2VzcyB0byB0cnVlIHVwb24gc3VjY2Vzc2Z1bCBhY3RpdmF0aW9uJywgaW5qZWN0KFxuICAgICAgW0FjdGl2YXRlU2VydmljZV0sXG4gICAgICBmYWtlQXN5bmMoKHNlcnZpY2U6IEFjdGl2YXRlU2VydmljZSkgPT4ge1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnZ2V0JykuYW5kLnJldHVyblZhbHVlKG9mKHt9KSk7XG5cbiAgICAgICAgY29tcC5uZ09uSW5pdCgpO1xuICAgICAgICB0aWNrKCk7XG5cbiAgICAgICAgZXhwZWN0KGNvbXAuZXJyb3IpLnRvQmUoZmFsc2UpO1xuICAgICAgICBleHBlY3QoY29tcC5zdWNjZXNzKS50b0JlKHRydWUpO1xuICAgICAgfSlcbiAgICApKTtcblxuICAgIGl0KCdzaG91bGQgc2V0IHNldCBlcnJvciB0byB0cnVlIHVwb24gYWN0aXZhdGlvbiBmYWlsdXJlJywgaW5qZWN0KFxuICAgICAgW0FjdGl2YXRlU2VydmljZV0sXG4gICAgICBmYWtlQXN5bmMoKHNlcnZpY2U6IEFjdGl2YXRlU2VydmljZSkgPT4ge1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnZ2V0JykuYW5kLnJldHVyblZhbHVlKHRocm93RXJyb3IoJ0VSUk9SJykpO1xuXG4gICAgICAgIGNvbXAubmdPbkluaXQoKTtcbiAgICAgICAgdGljaygpO1xuXG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKHRydWUpO1xuICAgICAgICBleHBlY3QoY29tcC5zdWNjZXNzKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pXG4gICAgKSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=