"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const http_1 = require("@angular/common/http");
const operators_1 = require("rxjs/operators");
const moment = require("moment");
const app_constants_1 = require("app/app.constants");
const request_util_1 = require("app/shared/util/request-util");
let CourseService = class CourseService {
    constructor(http) {
        this.http = http;
        this.resourceUrl = app_constants_1.SERVER_API_URL + 'api/courses';
    }
    create(course) {
        const copy = this.convertDateFromClient(course);
        return this.http
            .post(this.resourceUrl, copy, { observe: 'response' })
            .pipe(operators_1.map((res) => this.convertDateFromServer(res)));
    }
    update(course) {
        const copy = this.convertDateFromClient(course);
        return this.http
            .put(this.resourceUrl, copy, { observe: 'response' })
            .pipe(operators_1.map((res) => this.convertDateFromServer(res)));
    }
    find(id) {
        return this.http
            .get(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(operators_1.map((res) => this.convertDateFromServer(res)));
    }
    query(req) {
        const options = request_util_1.createRequestOption(req);
        return this.http
            .get(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(operators_1.map((res) => this.convertDateArrayFromServer(res)));
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    convertDateFromClient(course) {
        const copy = Object.assign({}, course, {
            deliveryTime: course.deliveryTime && course.deliveryTime.isValid() ? course.deliveryTime.toJSON() : undefined
        });
        return copy;
    }
    convertDateFromServer(res) {
        if (res.body) {
            res.body.deliveryTime = res.body.deliveryTime ? moment(res.body.deliveryTime) : undefined;
        }
        return res;
    }
    convertDateArrayFromServer(res) {
        if (res.body) {
            res.body.forEach((course) => {
                course.deliveryTime = course.deliveryTime ? moment(course.deliveryTime) : undefined;
            });
        }
        return res;
    }
};
CourseService = tslib_1.__decorate([
    core_1.Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [http_1.HttpClient])
], CourseService);
exports.CourseService = CourseService;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvZW50aXRpZXMvY291cnNlL2NvdXJzZS5zZXJ2aWNlLnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUEyQztBQUMzQywrQ0FBZ0U7QUFFaEUsOENBQXFDO0FBQ3JDLGlDQUFpQztBQUVqQyxxREFBbUQ7QUFDbkQsK0RBQW1FO0FBT25FLElBQWEsYUFBYSxHQUExQixNQUFhLGFBQWE7SUFHeEIsWUFBc0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUYvQixnQkFBVyxHQUFHLDhCQUFjLEdBQUcsYUFBYSxDQUFDO0lBRVgsQ0FBQztJQUUxQyxNQUFNLENBQUMsTUFBZTtRQUNwQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEQsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLElBQUksQ0FBVSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM5RCxJQUFJLENBQUMsZUFBRyxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsTUFBTSxDQUFDLE1BQWU7UUFDcEIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hELE9BQU8sSUFBSSxDQUFDLElBQUk7YUFDYixHQUFHLENBQVUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDN0QsSUFBSSxDQUFDLGVBQUcsQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELElBQUksQ0FBQyxFQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLEdBQUcsQ0FBVSxHQUFHLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDbEUsSUFBSSxDQUFDLGVBQUcsQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELEtBQUssQ0FBQyxHQUFTO1FBQ2IsTUFBTSxPQUFPLEdBQUcsa0NBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLEdBQUcsQ0FBWSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDMUUsSUFBSSxDQUFDLGVBQUcsQ0FBQyxDQUFDLEdBQTRCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELE1BQU0sQ0FBQyxFQUFVO1FBQ2YsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRVMscUJBQXFCLENBQUMsTUFBZTtRQUM3QyxNQUFNLElBQUksR0FBWSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLEVBQUU7WUFDOUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxZQUFZLElBQUksTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUztTQUM5RyxDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFUyxxQkFBcUIsQ0FBQyxHQUF1QjtRQUNyRCxJQUFJLEdBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDWixHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztTQUMzRjtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVTLDBCQUEwQixDQUFDLEdBQTRCO1FBQy9ELElBQUksR0FBRyxDQUFDLElBQUksRUFBRTtZQUNaLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBZSxFQUFFLEVBQUU7Z0JBQ25DLE1BQU0sQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQ3RGLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7Q0FDRixDQUFBO0FBMURZLGFBQWE7SUFEekIsaUJBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzs2Q0FJTCxpQkFBVTtHQUgzQixhQUFhLENBMER6QjtBQTFEWSxzQ0FBYSIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9lbnRpdGllcy9jb3Vyc2UvY291cnNlLnNlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudCc7XG5cbmltcG9ydCB7IFNFUlZFUl9BUElfVVJMIH0gZnJvbSAnYXBwL2FwcC5jb25zdGFudHMnO1xuaW1wb3J0IHsgY3JlYXRlUmVxdWVzdE9wdGlvbiB9IGZyb20gJ2FwcC9zaGFyZWQvdXRpbC9yZXF1ZXN0LXV0aWwnO1xuaW1wb3J0IHsgSUNvdXJzZSB9IGZyb20gJ2FwcC9zaGFyZWQvbW9kZWwvY291cnNlLm1vZGVsJztcblxudHlwZSBFbnRpdHlSZXNwb25zZVR5cGUgPSBIdHRwUmVzcG9uc2U8SUNvdXJzZT47XG50eXBlIEVudGl0eUFycmF5UmVzcG9uc2VUeXBlID0gSHR0cFJlc3BvbnNlPElDb3Vyc2VbXT47XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgQ291cnNlU2VydmljZSB7XG4gIHB1YmxpYyByZXNvdXJjZVVybCA9IFNFUlZFUl9BUElfVVJMICsgJ2FwaS9jb3Vyc2VzJztcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgaHR0cDogSHR0cENsaWVudCkge31cblxuICBjcmVhdGUoY291cnNlOiBJQ291cnNlKTogT2JzZXJ2YWJsZTxFbnRpdHlSZXNwb25zZVR5cGU+IHtcbiAgICBjb25zdCBjb3B5ID0gdGhpcy5jb252ZXJ0RGF0ZUZyb21DbGllbnQoY291cnNlKTtcbiAgICByZXR1cm4gdGhpcy5odHRwXG4gICAgICAucG9zdDxJQ291cnNlPih0aGlzLnJlc291cmNlVXJsLCBjb3B5LCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSlcbiAgICAgIC5waXBlKG1hcCgocmVzOiBFbnRpdHlSZXNwb25zZVR5cGUpID0+IHRoaXMuY29udmVydERhdGVGcm9tU2VydmVyKHJlcykpKTtcbiAgfVxuXG4gIHVwZGF0ZShjb3Vyc2U6IElDb3Vyc2UpOiBPYnNlcnZhYmxlPEVudGl0eVJlc3BvbnNlVHlwZT4ge1xuICAgIGNvbnN0IGNvcHkgPSB0aGlzLmNvbnZlcnREYXRlRnJvbUNsaWVudChjb3Vyc2UpO1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5wdXQ8SUNvdXJzZT4odGhpcy5yZXNvdXJjZVVybCwgY29weSwgeyBvYnNlcnZlOiAncmVzcG9uc2UnIH0pXG4gICAgICAucGlwZShtYXAoKHJlczogRW50aXR5UmVzcG9uc2VUeXBlKSA9PiB0aGlzLmNvbnZlcnREYXRlRnJvbVNlcnZlcihyZXMpKSk7XG4gIH1cblxuICBmaW5kKGlkOiBudW1iZXIpOiBPYnNlcnZhYmxlPEVudGl0eVJlc3BvbnNlVHlwZT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5nZXQ8SUNvdXJzZT4oYCR7dGhpcy5yZXNvdXJjZVVybH0vJHtpZH1gLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSlcbiAgICAgIC5waXBlKG1hcCgocmVzOiBFbnRpdHlSZXNwb25zZVR5cGUpID0+IHRoaXMuY29udmVydERhdGVGcm9tU2VydmVyKHJlcykpKTtcbiAgfVxuXG4gIHF1ZXJ5KHJlcT86IGFueSk6IE9ic2VydmFibGU8RW50aXR5QXJyYXlSZXNwb25zZVR5cGU+IHtcbiAgICBjb25zdCBvcHRpb25zID0gY3JlYXRlUmVxdWVzdE9wdGlvbihyZXEpO1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5nZXQ8SUNvdXJzZVtdPih0aGlzLnJlc291cmNlVXJsLCB7IHBhcmFtczogb3B0aW9ucywgb2JzZXJ2ZTogJ3Jlc3BvbnNlJyB9KVxuICAgICAgLnBpcGUobWFwKChyZXM6IEVudGl0eUFycmF5UmVzcG9uc2VUeXBlKSA9PiB0aGlzLmNvbnZlcnREYXRlQXJyYXlGcm9tU2VydmVyKHJlcykpKTtcbiAgfVxuXG4gIGRlbGV0ZShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8e30+PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5kZWxldGUoYCR7dGhpcy5yZXNvdXJjZVVybH0vJHtpZH1gLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgY29udmVydERhdGVGcm9tQ2xpZW50KGNvdXJzZTogSUNvdXJzZSk6IElDb3Vyc2Uge1xuICAgIGNvbnN0IGNvcHk6IElDb3Vyc2UgPSBPYmplY3QuYXNzaWduKHt9LCBjb3Vyc2UsIHtcbiAgICAgIGRlbGl2ZXJ5VGltZTogY291cnNlLmRlbGl2ZXJ5VGltZSAmJiBjb3Vyc2UuZGVsaXZlcnlUaW1lLmlzVmFsaWQoKSA/IGNvdXJzZS5kZWxpdmVyeVRpbWUudG9KU09OKCkgOiB1bmRlZmluZWRcbiAgICB9KTtcbiAgICByZXR1cm4gY29weTtcbiAgfVxuXG4gIHByb3RlY3RlZCBjb252ZXJ0RGF0ZUZyb21TZXJ2ZXIocmVzOiBFbnRpdHlSZXNwb25zZVR5cGUpOiBFbnRpdHlSZXNwb25zZVR5cGUge1xuICAgIGlmIChyZXMuYm9keSkge1xuICAgICAgcmVzLmJvZHkuZGVsaXZlcnlUaW1lID0gcmVzLmJvZHkuZGVsaXZlcnlUaW1lID8gbW9tZW50KHJlcy5ib2R5LmRlbGl2ZXJ5VGltZSkgOiB1bmRlZmluZWQ7XG4gICAgfVxuICAgIHJldHVybiByZXM7XG4gIH1cblxuICBwcm90ZWN0ZWQgY29udmVydERhdGVBcnJheUZyb21TZXJ2ZXIocmVzOiBFbnRpdHlBcnJheVJlc3BvbnNlVHlwZSk6IEVudGl0eUFycmF5UmVzcG9uc2VUeXBlIHtcbiAgICBpZiAocmVzLmJvZHkpIHtcbiAgICAgIHJlcy5ib2R5LmZvckVhY2goKGNvdXJzZTogSUNvdXJzZSkgPT4ge1xuICAgICAgICBjb3Vyc2UuZGVsaXZlcnlUaW1lID0gY291cnNlLmRlbGl2ZXJ5VGltZSA/IG1vbWVudChjb3Vyc2UuZGVsaXZlcnlUaW1lKSA6IHVuZGVmaW5lZDtcbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gcmVzO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=