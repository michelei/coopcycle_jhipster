"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const health_service_1 = require("./health.service");
const health_modal_component_1 = require("./health-modal.component");
let HealthComponent = class HealthComponent {
    constructor(modalService, healthService) {
        this.modalService = modalService;
        this.healthService = healthService;
    }
    ngOnInit() {
        this.refresh();
    }
    getBadgeClass(statusState) {
        if (statusState === 'UP') {
            return 'badge-success';
        }
        else {
            return 'badge-danger';
        }
    }
    refresh() {
        this.healthService.checkHealth().subscribe(health => (this.health = health), (error) => {
            if (error.status === 503) {
                this.health = error.error;
            }
        });
    }
    showHealth(health) {
        const modalRef = this.modalService.open(health_modal_component_1.HealthModalComponent);
        modalRef.componentInstance.health = health;
    }
};
HealthComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-health',
        template: require('./health.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [ng_bootstrap_1.NgbModal, health_service_1.HealthService])
], HealthComponent);
exports.HealthComponent = HealthComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vaGVhbHRoL2hlYWx0aC5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBRWxELDZEQUFzRDtBQUV0RCxxREFBaUc7QUFDakcscUVBQWdFO0FBTWhFLElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFHMUIsWUFBb0IsWUFBc0IsRUFBVSxhQUE0QjtRQUE1RCxpQkFBWSxHQUFaLFlBQVksQ0FBVTtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO0lBQUcsQ0FBQztJQUVwRixRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCxhQUFhLENBQUMsV0FBeUI7UUFDckMsSUFBSSxXQUFXLEtBQUssSUFBSSxFQUFFO1lBQ3hCLE9BQU8sZUFBZSxDQUFDO1NBQ3hCO2FBQU07WUFDTCxPQUFPLGNBQWMsQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCxPQUFPO1FBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQ3hDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxFQUNoQyxDQUFDLEtBQXdCLEVBQUUsRUFBRTtZQUMzQixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO2dCQUN4QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7YUFDM0I7UUFDSCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCxVQUFVLENBQUMsTUFBZ0Q7UUFDekQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsNkNBQW9CLENBQUMsQ0FBQztRQUM5RCxRQUFRLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUM3QyxDQUFDO0NBQ0YsQ0FBQTtBQWhDWSxlQUFlO0lBSjNCLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsWUFBWTtRQUN0QixrQkFBYSx5QkFBeUIsQ0FBQTtLQUN2QyxDQUFDOzZDQUlrQyx1QkFBUSxFQUF5Qiw4QkFBYTtHQUhyRSxlQUFlLENBZ0MzQjtBQWhDWSwwQ0FBZSIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi9oZWFsdGgvaGVhbHRoLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBOZ2JNb2RhbCB9IGZyb20gJ0BuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwJztcblxuaW1wb3J0IHsgSGVhbHRoU2VydmljZSwgSGVhbHRoU3RhdHVzLCBIZWFsdGgsIEhlYWx0aEtleSwgSGVhbHRoRGV0YWlscyB9IGZyb20gJy4vaGVhbHRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgSGVhbHRoTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2hlYWx0aC1tb2RhbC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktaGVhbHRoJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWx0aC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgSGVhbHRoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgaGVhbHRoPzogSGVhbHRoO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBOZ2JNb2RhbCwgcHJpdmF0ZSBoZWFsdGhTZXJ2aWNlOiBIZWFsdGhTZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMucmVmcmVzaCgpO1xuICB9XG5cbiAgZ2V0QmFkZ2VDbGFzcyhzdGF0dXNTdGF0ZTogSGVhbHRoU3RhdHVzKTogc3RyaW5nIHtcbiAgICBpZiAoc3RhdHVzU3RhdGUgPT09ICdVUCcpIHtcbiAgICAgIHJldHVybiAnYmFkZ2Utc3VjY2Vzcyc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnYmFkZ2UtZGFuZ2VyJztcbiAgICB9XG4gIH1cblxuICByZWZyZXNoKCk6IHZvaWQge1xuICAgIHRoaXMuaGVhbHRoU2VydmljZS5jaGVja0hlYWx0aCgpLnN1YnNjcmliZShcbiAgICAgIGhlYWx0aCA9PiAodGhpcy5oZWFsdGggPSBoZWFsdGgpLFxuICAgICAgKGVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAoZXJyb3Iuc3RhdHVzID09PSA1MDMpIHtcbiAgICAgICAgICB0aGlzLmhlYWx0aCA9IGVycm9yLmVycm9yO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHNob3dIZWFsdGgoaGVhbHRoOiB7IGtleTogSGVhbHRoS2V5OyB2YWx1ZTogSGVhbHRoRGV0YWlscyB9KTogdm9pZCB7XG4gICAgY29uc3QgbW9kYWxSZWYgPSB0aGlzLm1vZGFsU2VydmljZS5vcGVuKEhlYWx0aE1vZGFsQ29tcG9uZW50KTtcbiAgICBtb2RhbFJlZi5jb21wb25lbnRJbnN0YW5jZS5oZWFsdGggPSBoZWFsdGg7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==