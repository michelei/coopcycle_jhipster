"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
let PasswordStrengthBarComponent = class PasswordStrengthBarComponent {
    constructor(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.colors = ['#F00', '#F90', '#FF0', '#9F0', '#0F0'];
    }
    measureStrength(p) {
        let force = 0;
        const regex = /[$-/:-?{-~!"^_`[\]]/g; // "
        const lowerLetters = /[a-z]+/.test(p);
        const upperLetters = /[A-Z]+/.test(p);
        const numbers = /[0-9]+/.test(p);
        const symbols = regex.test(p);
        const flags = [lowerLetters, upperLetters, numbers, symbols];
        const passedMatches = flags.filter((isMatchedFlag) => {
            return isMatchedFlag === true;
        }).length;
        force += 2 * p.length + (p.length >= 10 ? 1 : 0);
        force += passedMatches * 10;
        // penalty (short password)
        force = p.length <= 6 ? Math.min(force, 10) : force;
        // penalty (poor variety of characters)
        force = passedMatches === 1 ? Math.min(force, 10) : force;
        force = passedMatches === 2 ? Math.min(force, 20) : force;
        force = passedMatches === 3 ? Math.min(force, 40) : force;
        return force;
    }
    getColor(s) {
        let idx = 0;
        if (s <= 10) {
            idx = 0;
        }
        else if (s <= 20) {
            idx = 1;
        }
        else if (s <= 30) {
            idx = 2;
        }
        else if (s <= 40) {
            idx = 3;
        }
        else {
            idx = 4;
        }
        return { idx: idx + 1, color: this.colors[idx] };
    }
    set passwordToCheck(password) {
        if (password) {
            const c = this.getColor(this.measureStrength(password));
            const element = this.elementRef.nativeElement;
            if (element.className) {
                this.renderer.removeClass(element, element.className);
            }
            const lis = element.getElementsByTagName('li');
            for (let i = 0; i < lis.length; i++) {
                if (i < c.idx) {
                    this.renderer.setStyle(lis[i], 'backgroundColor', c.color);
                }
                else {
                    this.renderer.setStyle(lis[i], 'backgroundColor', '#DDD');
                }
            }
        }
    }
};
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", String),
    tslib_1.__metadata("design:paramtypes", [String])
], PasswordStrengthBarComponent.prototype, "passwordToCheck", null);
PasswordStrengthBarComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-password-strength-bar',
        template: `
    <div id="strength">
      <small jhiTranslate="global.messages.validate.newpassword.strength">Password strength:</small>
      <ul id="strengthBar">
        <li class="point"></li>
        <li class="point"></li>
        <li class="point"></li>
        <li class="point"></li>
        <li class="point"></li>
      </ul>
    </div>
  `,
        styleUrls: []
    }),
    tslib_1.__metadata("design:paramtypes", [core_1.Renderer2, core_1.ElementRef])
], PasswordStrengthBarComponent);
exports.PasswordStrengthBarComponent = PasswordStrengthBarComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9wYXNzd29yZC9wYXNzd29yZC1zdHJlbmd0aC1iYXIuY29tcG9uZW50LnRzIiwibWFwcGluZ3MiOiI7OztBQUFBLHdDQUF3RTtBQWtCeEUsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUFHdkMsWUFBb0IsUUFBbUIsRUFBVSxVQUFzQjtRQUFuRCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQVUsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUZ2RSxXQUFNLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFFd0IsQ0FBQztJQUUzRSxlQUFlLENBQUMsQ0FBUztRQUN2QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxNQUFNLEtBQUssR0FBRyxzQkFBc0IsQ0FBQyxDQUFDLElBQUk7UUFDMUMsTUFBTSxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QyxNQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RDLE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakMsTUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUU5QixNQUFNLEtBQUssR0FBRyxDQUFDLFlBQVksRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzdELE1BQU0sYUFBYSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxhQUFzQixFQUFFLEVBQUU7WUFDNUQsT0FBTyxhQUFhLEtBQUssSUFBSSxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUVWLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELEtBQUssSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBRTVCLDJCQUEyQjtRQUMzQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFFcEQsdUNBQXVDO1FBQ3ZDLEtBQUssR0FBRyxhQUFhLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzFELEtBQUssR0FBRyxhQUFhLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzFELEtBQUssR0FBRyxhQUFhLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBRTFELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELFFBQVEsQ0FBQyxDQUFTO1FBQ2hCLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNYLEdBQUcsR0FBRyxDQUFDLENBQUM7U0FDVDthQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNsQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7YUFBTSxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEIsR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNUO2FBQU0sSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ2xCLEdBQUcsR0FBRyxDQUFDLENBQUM7U0FDVDthQUFNO1lBQ0wsR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNUO1FBQ0QsT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7SUFDbkQsQ0FBQztJQUdELElBQUksZUFBZSxDQUFDLFFBQWdCO1FBQ2xDLElBQUksUUFBUSxFQUFFO1lBQ1osTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7WUFDOUMsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3ZEO1lBQ0QsTUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9DLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFO29CQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVEO3FCQUFNO29CQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDM0Q7YUFDRjtTQUNGO0lBQ0gsQ0FBQztDQUNGLENBQUE7QUFqQkM7SUFEQyxZQUFLLEVBQUU7OzttRUFpQlA7QUFqRVUsNEJBQTRCO0lBaEJ4QyxnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLDJCQUEyQjtRQUNyQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7O0dBV1Q7UUFDRCxTQUFTLElBQWdDO0tBQzFDLENBQUM7NkNBSThCLGdCQUFTLEVBQXNCLGlCQUFVO0dBSDVELDRCQUE0QixDQWtFeEM7QUFsRVksb0VBQTRCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FjY291bnQvcGFzc3dvcmQvcGFzc3dvcmQtc3RyZW5ndGgtYmFyLmNvbXBvbmVudC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIElucHV0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLXBhc3N3b3JkLXN0cmVuZ3RoLWJhcicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGRpdiBpZD1cInN0cmVuZ3RoXCI+XG4gICAgICA8c21hbGwgamhpVHJhbnNsYXRlPVwiZ2xvYmFsLm1lc3NhZ2VzLnZhbGlkYXRlLm5ld3Bhc3N3b3JkLnN0cmVuZ3RoXCI+UGFzc3dvcmQgc3RyZW5ndGg6PC9zbWFsbD5cbiAgICAgIDx1bCBpZD1cInN0cmVuZ3RoQmFyXCI+XG4gICAgICAgIDxsaSBjbGFzcz1cInBvaW50XCI+PC9saT5cbiAgICAgICAgPGxpIGNsYXNzPVwicG9pbnRcIj48L2xpPlxuICAgICAgICA8bGkgY2xhc3M9XCJwb2ludFwiPjwvbGk+XG4gICAgICAgIDxsaSBjbGFzcz1cInBvaW50XCI+PC9saT5cbiAgICAgICAgPGxpIGNsYXNzPVwicG9pbnRcIj48L2xpPlxuICAgICAgPC91bD5cbiAgICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVVcmxzOiBbJ3Bhc3N3b3JkLXN0cmVuZ3RoLWJhci5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUGFzc3dvcmRTdHJlbmd0aEJhckNvbXBvbmVudCB7XG4gIGNvbG9ycyA9IFsnI0YwMCcsICcjRjkwJywgJyNGRjAnLCAnIzlGMCcsICcjMEYwJ107XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLCBwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYpIHt9XG5cbiAgbWVhc3VyZVN0cmVuZ3RoKHA6IHN0cmluZyk6IG51bWJlciB7XG4gICAgbGV0IGZvcmNlID0gMDtcbiAgICBjb25zdCByZWdleCA9IC9bJC0vOi0/ey1+IVwiXl9gW1xcXV0vZzsgLy8gXCJcbiAgICBjb25zdCBsb3dlckxldHRlcnMgPSAvW2Etel0rLy50ZXN0KHApO1xuICAgIGNvbnN0IHVwcGVyTGV0dGVycyA9IC9bQS1aXSsvLnRlc3QocCk7XG4gICAgY29uc3QgbnVtYmVycyA9IC9bMC05XSsvLnRlc3QocCk7XG4gICAgY29uc3Qgc3ltYm9scyA9IHJlZ2V4LnRlc3QocCk7XG5cbiAgICBjb25zdCBmbGFncyA9IFtsb3dlckxldHRlcnMsIHVwcGVyTGV0dGVycywgbnVtYmVycywgc3ltYm9sc107XG4gICAgY29uc3QgcGFzc2VkTWF0Y2hlcyA9IGZsYWdzLmZpbHRlcigoaXNNYXRjaGVkRmxhZzogYm9vbGVhbikgPT4ge1xuICAgICAgcmV0dXJuIGlzTWF0Y2hlZEZsYWcgPT09IHRydWU7XG4gICAgfSkubGVuZ3RoO1xuXG4gICAgZm9yY2UgKz0gMiAqIHAubGVuZ3RoICsgKHAubGVuZ3RoID49IDEwID8gMSA6IDApO1xuICAgIGZvcmNlICs9IHBhc3NlZE1hdGNoZXMgKiAxMDtcblxuICAgIC8vIHBlbmFsdHkgKHNob3J0IHBhc3N3b3JkKVxuICAgIGZvcmNlID0gcC5sZW5ndGggPD0gNiA/IE1hdGgubWluKGZvcmNlLCAxMCkgOiBmb3JjZTtcblxuICAgIC8vIHBlbmFsdHkgKHBvb3IgdmFyaWV0eSBvZiBjaGFyYWN0ZXJzKVxuICAgIGZvcmNlID0gcGFzc2VkTWF0Y2hlcyA9PT0gMSA/IE1hdGgubWluKGZvcmNlLCAxMCkgOiBmb3JjZTtcbiAgICBmb3JjZSA9IHBhc3NlZE1hdGNoZXMgPT09IDIgPyBNYXRoLm1pbihmb3JjZSwgMjApIDogZm9yY2U7XG4gICAgZm9yY2UgPSBwYXNzZWRNYXRjaGVzID09PSAzID8gTWF0aC5taW4oZm9yY2UsIDQwKSA6IGZvcmNlO1xuXG4gICAgcmV0dXJuIGZvcmNlO1xuICB9XG5cbiAgZ2V0Q29sb3IoczogbnVtYmVyKTogeyBpZHg6IG51bWJlcjsgY29sb3I6IHN0cmluZyB9IHtcbiAgICBsZXQgaWR4ID0gMDtcbiAgICBpZiAocyA8PSAxMCkge1xuICAgICAgaWR4ID0gMDtcbiAgICB9IGVsc2UgaWYgKHMgPD0gMjApIHtcbiAgICAgIGlkeCA9IDE7XG4gICAgfSBlbHNlIGlmIChzIDw9IDMwKSB7XG4gICAgICBpZHggPSAyO1xuICAgIH0gZWxzZSBpZiAocyA8PSA0MCkge1xuICAgICAgaWR4ID0gMztcbiAgICB9IGVsc2Uge1xuICAgICAgaWR4ID0gNDtcbiAgICB9XG4gICAgcmV0dXJuIHsgaWR4OiBpZHggKyAxLCBjb2xvcjogdGhpcy5jb2xvcnNbaWR4XSB9O1xuICB9XG5cbiAgQElucHV0KClcbiAgc2V0IHBhc3N3b3JkVG9DaGVjayhwYXNzd29yZDogc3RyaW5nKSB7XG4gICAgaWYgKHBhc3N3b3JkKSB7XG4gICAgICBjb25zdCBjID0gdGhpcy5nZXRDb2xvcih0aGlzLm1lYXN1cmVTdHJlbmd0aChwYXNzd29yZCkpO1xuICAgICAgY29uc3QgZWxlbWVudCA9IHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xuICAgICAgaWYgKGVsZW1lbnQuY2xhc3NOYW1lKSB7XG4gICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3MoZWxlbWVudCwgZWxlbWVudC5jbGFzc05hbWUpO1xuICAgICAgfVxuICAgICAgY29uc3QgbGlzID0gZWxlbWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnbGknKTtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGlmIChpIDwgYy5pZHgpIHtcbiAgICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGxpc1tpXSwgJ2JhY2tncm91bmRDb2xvcicsIGMuY29sb3IpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUobGlzW2ldLCAnYmFja2dyb3VuZENvbG9yJywgJyNEREQnKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl0sInZlcnNpb24iOjN9