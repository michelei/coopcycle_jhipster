"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
let HealthModalComponent = class HealthModalComponent {
    constructor(activeModal) {
        this.activeModal = activeModal;
    }
    readableValue(value) {
        if (this.health && this.health.key === 'diskSpace') {
            // Should display storage space in an human readable unit
            const val = value / 1073741824;
            if (val > 1) {
                // Value
                return val.toFixed(2) + ' GB';
            }
            else {
                return (value / 1048576).toFixed(2) + ' MB';
            }
        }
        if (typeof value === 'object') {
            return JSON.stringify(value);
        }
        else {
            return value.toString();
        }
    }
    dismiss() {
        this.activeModal.dismiss();
    }
};
HealthModalComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-health-modal',
        template: require('./health-modal.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [ng_bootstrap_1.NgbActiveModal])
], HealthModalComponent);
exports.HealthModalComponent = HealthModalComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWRtaW4vaGVhbHRoL2hlYWx0aC1tb2RhbC5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQTBDO0FBQzFDLDZEQUE0RDtBQVE1RCxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQUcvQixZQUFtQixXQUEyQjtRQUEzQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7SUFBRyxDQUFDO0lBRWxELGFBQWEsQ0FBQyxLQUFVO1FBQ3RCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxXQUFXLEVBQUU7WUFDbEQseURBQXlEO1lBQ3pELE1BQU0sR0FBRyxHQUFHLEtBQUssR0FBRyxVQUFVLENBQUM7WUFDL0IsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFO2dCQUNYLFFBQVE7Z0JBQ1IsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQzthQUMvQjtpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7YUFDN0M7U0FDRjtRQUVELElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDN0IsQ0FBQztDQUNGLENBQUE7QUEzQlksb0JBQW9CO0lBSmhDLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLGtCQUFhLCtCQUErQixDQUFBO0tBQzdDLENBQUM7NkNBSWdDLDZCQUFjO0dBSG5DLG9CQUFvQixDQTJCaEM7QUEzQlksb0RBQW9CIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FkbWluL2hlYWx0aC9oZWFsdGgtbW9kYWwuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdiQWN0aXZlTW9kYWwgfSBmcm9tICdAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcCc7XG5cbmltcG9ydCB7IEhlYWx0aEtleSwgSGVhbHRoRGV0YWlscyB9IGZyb20gJy4vaGVhbHRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdqaGktaGVhbHRoLW1vZGFsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWx0aC1tb2RhbC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgSGVhbHRoTW9kYWxDb21wb25lbnQge1xuICBoZWFsdGg/OiB7IGtleTogSGVhbHRoS2V5OyB2YWx1ZTogSGVhbHRoRGV0YWlscyB9O1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBhY3RpdmVNb2RhbDogTmdiQWN0aXZlTW9kYWwpIHt9XG5cbiAgcmVhZGFibGVWYWx1ZSh2YWx1ZTogYW55KTogc3RyaW5nIHtcbiAgICBpZiAodGhpcy5oZWFsdGggJiYgdGhpcy5oZWFsdGgua2V5ID09PSAnZGlza1NwYWNlJykge1xuICAgICAgLy8gU2hvdWxkIGRpc3BsYXkgc3RvcmFnZSBzcGFjZSBpbiBhbiBodW1hbiByZWFkYWJsZSB1bml0XG4gICAgICBjb25zdCB2YWwgPSB2YWx1ZSAvIDEwNzM3NDE4MjQ7XG4gICAgICBpZiAodmFsID4gMSkge1xuICAgICAgICAvLyBWYWx1ZVxuICAgICAgICByZXR1cm4gdmFsLnRvRml4ZWQoMikgKyAnIEdCJztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAodmFsdWUgLyAxMDQ4NTc2KS50b0ZpeGVkKDIpICsgJyBNQic7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh2YWx1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB2YWx1ZS50b1N0cmluZygpO1xuICAgIH1cbiAgfVxuXG4gIGRpc21pc3MoKTogdm9pZCB7XG4gICAgdGhpcy5hY3RpdmVNb2RhbC5kaXNtaXNzKCk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==