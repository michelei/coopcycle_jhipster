"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const forms_1 = require("@angular/forms");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const account_service_1 = require("app/core/auth/account.service");
const settings_component_1 = require("app/account/settings/settings.component");
describe('Component Tests', () => {
    describe('SettingsComponent', () => {
        let comp;
        let fixture;
        let mockAuth;
        const accountValues = {
            firstName: 'John',
            lastName: 'Doe',
            activated: true,
            email: 'john.doe@mail.com',
            langKey: 'en',
            login: 'john',
            authorities: [],
            imageUrl: ''
        };
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [settings_component_1.SettingsComponent],
                providers: [forms_1.FormBuilder]
            })
                .overrideTemplate(settings_component_1.SettingsComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(settings_component_1.SettingsComponent);
            comp = fixture.componentInstance;
            mockAuth = testing_1.TestBed.get(account_service_1.AccountService);
            mockAuth.setIdentityResponse(accountValues);
        });
        it('should send the current identity upon save', () => {
            // GIVEN
            mockAuth.saveSpy.and.returnValue(rxjs_1.of({}));
            const settingsFormValues = {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@mail.com',
                langKey: 'en'
            };
            // WHEN
            comp.ngOnInit();
            comp.save();
            // THEN
            expect(mockAuth.identitySpy).toHaveBeenCalled();
            expect(mockAuth.saveSpy).toHaveBeenCalledWith(accountValues);
            expect(mockAuth.authenticateSpy).toHaveBeenCalledWith(accountValues);
            expect(comp.settingsForm.value).toEqual(settingsFormValues);
        });
        it('should notify of success upon successful save', () => {
            // GIVEN
            mockAuth.saveSpy.and.returnValue(rxjs_1.of({}));
            // WHEN
            comp.ngOnInit();
            comp.save();
            // THEN
            expect(comp.success).toBe(true);
        });
        it('should notify of error upon failed save', () => {
            // GIVEN
            mockAuth.saveSpy.and.returnValue(rxjs_1.throwError('ERROR'));
            // WHEN
            comp.ngOnInit();
            comp.save();
            // THEN
            expect(comp.success).toBe(false);
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuc3BlYy50cyIsIm1hcHBpbmdzIjoiOztBQUFBLG1EQUF5RTtBQUN6RSwwQ0FBNkM7QUFDN0MsK0JBQXNDO0FBRXRDLHNEQUEyRDtBQUMzRCxtRUFBK0Q7QUFFL0QsZ0ZBQTRFO0FBRzVFLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7SUFDL0IsUUFBUSxDQUFDLG1CQUFtQixFQUFFLEdBQUcsRUFBRTtRQUNqQyxJQUFJLElBQXVCLENBQUM7UUFDNUIsSUFBSSxPQUE0QyxDQUFDO1FBQ2pELElBQUksUUFBNEIsQ0FBQztRQUNqQyxNQUFNLGFBQWEsR0FBWTtZQUM3QixTQUFTLEVBQUUsTUFBTTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLFNBQVMsRUFBRSxJQUFJO1lBQ2YsS0FBSyxFQUFFLG1CQUFtQjtZQUMxQixPQUFPLEVBQUUsSUFBSTtZQUNiLEtBQUssRUFBRSxNQUFNO1lBQ2IsV0FBVyxFQUFFLEVBQUU7WUFDZixRQUFRLEVBQUUsRUFBRTtTQUNiLENBQUM7UUFFRixVQUFVLENBQUMsZUFBSyxDQUFDLEdBQUcsRUFBRTtZQUNwQixpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUM3QixPQUFPLEVBQUUsQ0FBQyxpQ0FBbUIsQ0FBQztnQkFDOUIsWUFBWSxFQUFFLENBQUMsc0NBQWlCLENBQUM7Z0JBQ2pDLFNBQVMsRUFBRSxDQUFDLG1CQUFXLENBQUM7YUFDekIsQ0FBQztpQkFDQyxnQkFBZ0IsQ0FBQyxzQ0FBaUIsRUFBRSxFQUFFLENBQUM7aUJBQ3ZDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVKLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxlQUFlLENBQUMsc0NBQWlCLENBQUMsQ0FBQztZQUNyRCxJQUFJLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO1lBQ2pDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBYyxDQUFDLENBQUM7WUFDdkMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDRDQUE0QyxFQUFFLEdBQUcsRUFBRTtZQUNwRCxRQUFRO1lBQ1IsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sa0JBQWtCLEdBQUc7Z0JBQ3pCLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsS0FBSztnQkFDZixLQUFLLEVBQUUsbUJBQW1CO2dCQUMxQixPQUFPLEVBQUUsSUFBSTthQUNkLENBQUM7WUFFRixPQUFPO1lBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVaLE9BQU87WUFDUCxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDaEQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzlELENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLCtDQUErQyxFQUFFLEdBQUcsRUFBRTtZQUN2RCxRQUFRO1lBQ1IsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFNBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRXpDLE9BQU87WUFDUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRVosT0FBTztZQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHlDQUF5QyxFQUFFLEdBQUcsRUFBRTtZQUNqRCxRQUFRO1lBQ1IsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLGlCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUV0RCxPQUFPO1lBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVaLE9BQU87WUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRGaXh0dXJlLCBUZXN0QmVkLCBhc3luYyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IHRocm93RXJyb3IsIG9mIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IENvb3BjeWNsZVRlc3RNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi90ZXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gJ2FwcC9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcbmltcG9ydCB7IEFjY291bnQgfSBmcm9tICdhcHAvY29yZS91c2VyL2FjY291bnQubW9kZWwnO1xuaW1wb3J0IHsgU2V0dGluZ3NDb21wb25lbnQgfSBmcm9tICdhcHAvYWNjb3VudC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQnO1xuaW1wb3J0IHsgTW9ja0FjY291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vaGVscGVycy9tb2NrLWFjY291bnQuc2VydmljZSc7XG5cbmRlc2NyaWJlKCdDb21wb25lbnQgVGVzdHMnLCAoKSA9PiB7XG4gIGRlc2NyaWJlKCdTZXR0aW5nc0NvbXBvbmVudCcsICgpID0+IHtcbiAgICBsZXQgY29tcDogU2V0dGluZ3NDb21wb25lbnQ7XG4gICAgbGV0IGZpeHR1cmU6IENvbXBvbmVudEZpeHR1cmU8U2V0dGluZ3NDb21wb25lbnQ+O1xuICAgIGxldCBtb2NrQXV0aDogTW9ja0FjY291bnRTZXJ2aWNlO1xuICAgIGNvbnN0IGFjY291bnRWYWx1ZXM6IEFjY291bnQgPSB7XG4gICAgICBmaXJzdE5hbWU6ICdKb2huJyxcbiAgICAgIGxhc3ROYW1lOiAnRG9lJyxcbiAgICAgIGFjdGl2YXRlZDogdHJ1ZSxcbiAgICAgIGVtYWlsOiAnam9obi5kb2VAbWFpbC5jb20nLFxuICAgICAgbGFuZ0tleTogJ2VuJyxcbiAgICAgIGxvZ2luOiAnam9obicsXG4gICAgICBhdXRob3JpdGllczogW10sXG4gICAgICBpbWFnZVVybDogJydcbiAgICB9O1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW1NldHRpbmdzQ29tcG9uZW50XSxcbiAgICAgICAgcHJvdmlkZXJzOiBbRm9ybUJ1aWxkZXJdXG4gICAgICB9KVxuICAgICAgICAub3ZlcnJpZGVUZW1wbGF0ZShTZXR0aW5nc0NvbXBvbmVudCwgJycpXG4gICAgICAgIC5jb21waWxlQ29tcG9uZW50cygpO1xuICAgIH0pKTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KFNldHRpbmdzQ29tcG9uZW50KTtcbiAgICAgIGNvbXAgPSBmaXh0dXJlLmNvbXBvbmVudEluc3RhbmNlO1xuICAgICAgbW9ja0F1dGggPSBUZXN0QmVkLmdldChBY2NvdW50U2VydmljZSk7XG4gICAgICBtb2NrQXV0aC5zZXRJZGVudGl0eVJlc3BvbnNlKGFjY291bnRWYWx1ZXMpO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBzZW5kIHRoZSBjdXJyZW50IGlkZW50aXR5IHVwb24gc2F2ZScsICgpID0+IHtcbiAgICAgIC8vIEdJVkVOXG4gICAgICBtb2NrQXV0aC5zYXZlU3B5LmFuZC5yZXR1cm5WYWx1ZShvZih7fSkpO1xuICAgICAgY29uc3Qgc2V0dGluZ3NGb3JtVmFsdWVzID0ge1xuICAgICAgICBmaXJzdE5hbWU6ICdKb2huJyxcbiAgICAgICAgbGFzdE5hbWU6ICdEb2UnLFxuICAgICAgICBlbWFpbDogJ2pvaG4uZG9lQG1haWwuY29tJyxcbiAgICAgICAgbGFuZ0tleTogJ2VuJ1xuICAgICAgfTtcblxuICAgICAgLy8gV0hFTlxuICAgICAgY29tcC5uZ09uSW5pdCgpO1xuICAgICAgY29tcC5zYXZlKCk7XG5cbiAgICAgIC8vIFRIRU5cbiAgICAgIGV4cGVjdChtb2NrQXV0aC5pZGVudGl0eVNweSkudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgZXhwZWN0KG1vY2tBdXRoLnNhdmVTcHkpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKGFjY291bnRWYWx1ZXMpO1xuICAgICAgZXhwZWN0KG1vY2tBdXRoLmF1dGhlbnRpY2F0ZVNweSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoYWNjb3VudFZhbHVlcyk7XG4gICAgICBleHBlY3QoY29tcC5zZXR0aW5nc0Zvcm0udmFsdWUpLnRvRXF1YWwoc2V0dGluZ3NGb3JtVmFsdWVzKTtcbiAgICB9KTtcblxuICAgIGl0KCdzaG91bGQgbm90aWZ5IG9mIHN1Y2Nlc3MgdXBvbiBzdWNjZXNzZnVsIHNhdmUnLCAoKSA9PiB7XG4gICAgICAvLyBHSVZFTlxuICAgICAgbW9ja0F1dGguc2F2ZVNweS5hbmQucmV0dXJuVmFsdWUob2Yoe30pKTtcblxuICAgICAgLy8gV0hFTlxuICAgICAgY29tcC5uZ09uSW5pdCgpO1xuICAgICAgY29tcC5zYXZlKCk7XG5cbiAgICAgIC8vIFRIRU5cbiAgICAgIGV4cGVjdChjb21wLnN1Y2Nlc3MpLnRvQmUodHJ1ZSk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIG5vdGlmeSBvZiBlcnJvciB1cG9uIGZhaWxlZCBzYXZlJywgKCkgPT4ge1xuICAgICAgLy8gR0lWRU5cbiAgICAgIG1vY2tBdXRoLnNhdmVTcHkuYW5kLnJldHVyblZhbHVlKHRocm93RXJyb3IoJ0VSUk9SJykpO1xuXG4gICAgICAvLyBXSEVOXG4gICAgICBjb21wLm5nT25Jbml0KCk7XG4gICAgICBjb21wLnNhdmUoKTtcblxuICAgICAgLy8gVEhFTlxuICAgICAgZXhwZWN0KGNvbXAuc3VjY2VzcykudG9CZShmYWxzZSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=