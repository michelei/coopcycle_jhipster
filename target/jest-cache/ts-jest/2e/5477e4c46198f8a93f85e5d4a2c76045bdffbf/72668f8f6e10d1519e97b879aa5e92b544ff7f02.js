"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../test.module");
const metrics_component_1 = require("app/admin/metrics/metrics.component");
const metrics_service_1 = require("app/admin/metrics/metrics.service");
describe('Component Tests', () => {
    describe('MetricsComponent', () => {
        let comp;
        let fixture;
        let service;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [metrics_component_1.MetricsComponent]
            })
                .overrideTemplate(metrics_component_1.MetricsComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(metrics_component_1.MetricsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(metrics_service_1.MetricsService);
        });
        describe('refresh', () => {
            it('should call refresh on init', () => {
                // GIVEN
                const response = {
                    timers: {
                        service: 'test',
                        unrelatedKey: 'test'
                    },
                    gauges: {
                        'jcache.statistics': {
                            value: 2
                        },
                        unrelatedKey: 'test'
                    }
                };
                spyOn(service, 'getMetrics').and.returnValue(rxjs_1.of(response));
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(service.getMetrics).toHaveBeenCalled();
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vbWV0cmljcy9tZXRyaWNzLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlFO0FBQ3pFLCtCQUEwQjtBQUUxQixzREFBMkQ7QUFDM0QsMkVBQXVFO0FBQ3ZFLHVFQUFtRTtBQUVuRSxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLEVBQUU7UUFDaEMsSUFBSSxJQUFzQixDQUFDO1FBQzNCLElBQUksT0FBMkMsQ0FBQztRQUNoRCxJQUFJLE9BQXVCLENBQUM7UUFFNUIsVUFBVSxDQUFDLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDcEIsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQW1CLENBQUM7Z0JBQzlCLFlBQVksRUFBRSxDQUFDLG9DQUFnQixDQUFDO2FBQ2pDLENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMsb0NBQWdCLEVBQUUsRUFBRSxDQUFDO2lCQUN0QyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLG9DQUFnQixDQUFDLENBQUM7WUFDcEQsSUFBSSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztZQUNqQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdDQUFjLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFO1lBQ3ZCLEVBQUUsQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLEVBQUU7Z0JBQ3JDLFFBQVE7Z0JBQ1IsTUFBTSxRQUFRLEdBQUc7b0JBQ2YsTUFBTSxFQUFFO3dCQUNOLE9BQU8sRUFBRSxNQUFNO3dCQUNmLFlBQVksRUFBRSxNQUFNO3FCQUNyQjtvQkFDRCxNQUFNLEVBQUU7d0JBQ04sbUJBQW1CLEVBQUU7NEJBQ25CLEtBQUssRUFBRSxDQUFDO3lCQUNUO3dCQUNELFlBQVksRUFBRSxNQUFNO3FCQUNyQjtpQkFDRixDQUFDO2dCQUNGLEtBQUssQ0FBQyxPQUFPLEVBQUUsWUFBWSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFFM0QsT0FBTztnQkFDUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBRWhCLE9BQU87Z0JBQ1AsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2FkbWluL21ldHJpY3MvbWV0cmljcy5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRGaXh0dXJlLCBUZXN0QmVkLCBhc3luYyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBvZiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBDb29wY3ljbGVUZXN0TW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vdGVzdC5tb2R1bGUnO1xuaW1wb3J0IHsgTWV0cmljc0NvbXBvbmVudCB9IGZyb20gJ2FwcC9hZG1pbi9tZXRyaWNzL21ldHJpY3MuY29tcG9uZW50JztcbmltcG9ydCB7IE1ldHJpY3NTZXJ2aWNlIH0gZnJvbSAnYXBwL2FkbWluL21ldHJpY3MvbWV0cmljcy5zZXJ2aWNlJztcblxuZGVzY3JpYmUoJ0NvbXBvbmVudCBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ01ldHJpY3NDb21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IE1ldHJpY3NDb21wb25lbnQ7XG4gICAgbGV0IGZpeHR1cmU6IENvbXBvbmVudEZpeHR1cmU8TWV0cmljc0NvbXBvbmVudD47XG4gICAgbGV0IHNlcnZpY2U6IE1ldHJpY3NTZXJ2aWNlO1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW01ldHJpY3NDb21wb25lbnRdXG4gICAgICB9KVxuICAgICAgICAub3ZlcnJpZGVUZW1wbGF0ZShNZXRyaWNzQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgfSkpO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBmaXh0dXJlID0gVGVzdEJlZC5jcmVhdGVDb21wb25lbnQoTWV0cmljc0NvbXBvbmVudCk7XG4gICAgICBjb21wID0gZml4dHVyZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICAgIHNlcnZpY2UgPSBmaXh0dXJlLmRlYnVnRWxlbWVudC5pbmplY3Rvci5nZXQoTWV0cmljc1NlcnZpY2UpO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ3JlZnJlc2gnLCAoKSA9PiB7XG4gICAgICBpdCgnc2hvdWxkIGNhbGwgcmVmcmVzaCBvbiBpbml0JywgKCkgPT4ge1xuICAgICAgICAvLyBHSVZFTlxuICAgICAgICBjb25zdCByZXNwb25zZSA9IHtcbiAgICAgICAgICB0aW1lcnM6IHtcbiAgICAgICAgICAgIHNlcnZpY2U6ICd0ZXN0JyxcbiAgICAgICAgICAgIHVucmVsYXRlZEtleTogJ3Rlc3QnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBnYXVnZXM6IHtcbiAgICAgICAgICAgICdqY2FjaGUuc3RhdGlzdGljcyc6IHtcbiAgICAgICAgICAgICAgdmFsdWU6IDJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1bnJlbGF0ZWRLZXk6ICd0ZXN0J1xuICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgc3B5T24oc2VydmljZSwgJ2dldE1ldHJpY3MnKS5hbmQucmV0dXJuVmFsdWUob2YocmVzcG9uc2UpKTtcblxuICAgICAgICAvLyBXSEVOXG4gICAgICAgIGNvbXAubmdPbkluaXQoKTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdChzZXJ2aWNlLmdldE1ldHJpY3MpLnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9