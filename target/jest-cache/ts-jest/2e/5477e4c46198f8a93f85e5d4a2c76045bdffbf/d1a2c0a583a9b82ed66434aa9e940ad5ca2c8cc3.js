"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const http_1 = require("@angular/common/http");
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const authority_constants_1 = require("app/shared/constants/authority.constants");
const test_module_1 = require("../../../test.module");
const user_management_update_component_1 = require("app/admin/user-management/user-management-update.component");
const user_service_1 = require("app/core/user/user.service");
const user_model_1 = require("app/core/user/user.model");
describe('Component Tests', () => {
    describe('User Management Update Component', () => {
        let comp;
        let fixture;
        let service;
        const route = {
            data: rxjs_1.of({ user: new user_model_1.User(1, 'user', 'first', 'last', 'first@last.com', true, 'en', [authority_constants_1.Authority.USER], 'admin') })
        };
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [user_management_update_component_1.UserManagementUpdateComponent],
                providers: [
                    forms_1.FormBuilder,
                    {
                        provide: router_1.ActivatedRoute,
                        useValue: route
                    }
                ]
            })
                .overrideTemplate(user_management_update_component_1.UserManagementUpdateComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(user_management_update_component_1.UserManagementUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(user_service_1.UserService);
        });
        describe('OnInit', () => {
            it('Should load authorities and language on init', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                spyOn(service, 'authorities').and.returnValue(rxjs_1.of(['USER']));
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(service.authorities).toHaveBeenCalled();
                expect(comp.authorities).toEqual(['USER']);
            })));
        });
        describe('save', () => {
            it('Should call update service on save for existing user', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                const entity = new user_model_1.User(123);
                spyOn(service, 'update').and.returnValue(rxjs_1.of(new http_1.HttpResponse({
                    body: entity
                })));
                comp.user = entity;
                comp.editForm.patchValue({ id: entity.id });
                // WHEN
                comp.save();
                testing_1.tick(); // simulate async
                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            })));
            it('Should call create service on save for new user', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                const entity = new user_model_1.User();
                spyOn(service, 'create').and.returnValue(rxjs_1.of(new http_1.HttpResponse({ body: entity })));
                comp.user = entity;
                // WHEN
                comp.save();
                testing_1.tick(); // simulate async
                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            })));
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWRtaW4vdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC11cGRhdGUuY29tcG9uZW50LnNwZWMudHMiLCJtYXBwaW5ncyI6Ijs7QUFBQSxtREFBa0c7QUFDbEcsK0NBQW9EO0FBQ3BELDBDQUE2QztBQUM3Qyw0Q0FBaUQ7QUFDakQsK0JBQTBCO0FBRTFCLGtGQUFxRTtBQUNyRSxzREFBMkQ7QUFDM0QsaUhBQTJHO0FBQzNHLDZEQUF5RDtBQUN6RCx5REFBZ0Q7QUFFaEQsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsa0NBQWtDLEVBQUUsR0FBRyxFQUFFO1FBQ2hELElBQUksSUFBbUMsQ0FBQztRQUN4QyxJQUFJLE9BQXdELENBQUM7UUFDN0QsSUFBSSxPQUFvQixDQUFDO1FBQ3pCLE1BQU0sS0FBSyxHQUFvQjtZQUM3QixJQUFJLEVBQUUsU0FBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksaUJBQUksQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUUsQ0FBQztTQUN4RixDQUFDO1FBRTVCLFVBQVUsQ0FBQyxlQUFLLENBQUMsR0FBRyxFQUFFO1lBQ3BCLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUFtQixDQUFDO2dCQUM5QixZQUFZLEVBQUUsQ0FBQyxnRUFBNkIsQ0FBQztnQkFDN0MsU0FBUyxFQUFFO29CQUNULG1CQUFXO29CQUNYO3dCQUNFLE9BQU8sRUFBRSx1QkFBYzt3QkFDdkIsUUFBUSxFQUFFLEtBQUs7cUJBQ2hCO2lCQUNGO2FBQ0YsQ0FBQztpQkFDQyxnQkFBZ0IsQ0FBQyxnRUFBNkIsRUFBRSxFQUFFLENBQUM7aUJBQ25ELGlCQUFpQixFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVKLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxlQUFlLENBQUMsZ0VBQTZCLENBQUMsQ0FBQztZQUNqRSxJQUFJLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO1lBQ2pDLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsMEJBQVcsQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUU7WUFDdEIsRUFBRSxDQUFDLDhDQUE4QyxFQUFFLGdCQUFNLENBQ3ZELEVBQUUsRUFDRixtQkFBUyxDQUFDLEdBQUcsRUFBRTtnQkFDYixRQUFRO2dCQUNSLEtBQUssQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRTVELE9BQU87Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUVoQixPQUFPO2dCQUNQLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUNILENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUU7WUFDcEIsRUFBRSxDQUFDLHNEQUFzRCxFQUFFLGdCQUFNLENBQy9ELEVBQUUsRUFDRixtQkFBUyxDQUFDLEdBQUcsRUFBRTtnQkFDYixRQUFRO2dCQUNSLE1BQU0sTUFBTSxHQUFHLElBQUksaUJBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDN0IsS0FBSyxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUN0QyxTQUFFLENBQ0EsSUFBSSxtQkFBWSxDQUFDO29CQUNmLElBQUksRUFBRSxNQUFNO2lCQUNiLENBQUMsQ0FDSCxDQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUM1QyxPQUFPO2dCQUNQLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDWixjQUFJLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQjtnQkFFekIsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwRCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsaURBQWlELEVBQUUsZ0JBQU0sQ0FDMUQsRUFBRSxFQUNGLG1CQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNiLFFBQVE7Z0JBQ1IsTUFBTSxNQUFNLEdBQUcsSUFBSSxpQkFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUssQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFFLENBQUMsSUFBSSxtQkFBWSxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqRixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztnQkFDbkIsT0FBTztnQkFDUCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ1osY0FBSSxFQUFFLENBQUMsQ0FBQyxpQkFBaUI7Z0JBRXpCLE9BQU87Z0JBQ1AsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQ0gsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2FkbWluL3VzZXItbWFuYWdlbWVudC91c2VyLW1hbmFnZW1lbnQtdXBkYXRlLmNvbXBvbmVudC5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQsIGFzeW5jLCBpbmplY3QsIGZha2VBc3luYywgdGljayB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEF1dGhvcml0eSB9IGZyb20gJ2FwcC9zaGFyZWQvY29uc3RhbnRzL2F1dGhvcml0eS5jb25zdGFudHMnO1xuaW1wb3J0IHsgQ29vcGN5Y2xlVGVzdE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3Rlc3QubW9kdWxlJztcbmltcG9ydCB7IFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50IH0gZnJvbSAnYXBwL2FkbWluL3VzZXItbWFuYWdlbWVudC91c2VyLW1hbmFnZW1lbnQtdXBkYXRlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJ2FwcC9jb3JlL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tICdhcHAvY29yZS91c2VyL3VzZXIubW9kZWwnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnVXNlciBNYW5hZ2VtZW50IFVwZGF0ZSBDb21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50O1xuICAgIGxldCBmaXh0dXJlOiBDb21wb25lbnRGaXh0dXJlPFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50PjtcbiAgICBsZXQgc2VydmljZTogVXNlclNlcnZpY2U7XG4gICAgY29uc3Qgcm91dGU6IEFjdGl2YXRlZFJvdXRlID0gKHtcbiAgICAgIGRhdGE6IG9mKHsgdXNlcjogbmV3IFVzZXIoMSwgJ3VzZXInLCAnZmlyc3QnLCAnbGFzdCcsICdmaXJzdEBsYXN0LmNvbScsIHRydWUsICdlbicsIFtBdXRob3JpdHkuVVNFUl0sICdhZG1pbicpIH0pXG4gICAgfSBhcyBhbnkpIGFzIEFjdGl2YXRlZFJvdXRlO1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW1VzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50XSxcbiAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgRm9ybUJ1aWxkZXIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgcHJvdmlkZTogQWN0aXZhdGVkUm91dGUsXG4gICAgICAgICAgICB1c2VWYWx1ZTogcm91dGVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0pXG4gICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgfSkpO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBmaXh0dXJlID0gVGVzdEJlZC5jcmVhdGVDb21wb25lbnQoVXNlck1hbmFnZW1lbnRVcGRhdGVDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgICBzZXJ2aWNlID0gZml4dHVyZS5kZWJ1Z0VsZW1lbnQuaW5qZWN0b3IuZ2V0KFVzZXJTZXJ2aWNlKTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdPbkluaXQnLCAoKSA9PiB7XG4gICAgICBpdCgnU2hvdWxkIGxvYWQgYXV0aG9yaXRpZXMgYW5kIGxhbmd1YWdlIG9uIGluaXQnLCBpbmplY3QoXG4gICAgICAgIFtdLFxuICAgICAgICBmYWtlQXN5bmMoKCkgPT4ge1xuICAgICAgICAgIC8vIEdJVkVOXG4gICAgICAgICAgc3B5T24oc2VydmljZSwgJ2F1dGhvcml0aWVzJykuYW5kLnJldHVyblZhbHVlKG9mKFsnVVNFUiddKSk7XG5cbiAgICAgICAgICAvLyBXSEVOXG4gICAgICAgICAgY29tcC5uZ09uSW5pdCgpO1xuXG4gICAgICAgICAgLy8gVEhFTlxuICAgICAgICAgIGV4cGVjdChzZXJ2aWNlLmF1dGhvcml0aWVzKS50b0hhdmVCZWVuQ2FsbGVkKCk7XG4gICAgICAgICAgZXhwZWN0KGNvbXAuYXV0aG9yaXRpZXMpLnRvRXF1YWwoWydVU0VSJ10pO1xuICAgICAgICB9KVxuICAgICAgKSk7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnc2F2ZScsICgpID0+IHtcbiAgICAgIGl0KCdTaG91bGQgY2FsbCB1cGRhdGUgc2VydmljZSBvbiBzYXZlIGZvciBleGlzdGluZyB1c2VyJywgaW5qZWN0KFxuICAgICAgICBbXSxcbiAgICAgICAgZmFrZUFzeW5jKCgpID0+IHtcbiAgICAgICAgICAvLyBHSVZFTlxuICAgICAgICAgIGNvbnN0IGVudGl0eSA9IG5ldyBVc2VyKDEyMyk7XG4gICAgICAgICAgc3B5T24oc2VydmljZSwgJ3VwZGF0ZScpLmFuZC5yZXR1cm5WYWx1ZShcbiAgICAgICAgICAgIG9mKFxuICAgICAgICAgICAgICBuZXcgSHR0cFJlc3BvbnNlKHtcbiAgICAgICAgICAgICAgICBib2R5OiBlbnRpdHlcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgICApO1xuICAgICAgICAgIGNvbXAudXNlciA9IGVudGl0eTtcbiAgICAgICAgICBjb21wLmVkaXRGb3JtLnBhdGNoVmFsdWUoeyBpZDogZW50aXR5LmlkIH0pO1xuICAgICAgICAgIC8vIFdIRU5cbiAgICAgICAgICBjb21wLnNhdmUoKTtcbiAgICAgICAgICB0aWNrKCk7IC8vIHNpbXVsYXRlIGFzeW5jXG5cbiAgICAgICAgICAvLyBUSEVOXG4gICAgICAgICAgZXhwZWN0KHNlcnZpY2UudXBkYXRlKS50b0hhdmVCZWVuQ2FsbGVkV2l0aChlbnRpdHkpO1xuICAgICAgICAgIGV4cGVjdChjb21wLmlzU2F2aW5nKS50b0VxdWFsKGZhbHNlKTtcbiAgICAgICAgfSlcbiAgICAgICkpO1xuXG4gICAgICBpdCgnU2hvdWxkIGNhbGwgY3JlYXRlIHNlcnZpY2Ugb24gc2F2ZSBmb3IgbmV3IHVzZXInLCBpbmplY3QoXG4gICAgICAgIFtdLFxuICAgICAgICBmYWtlQXN5bmMoKCkgPT4ge1xuICAgICAgICAgIC8vIEdJVkVOXG4gICAgICAgICAgY29uc3QgZW50aXR5ID0gbmV3IFVzZXIoKTtcbiAgICAgICAgICBzcHlPbihzZXJ2aWNlLCAnY3JlYXRlJykuYW5kLnJldHVyblZhbHVlKG9mKG5ldyBIdHRwUmVzcG9uc2UoeyBib2R5OiBlbnRpdHkgfSkpKTtcbiAgICAgICAgICBjb21wLnVzZXIgPSBlbnRpdHk7XG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIGNvbXAuc2F2ZSgpO1xuICAgICAgICAgIHRpY2soKTsgLy8gc2ltdWxhdGUgYXN5bmNcblxuICAgICAgICAgIC8vIFRIRU5cbiAgICAgICAgICBleHBlY3Qoc2VydmljZS5jcmVhdGUpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKGVudGl0eSk7XG4gICAgICAgICAgZXhwZWN0KGNvbXAuaXNTYXZpbmcpLnRvRXF1YWwoZmFsc2UpO1xuICAgICAgICB9KVxuICAgICAgKSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXSwidmVyc2lvbiI6M30=