"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const ng_jhipster_1 = require("ng-jhipster");
const account_service_1 = require("app/core/auth/account.service");
const language_constants_1 = require("app/core/language/language.constants");
let SettingsComponent = class SettingsComponent {
    constructor(accountService, fb, languageService) {
        this.accountService = accountService;
        this.fb = fb;
        this.languageService = languageService;
        this.success = false;
        this.languages = language_constants_1.LANGUAGES;
        this.settingsForm = this.fb.group({
            firstName: [undefined, [forms_1.Validators.required, forms_1.Validators.minLength(1), forms_1.Validators.maxLength(50)]],
            lastName: [undefined, [forms_1.Validators.required, forms_1.Validators.minLength(1), forms_1.Validators.maxLength(50)]],
            email: [undefined, [forms_1.Validators.required, forms_1.Validators.minLength(5), forms_1.Validators.maxLength(254), forms_1.Validators.email]],
            langKey: [undefined]
        });
    }
    ngOnInit() {
        this.accountService.identity().subscribe(account => {
            if (account) {
                this.settingsForm.patchValue({
                    firstName: account.firstName,
                    lastName: account.lastName,
                    email: account.email,
                    langKey: account.langKey
                });
                this.account = account;
            }
        });
    }
    save() {
        this.success = false;
        this.account.firstName = this.settingsForm.get('firstName').value;
        this.account.lastName = this.settingsForm.get('lastName').value;
        this.account.email = this.settingsForm.get('email').value;
        this.account.langKey = this.settingsForm.get('langKey').value;
        this.accountService.save(this.account).subscribe(() => {
            this.success = true;
            this.accountService.authenticate(this.account);
            if (this.account.langKey !== this.languageService.getCurrentLanguage()) {
                this.languageService.changeLanguage(this.account.langKey);
            }
        });
    }
};
SettingsComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-settings',
        template: require('./settings.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [account_service_1.AccountService, forms_1.FormBuilder, ng_jhipster_1.JhiLanguageService])
], SettingsComponent);
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvYWNjb3VudC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQudHMiLCJtYXBwaW5ncyI6Ijs7O0FBQUEsd0NBQWtEO0FBQ2xELDBDQUF5RDtBQUN6RCw2Q0FBaUQ7QUFFakQsbUVBQStEO0FBRS9ELDZFQUFpRTtBQU1qRSxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQVc1QixZQUFvQixjQUE4QixFQUFVLEVBQWUsRUFBVSxlQUFtQztRQUFwRyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQVUsb0JBQWUsR0FBZixlQUFlLENBQW9CO1FBVHhILFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsY0FBUyxHQUFHLDhCQUFTLENBQUM7UUFDdEIsaUJBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUMzQixTQUFTLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2hHLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0YsS0FBSyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsa0JBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvRyxPQUFPLEVBQUUsQ0FBQyxTQUFTLENBQUM7U0FDckIsQ0FBQyxDQUFDO0lBRXdILENBQUM7SUFFNUgsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ2pELElBQUksT0FBTyxFQUFFO2dCQUNYLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO29CQUMzQixTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVM7b0JBQzVCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUTtvQkFDMUIsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLO29CQUNwQixPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU87aUJBQ3pCLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzthQUN4QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUVyQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUUsQ0FBQyxLQUFLLENBQUM7UUFDbkUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFFLENBQUMsS0FBSyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBRSxDQUFDLEtBQUssQ0FBQztRQUMzRCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUUsQ0FBQyxLQUFLLENBQUM7UUFFL0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDcEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFFcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRS9DLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRSxFQUFFO2dCQUN0RSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzNEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YsQ0FBQTtBQTlDWSxpQkFBaUI7SUFKN0IsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxjQUFjO1FBQ3hCLGtCQUFhLDJCQUEyQixDQUFBO0tBQ3pDLENBQUM7NkNBWW9DLGdDQUFjLEVBQWMsbUJBQVcsRUFBMkIsZ0NBQWtCO0dBWDdHLGlCQUFpQixDQThDN0I7QUE5Q1ksOENBQWlCIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FjY291bnQvc2V0dGluZ3Mvc2V0dGluZ3MuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEpoaUxhbmd1YWdlU2VydmljZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcblxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS9hdXRoL2FjY291bnQuc2VydmljZSc7XG5pbXBvcnQgeyBBY2NvdW50IH0gZnJvbSAnYXBwL2NvcmUvdXNlci9hY2NvdW50Lm1vZGVsJztcbmltcG9ydCB7IExBTkdVQUdFUyB9IGZyb20gJ2FwcC9jb3JlL2xhbmd1YWdlL2xhbmd1YWdlLmNvbnN0YW50cyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2poaS1zZXR0aW5ncycsXG4gIHRlbXBsYXRlVXJsOiAnLi9zZXR0aW5ncy5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgU2V0dGluZ3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBhY2NvdW50ITogQWNjb3VudDtcbiAgc3VjY2VzcyA9IGZhbHNlO1xuICBsYW5ndWFnZXMgPSBMQU5HVUFHRVM7XG4gIHNldHRpbmdzRm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgIGZpcnN0TmFtZTogW3VuZGVmaW5lZCwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDEpLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldXSxcbiAgICBsYXN0TmFtZTogW3VuZGVmaW5lZCwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDEpLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldXSxcbiAgICBlbWFpbDogW3VuZGVmaW5lZCwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDUpLCBWYWxpZGF0b3JzLm1heExlbmd0aCgyNTQpLCBWYWxpZGF0b3JzLmVtYWlsXV0sXG4gICAgbGFuZ0tleTogW3VuZGVmaW5lZF1cbiAgfSk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIGxhbmd1YWdlU2VydmljZTogSmhpTGFuZ3VhZ2VTZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYWNjb3VudFNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoYWNjb3VudCA9PiB7XG4gICAgICBpZiAoYWNjb3VudCkge1xuICAgICAgICB0aGlzLnNldHRpbmdzRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICBmaXJzdE5hbWU6IGFjY291bnQuZmlyc3ROYW1lLFxuICAgICAgICAgIGxhc3ROYW1lOiBhY2NvdW50Lmxhc3ROYW1lLFxuICAgICAgICAgIGVtYWlsOiBhY2NvdW50LmVtYWlsLFxuICAgICAgICAgIGxhbmdLZXk6IGFjY291bnQubGFuZ0tleVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmFjY291bnQgPSBhY2NvdW50O1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgc2F2ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnN1Y2Nlc3MgPSBmYWxzZTtcblxuICAgIHRoaXMuYWNjb3VudC5maXJzdE5hbWUgPSB0aGlzLnNldHRpbmdzRm9ybS5nZXQoJ2ZpcnN0TmFtZScpIS52YWx1ZTtcbiAgICB0aGlzLmFjY291bnQubGFzdE5hbWUgPSB0aGlzLnNldHRpbmdzRm9ybS5nZXQoJ2xhc3ROYW1lJykhLnZhbHVlO1xuICAgIHRoaXMuYWNjb3VudC5lbWFpbCA9IHRoaXMuc2V0dGluZ3NGb3JtLmdldCgnZW1haWwnKSEudmFsdWU7XG4gICAgdGhpcy5hY2NvdW50LmxhbmdLZXkgPSB0aGlzLnNldHRpbmdzRm9ybS5nZXQoJ2xhbmdLZXknKSEudmFsdWU7XG5cbiAgICB0aGlzLmFjY291bnRTZXJ2aWNlLnNhdmUodGhpcy5hY2NvdW50KS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdGhpcy5zdWNjZXNzID0gdHJ1ZTtcblxuICAgICAgdGhpcy5hY2NvdW50U2VydmljZS5hdXRoZW50aWNhdGUodGhpcy5hY2NvdW50KTtcblxuICAgICAgaWYgKHRoaXMuYWNjb3VudC5sYW5nS2V5ICE9PSB0aGlzLmxhbmd1YWdlU2VydmljZS5nZXRDdXJyZW50TGFuZ3VhZ2UoKSkge1xuICAgICAgICB0aGlzLmxhbmd1YWdlU2VydmljZS5jaGFuZ2VMYW5ndWFnZSh0aGlzLmFjY291bnQubGFuZ0tleSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbn1cbiJdLCJ2ZXJzaW9uIjozfQ==