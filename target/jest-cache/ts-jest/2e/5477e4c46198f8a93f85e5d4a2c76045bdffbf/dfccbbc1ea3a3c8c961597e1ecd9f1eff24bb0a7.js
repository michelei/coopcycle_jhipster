"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const router_1 = require("@angular/router");
const login_service_1 = require("app/core/login/login.service");
let LoginModalComponent = class LoginModalComponent {
    constructor(loginService, router, activeModal, fb) {
        this.loginService = loginService;
        this.router = router;
        this.activeModal = activeModal;
        this.fb = fb;
        this.authenticationError = false;
        this.loginForm = this.fb.group({
            username: [''],
            password: [''],
            rememberMe: [false]
        });
    }
    ngAfterViewInit() {
        if (this.username) {
            this.username.nativeElement.focus();
        }
    }
    cancel() {
        this.authenticationError = false;
        this.loginForm.patchValue({
            username: '',
            password: ''
        });
        this.activeModal.dismiss('cancel');
    }
    login() {
        this.loginService
            .login({
            username: this.loginForm.get('username').value,
            password: this.loginForm.get('password').value,
            rememberMe: this.loginForm.get('rememberMe').value
        })
            .subscribe(() => {
            this.authenticationError = false;
            this.activeModal.close();
            if (this.router.url === '/account/register' ||
                this.router.url.startsWith('/account/activate') ||
                this.router.url.startsWith('/account/reset/')) {
                this.router.navigate(['']);
            }
        }, () => (this.authenticationError = true));
    }
    register() {
        this.activeModal.dismiss('to state register');
        this.router.navigate(['/account/register']);
    }
    requestResetPassword() {
        this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/account/reset', 'request']);
    }
};
tslib_1.__decorate([
    core_1.ViewChild('username', { static: false }),
    tslib_1.__metadata("design:type", core_1.ElementRef)
], LoginModalComponent.prototype, "username", void 0);
LoginModalComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'jhi-login-modal',
        template: require('./login.component.html')
    }),
    tslib_1.__metadata("design:paramtypes", [login_service_1.LoginService, router_1.Router, ng_bootstrap_1.NgbActiveModal, forms_1.FormBuilder])
], LoginModalComponent);
exports.LoginModalComponent = LoginModalComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy9tYWluL3dlYmFwcC9hcHAvc2hhcmVkL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyIsIm1hcHBpbmdzIjoiOzs7QUFBQSx3Q0FBZ0Y7QUFDaEYsMENBQTZDO0FBQzdDLDZEQUE0RDtBQUM1RCw0Q0FBeUM7QUFFekMsZ0VBQTREO0FBTTVELElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBWTlCLFlBQW9CLFlBQTBCLEVBQVUsTUFBYyxFQUFTLFdBQTJCLEVBQVUsRUFBZTtRQUEvRyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFBVSxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBUm5JLHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUU1QixjQUFTLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDeEIsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2QsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2QsVUFBVSxFQUFFLENBQUMsS0FBSyxDQUFDO1NBQ3BCLENBQUMsQ0FBQztJQUVtSSxDQUFDO0lBRXZJLGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDckM7SUFDSCxDQUFDO0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7WUFDeEIsUUFBUSxFQUFFLEVBQUU7WUFDWixRQUFRLEVBQUUsRUFBRTtTQUNiLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLFlBQVk7YUFDZCxLQUFLLENBQUM7WUFDTCxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFFLENBQUMsS0FBSztZQUMvQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFFLENBQUMsS0FBSztZQUMvQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFFLENBQUMsS0FBSztTQUNwRCxDQUFDO2FBQ0QsU0FBUyxDQUNSLEdBQUcsRUFBRTtZQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN6QixJQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLG1CQUFtQjtnQkFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDO2dCQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsRUFDN0M7Z0JBQ0EsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzVCO1FBQ0gsQ0FBQyxFQUNELEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxDQUN4QyxDQUFDO0lBQ04sQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdEQsQ0FBQztDQUNGLENBQUE7QUEzREM7SUFEQyxnQkFBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQztzQ0FDOUIsaUJBQVU7cURBQUM7QUFGWCxtQkFBbUI7SUFKL0IsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxpQkFBaUI7UUFDM0Isa0JBQWEsd0JBQXdCLENBQUE7S0FDdEMsQ0FBQzs2Q0Fha0MsNEJBQVksRUFBa0IsZUFBTSxFQUFzQiw2QkFBYyxFQUFjLG1CQUFXO0dBWnhILG1CQUFtQixDQTZEL0I7QUE3RFksa0RBQW1CIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvbWFpbi93ZWJhcHAvYXBwL3NoYXJlZC9sb2dpbi9sb2dpbi5jb21wb25lbnQudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBBZnRlclZpZXdJbml0LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTmdiQWN0aXZlTW9kYWwgfSBmcm9tICdAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcCc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS9sb2dpbi9sb2dpbi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnamhpLWxvZ2luLW1vZGFsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBMb2dpbk1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoJ3VzZXJuYW1lJywgeyBzdGF0aWM6IGZhbHNlIH0pXG4gIHVzZXJuYW1lPzogRWxlbWVudFJlZjtcblxuICBhdXRoZW50aWNhdGlvbkVycm9yID0gZmFsc2U7XG5cbiAgbG9naW5Gb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgdXNlcm5hbWU6IFsnJ10sXG4gICAgcGFzc3dvcmQ6IFsnJ10sXG4gICAgcmVtZW1iZXJNZTogW2ZhbHNlXVxuICB9KTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwdWJsaWMgYWN0aXZlTW9kYWw6IE5nYkFjdGl2ZU1vZGFsLCBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcikge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMudXNlcm5hbWUpIHtcbiAgICAgIHRoaXMudXNlcm5hbWUubmF0aXZlRWxlbWVudC5mb2N1cygpO1xuICAgIH1cbiAgfVxuXG4gIGNhbmNlbCgpOiB2b2lkIHtcbiAgICB0aGlzLmF1dGhlbnRpY2F0aW9uRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmxvZ2luRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgIHBhc3N3b3JkOiAnJ1xuICAgIH0pO1xuICAgIHRoaXMuYWN0aXZlTW9kYWwuZGlzbWlzcygnY2FuY2VsJyk7XG4gIH1cblxuICBsb2dpbigpOiB2b2lkIHtcbiAgICB0aGlzLmxvZ2luU2VydmljZVxuICAgICAgLmxvZ2luKHtcbiAgICAgICAgdXNlcm5hbWU6IHRoaXMubG9naW5Gb3JtLmdldCgndXNlcm5hbWUnKSEudmFsdWUsXG4gICAgICAgIHBhc3N3b3JkOiB0aGlzLmxvZ2luRm9ybS5nZXQoJ3Bhc3N3b3JkJykhLnZhbHVlLFxuICAgICAgICByZW1lbWJlck1lOiB0aGlzLmxvZ2luRm9ybS5nZXQoJ3JlbWVtYmVyTWUnKSEudmFsdWVcbiAgICAgIH0pXG4gICAgICAuc3Vic2NyaWJlKFxuICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvbkVycm9yID0gZmFsc2U7XG4gICAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5jbG9zZSgpO1xuICAgICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMucm91dGVyLnVybCA9PT0gJy9hY2NvdW50L3JlZ2lzdGVyJyB8fFxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIudXJsLnN0YXJ0c1dpdGgoJy9hY2NvdW50L2FjdGl2YXRlJykgfHxcbiAgICAgICAgICAgIHRoaXMucm91dGVyLnVybC5zdGFydHNXaXRoKCcvYWNjb3VudC9yZXNldC8nKVxuICAgICAgICAgICkge1xuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycnXSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICAoKSA9PiAodGhpcy5hdXRoZW50aWNhdGlvbkVycm9yID0gdHJ1ZSlcbiAgICAgICk7XG4gIH1cblxuICByZWdpc3RlcigpOiB2b2lkIHtcbiAgICB0aGlzLmFjdGl2ZU1vZGFsLmRpc21pc3MoJ3RvIHN0YXRlIHJlZ2lzdGVyJyk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvYWNjb3VudC9yZWdpc3RlciddKTtcbiAgfVxuXG4gIHJlcXVlc3RSZXNldFBhc3N3b3JkKCk6IHZvaWQge1xuICAgIHRoaXMuYWN0aXZlTW9kYWwuZGlzbWlzcygndG8gc3RhdGUgcmVxdWVzdFJlc2V0Jyk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvYWNjb3VudC9yZXNldCcsICdyZXF1ZXN0J10pO1xuICB9XG59XG4iXSwidmVyc2lvbiI6M30=