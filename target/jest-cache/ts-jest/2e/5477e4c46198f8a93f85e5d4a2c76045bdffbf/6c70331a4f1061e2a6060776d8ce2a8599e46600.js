"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const testing_1 = require("@angular/core/testing");
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const test_module_1 = require("../../../../test.module");
const password_reset_finish_component_1 = require("app/account/password-reset/finish/password-reset-finish.component");
const password_reset_finish_service_1 = require("app/account/password-reset/finish/password-reset-finish.service");
const mock_route_service_1 = require("../../../../helpers/mock-route.service");
describe('Component Tests', () => {
    describe('PasswordResetFinishComponent', () => {
        let fixture;
        let comp;
        beforeEach(() => {
            fixture = testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [password_reset_finish_component_1.PasswordResetFinishComponent],
                providers: [
                    forms_1.FormBuilder,
                    {
                        provide: router_1.ActivatedRoute,
                        useValue: new mock_route_service_1.MockActivatedRoute({ key: 'XYZPDQ' })
                    }
                ]
            })
                .overrideTemplate(password_reset_finish_component_1.PasswordResetFinishComponent, '')
                .createComponent(password_reset_finish_component_1.PasswordResetFinishComponent);
        });
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(password_reset_finish_component_1.PasswordResetFinishComponent);
            comp = fixture.componentInstance;
            comp.ngOnInit();
        });
        it('should define its initial state', () => {
            expect(comp.initialized).toBe(true);
            expect(comp.key).toEqual('XYZPDQ');
        });
        it('sets focus after the view has been initialized', () => {
            const node = {
                focus() { }
            };
            comp.newPassword = new core_1.ElementRef(node);
            spyOn(node, 'focus');
            comp.ngAfterViewInit();
            expect(node.focus).toHaveBeenCalled();
        });
        it('should ensure the two passwords entered match', () => {
            comp.passwordForm.patchValue({
                newPassword: 'password',
                confirmPassword: 'non-matching'
            });
            comp.finishReset();
            expect(comp.doNotMatch).toBe(true);
        });
        it('should update success to true after resetting password', testing_1.inject([password_reset_finish_service_1.PasswordResetFinishService], testing_1.fakeAsync((service) => {
            spyOn(service, 'save').and.returnValue(rxjs_1.of({}));
            comp.passwordForm.patchValue({
                newPassword: 'password',
                confirmPassword: 'password'
            });
            comp.finishReset();
            testing_1.tick();
            expect(service.save).toHaveBeenCalledWith('XYZPDQ', 'password');
            expect(comp.success).toBe(true);
        })));
        it('should notify of generic error', testing_1.inject([password_reset_finish_service_1.PasswordResetFinishService], testing_1.fakeAsync((service) => {
            spyOn(service, 'save').and.returnValue(rxjs_1.throwError('ERROR'));
            comp.passwordForm.patchValue({
                newPassword: 'password',
                confirmPassword: 'password'
            });
            comp.finishReset();
            testing_1.tick();
            expect(service.save).toHaveBeenCalledWith('XYZPDQ', 'password');
            expect(comp.success).toBe(false);
            expect(comp.error).toBe(true);
        })));
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9maW5pc2gvcGFzc3dvcmQtcmVzZXQtZmluaXNoLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsd0NBQTJDO0FBQzNDLG1EQUEyRjtBQUMzRiwwQ0FBNkM7QUFDN0MsNENBQWlEO0FBQ2pELCtCQUFzQztBQUV0Qyx5REFBOEQ7QUFDOUQsdUhBQWlIO0FBQ2pILG1IQUE2RztBQUM3RywrRUFBNEU7QUFFNUUsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsOEJBQThCLEVBQUUsR0FBRyxFQUFFO1FBQzVDLElBQUksT0FBdUQsQ0FBQztRQUM1RCxJQUFJLElBQWtDLENBQUM7UUFFdkMsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxpQkFBTyxDQUFDLHNCQUFzQixDQUFDO2dCQUN2QyxPQUFPLEVBQUUsQ0FBQyxpQ0FBbUIsQ0FBQztnQkFDOUIsWUFBWSxFQUFFLENBQUMsOERBQTRCLENBQUM7Z0JBQzVDLFNBQVMsRUFBRTtvQkFDVCxtQkFBVztvQkFDWDt3QkFDRSxPQUFPLEVBQUUsdUJBQWM7d0JBQ3ZCLFFBQVEsRUFBRSxJQUFJLHVDQUFrQixDQUFDLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxDQUFDO3FCQUNwRDtpQkFDRjthQUNGLENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMsOERBQTRCLEVBQUUsRUFBRSxDQUFDO2lCQUNsRCxlQUFlLENBQUMsOERBQTRCLENBQUMsQ0FBQztRQUNuRCxDQUFDLENBQUMsQ0FBQztRQUVILFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxlQUFlLENBQUMsOERBQTRCLENBQUMsQ0FBQztZQUNoRSxJQUFJLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxHQUFHLEVBQUU7WUFDekMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsZ0RBQWdELEVBQUUsR0FBRyxFQUFFO1lBQ3hELE1BQU0sSUFBSSxHQUFHO2dCQUNYLEtBQUssS0FBVSxDQUFDO2FBQ2pCLENBQUM7WUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksaUJBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4QyxLQUFLLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRXJCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUV2QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsK0NBQStDLEVBQUUsR0FBRyxFQUFFO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixXQUFXLEVBQUUsVUFBVTtnQkFDdkIsZUFBZSxFQUFFLGNBQWM7YUFDaEMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRW5CLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHdEQUF3RCxFQUFFLGdCQUFNLENBQ2pFLENBQUMsMERBQTBCLENBQUMsRUFDNUIsbUJBQVMsQ0FBQyxDQUFDLE9BQW1DLEVBQUUsRUFBRTtZQUNoRCxLQUFLLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsU0FBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7Z0JBQzNCLFdBQVcsRUFBRSxVQUFVO2dCQUN2QixlQUFlLEVBQUUsVUFBVTthQUM1QixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsY0FBSSxFQUFFLENBQUM7WUFFUCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNoRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsZ0NBQWdDLEVBQUUsZ0JBQU0sQ0FDekMsQ0FBQywwREFBMEIsQ0FBQyxFQUM1QixtQkFBUyxDQUFDLENBQUMsT0FBbUMsRUFBRSxFQUFFO1lBQ2hELEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxpQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7Z0JBQzNCLFdBQVcsRUFBRSxVQUFVO2dCQUN2QixlQUFlLEVBQUUsVUFBVTthQUM1QixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsY0FBSSxFQUFFLENBQUM7WUFFUCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNoRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9ob21lL2xpbG9vL0RvY3VtZW50cy9JTkZPNC9TOC9HTC9Fc3NhaTIvdGVzdGJlZC9zcmMvdGVzdC9qYXZhc2NyaXB0L3NwZWMvYXBwL2FjY291bnQvcGFzc3dvcmQtcmVzZXQvZmluaXNoL3Bhc3N3b3JkLXJlc2V0LWZpbmlzaC5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21wb25lbnRGaXh0dXJlLCBUZXN0QmVkLCBpbmplY3QsIHRpY2ssIGZha2VBc3luYyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IENvb3BjeWNsZVRlc3RNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi8uLi90ZXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBQYXNzd29yZFJlc2V0RmluaXNoQ29tcG9uZW50IH0gZnJvbSAnYXBwL2FjY291bnQvcGFzc3dvcmQtcmVzZXQvZmluaXNoL3Bhc3N3b3JkLXJlc2V0LWZpbmlzaC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGFzc3dvcmRSZXNldEZpbmlzaFNlcnZpY2UgfSBmcm9tICdhcHAvYWNjb3VudC9wYXNzd29yZC1yZXNldC9maW5pc2gvcGFzc3dvcmQtcmVzZXQtZmluaXNoLnNlcnZpY2UnO1xuaW1wb3J0IHsgTW9ja0FjdGl2YXRlZFJvdXRlIH0gZnJvbSAnLi4vLi4vLi4vLi4vaGVscGVycy9tb2NrLXJvdXRlLnNlcnZpY2UnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnUGFzc3dvcmRSZXNldEZpbmlzaENvbXBvbmVudCcsICgpID0+IHtcbiAgICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxQYXNzd29yZFJlc2V0RmluaXNoQ29tcG9uZW50PjtcbiAgICBsZXQgY29tcDogUGFzc3dvcmRSZXNldEZpbmlzaENvbXBvbmVudDtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgIGltcG9ydHM6IFtDb29wY3ljbGVUZXN0TW9kdWxlXSxcbiAgICAgICAgZGVjbGFyYXRpb25zOiBbUGFzc3dvcmRSZXNldEZpbmlzaENvbXBvbmVudF0sXG4gICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgIEZvcm1CdWlsZGVyLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHByb3ZpZGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgICAgICAgICAgdXNlVmFsdWU6IG5ldyBNb2NrQWN0aXZhdGVkUm91dGUoeyBrZXk6ICdYWVpQRFEnIH0pXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9KVxuICAgICAgICAub3ZlcnJpZGVUZW1wbGF0ZShQYXNzd29yZFJlc2V0RmluaXNoQ29tcG9uZW50LCAnJylcbiAgICAgICAgLmNyZWF0ZUNvbXBvbmVudChQYXNzd29yZFJlc2V0RmluaXNoQ29tcG9uZW50KTtcbiAgICB9KTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KFBhc3N3b3JkUmVzZXRGaW5pc2hDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgICBjb21wLm5nT25Jbml0KCk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIGRlZmluZSBpdHMgaW5pdGlhbCBzdGF0ZScsICgpID0+IHtcbiAgICAgIGV4cGVjdChjb21wLmluaXRpYWxpemVkKS50b0JlKHRydWUpO1xuICAgICAgZXhwZWN0KGNvbXAua2V5KS50b0VxdWFsKCdYWVpQRFEnKTtcbiAgICB9KTtcblxuICAgIGl0KCdzZXRzIGZvY3VzIGFmdGVyIHRoZSB2aWV3IGhhcyBiZWVuIGluaXRpYWxpemVkJywgKCkgPT4ge1xuICAgICAgY29uc3Qgbm9kZSA9IHtcbiAgICAgICAgZm9jdXMoKTogdm9pZCB7fVxuICAgICAgfTtcbiAgICAgIGNvbXAubmV3UGFzc3dvcmQgPSBuZXcgRWxlbWVudFJlZihub2RlKTtcbiAgICAgIHNweU9uKG5vZGUsICdmb2N1cycpO1xuXG4gICAgICBjb21wLm5nQWZ0ZXJWaWV3SW5pdCgpO1xuXG4gICAgICBleHBlY3Qobm9kZS5mb2N1cykudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBlbnN1cmUgdGhlIHR3byBwYXNzd29yZHMgZW50ZXJlZCBtYXRjaCcsICgpID0+IHtcbiAgICAgIGNvbXAucGFzc3dvcmRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgICBuZXdQYXNzd29yZDogJ3Bhc3N3b3JkJyxcbiAgICAgICAgY29uZmlybVBhc3N3b3JkOiAnbm9uLW1hdGNoaW5nJ1xuICAgICAgfSk7XG5cbiAgICAgIGNvbXAuZmluaXNoUmVzZXQoKTtcblxuICAgICAgZXhwZWN0KGNvbXAuZG9Ob3RNYXRjaCkudG9CZSh0cnVlKTtcbiAgICB9KTtcblxuICAgIGl0KCdzaG91bGQgdXBkYXRlIHN1Y2Nlc3MgdG8gdHJ1ZSBhZnRlciByZXNldHRpbmcgcGFzc3dvcmQnLCBpbmplY3QoXG4gICAgICBbUGFzc3dvcmRSZXNldEZpbmlzaFNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBQYXNzd29yZFJlc2V0RmluaXNoU2VydmljZSkgPT4ge1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnc2F2ZScpLmFuZC5yZXR1cm5WYWx1ZShvZih7fSkpO1xuICAgICAgICBjb21wLnBhc3N3b3JkRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICBuZXdQYXNzd29yZDogJ3Bhc3N3b3JkJyxcbiAgICAgICAgICBjb25maXJtUGFzc3dvcmQ6ICdwYXNzd29yZCdcbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29tcC5maW5pc2hSZXNldCgpO1xuICAgICAgICB0aWNrKCk7XG5cbiAgICAgICAgZXhwZWN0KHNlcnZpY2Uuc2F2ZSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoJ1hZWlBEUScsICdwYXNzd29yZCcpO1xuICAgICAgICBleHBlY3QoY29tcC5zdWNjZXNzKS50b0JlKHRydWUpO1xuICAgICAgfSlcbiAgICApKTtcblxuICAgIGl0KCdzaG91bGQgbm90aWZ5IG9mIGdlbmVyaWMgZXJyb3InLCBpbmplY3QoXG4gICAgICBbUGFzc3dvcmRSZXNldEZpbmlzaFNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBQYXNzd29yZFJlc2V0RmluaXNoU2VydmljZSkgPT4ge1xuICAgICAgICBzcHlPbihzZXJ2aWNlLCAnc2F2ZScpLmFuZC5yZXR1cm5WYWx1ZSh0aHJvd0Vycm9yKCdFUlJPUicpKTtcbiAgICAgICAgY29tcC5wYXNzd29yZEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgbmV3UGFzc3dvcmQ6ICdwYXNzd29yZCcsXG4gICAgICAgICAgY29uZmlybVBhc3N3b3JkOiAncGFzc3dvcmQnXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbXAuZmluaXNoUmVzZXQoKTtcbiAgICAgICAgdGljaygpO1xuXG4gICAgICAgIGV4cGVjdChzZXJ2aWNlLnNhdmUpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKCdYWVpQRFEnLCAncGFzc3dvcmQnKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuc3VjY2VzcykudG9CZShmYWxzZSk7XG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKHRydWUpO1xuICAgICAgfSlcbiAgICApKTtcbiAgfSk7XG59KTtcbiJdLCJ2ZXJzaW9uIjozfQ==