"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const login_service_1 = require("app/core/login/login.service");
const login_component_1 = require("app/shared/login/login.component");
const test_module_1 = require("../../../test.module");
const mock_login_service_1 = require("../../../helpers/mock-login.service");
describe('Component Tests', () => {
    describe('LoginComponent', () => {
        let comp;
        let fixture;
        let mockLoginService;
        let mockRouter;
        let mockActiveModal;
        beforeEach(testing_1.async(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [test_module_1.CoopcycleTestModule],
                declarations: [login_component_1.LoginModalComponent],
                providers: [
                    forms_1.FormBuilder,
                    {
                        provide: login_service_1.LoginService,
                        useClass: mock_login_service_1.MockLoginService
                    }
                ]
            })
                .overrideTemplate(login_component_1.LoginModalComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(login_component_1.LoginModalComponent);
            comp = fixture.componentInstance;
            mockLoginService = testing_1.TestBed.get(login_service_1.LoginService);
            mockRouter = testing_1.TestBed.get(router_1.Router);
            mockActiveModal = testing_1.TestBed.get(ng_bootstrap_1.NgbActiveModal);
        });
        it('should authenticate the user', testing_1.inject([], testing_1.fakeAsync(() => {
            // GIVEN
            const credentials = {
                username: 'admin',
                password: 'admin',
                rememberMe: true
            };
            comp.loginForm.patchValue({
                username: 'admin',
                password: 'admin',
                rememberMe: true
            });
            mockLoginService.setResponse({});
            mockRouter.url = '/admin/metrics';
            // WHEN/
            comp.login();
            testing_1.tick(); // simulate async
            // THEN
            expect(comp.authenticationError).toEqual(false);
            expect(mockActiveModal.closeSpy).toHaveBeenCalled();
            expect(mockLoginService.loginSpy).toHaveBeenCalledWith(credentials);
        })));
        it('should empty the credentials upon cancel', () => {
            // GIVEN
            comp.loginForm.patchValue({
                username: 'admin',
                password: 'admin'
            });
            const expected = {
                username: '',
                password: '',
                rememberMe: false
            };
            // WHEN
            comp.cancel();
            // THEN
            expect(comp.authenticationError).toEqual(false);
            expect(comp.loginForm.get('username').value).toEqual(expected.username);
            expect(comp.loginForm.get('password').value).toEqual(expected.password);
            expect(comp.loginForm.get('rememberMe').value).toEqual(expected.rememberMe);
            expect(mockActiveModal.dismissSpy).toHaveBeenCalledWith('cancel');
        });
        it('should redirect user when register', () => {
            // WHEN
            comp.register();
            // THEN
            expect(mockActiveModal.dismissSpy).toHaveBeenCalledWith('to state register');
            expect(mockRouter.navigateSpy).toHaveBeenCalledWith(['/account/register']);
        });
        it('should redirect user when request password', () => {
            // WHEN
            comp.requestResetPassword();
            // THEN
            expect(mockActiveModal.dismissSpy).toHaveBeenCalledWith('to state requestReset');
            expect(mockRouter.navigateSpy).toHaveBeenCalledWith(['/account/reset', 'request']);
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL2hvbWUvbGlsb28vRG9jdW1lbnRzL0lORk80L1M4L0dML0Vzc2FpMi90ZXN0YmVkL3NyYy90ZXN0L2phdmFzY3JpcHQvc3BlYy9hcHAvc2hhcmVkL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsbURBQWtHO0FBQ2xHLDBDQUE2QztBQUM3Qyw0Q0FBeUM7QUFDekMsNkRBQTREO0FBRTVELGdFQUE0RDtBQUM1RCxzRUFBdUU7QUFDdkUsc0RBQTJEO0FBQzNELDRFQUF1RTtBQUl2RSxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxHQUFHLEVBQUU7UUFDOUIsSUFBSSxJQUF5QixDQUFDO1FBQzlCLElBQUksT0FBOEMsQ0FBQztRQUNuRCxJQUFJLGdCQUFrQyxDQUFDO1FBQ3ZDLElBQUksVUFBc0IsQ0FBQztRQUMzQixJQUFJLGVBQWdDLENBQUM7UUFFckMsVUFBVSxDQUFDLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDcEIsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQW1CLENBQUM7Z0JBQzlCLFlBQVksRUFBRSxDQUFDLHFDQUFtQixDQUFDO2dCQUNuQyxTQUFTLEVBQUU7b0JBQ1QsbUJBQVc7b0JBQ1g7d0JBQ0UsT0FBTyxFQUFFLDRCQUFZO3dCQUNyQixRQUFRLEVBQUUscUNBQWdCO3FCQUMzQjtpQkFDRjthQUNGLENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMscUNBQW1CLEVBQUUsRUFBRSxDQUFDO2lCQUN6QyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLHFDQUFtQixDQUFDLENBQUM7WUFDdkQsSUFBSSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztZQUNqQyxnQkFBZ0IsR0FBRyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBWSxDQUFDLENBQUM7WUFDN0MsVUFBVSxHQUFHLGlCQUFPLENBQUMsR0FBRyxDQUFDLGVBQU0sQ0FBQyxDQUFDO1lBQ2pDLGVBQWUsR0FBRyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBYyxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsOEJBQThCLEVBQUUsZ0JBQU0sQ0FDdkMsRUFBRSxFQUNGLG1CQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2IsUUFBUTtZQUNSLE1BQU0sV0FBVyxHQUFHO2dCQUNsQixRQUFRLEVBQUUsT0FBTztnQkFDakIsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLFVBQVUsRUFBRSxJQUFJO2FBQ2pCLENBQUM7WUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztnQkFDeEIsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLFFBQVEsRUFBRSxPQUFPO2dCQUNqQixVQUFVLEVBQUUsSUFBSTthQUNqQixDQUFDLENBQUM7WUFDSCxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakMsVUFBVSxDQUFDLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQztZQUVsQyxRQUFRO1lBQ1IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2IsY0FBSSxFQUFFLENBQUMsQ0FBQyxpQkFBaUI7WUFFekIsT0FBTztZQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ3BELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0RSxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMENBQTBDLEVBQUUsR0FBRyxFQUFFO1lBQ2xELFFBQVE7WUFDUixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztnQkFDeEIsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLFFBQVEsRUFBRSxPQUFPO2FBQ2xCLENBQUMsQ0FBQztZQUVILE1BQU0sUUFBUSxHQUFHO2dCQUNmLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxLQUFLO2FBQ2xCLENBQUM7WUFFRixPQUFPO1lBQ1AsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBRWQsT0FBTztZQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0UsTUFBTSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxvQ0FBb0MsRUFBRSxHQUFHLEVBQUU7WUFDNUMsT0FBTztZQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUVoQixPQUFPO1lBQ1AsTUFBTSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzdFLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7UUFDN0UsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsNENBQTRDLEVBQUUsR0FBRyxFQUFFO1lBQ3BELE9BQU87WUFDUCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUU1QixPQUFPO1lBQ1AsTUFBTSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ2pGLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvaG9tZS9saWxvby9Eb2N1bWVudHMvSU5GTzQvUzgvR0wvRXNzYWkyL3Rlc3RiZWQvc3JjL3Rlc3QvamF2YXNjcmlwdC9zcGVjL2FwcC9zaGFyZWQvbG9naW4vbG9naW4uY29tcG9uZW50LnNwZWMudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50Rml4dHVyZSwgVGVzdEJlZCwgYXN5bmMsIGluamVjdCwgZmFrZUFzeW5jLCB0aWNrIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE5nYkFjdGl2ZU1vZGFsIH0gZnJvbSAnQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAnO1xuXG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tICdhcHAvY29yZS9sb2dpbi9sb2dpbi5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ2luTW9kYWxDb21wb25lbnQgfSBmcm9tICdhcHAvc2hhcmVkL2xvZ2luL2xvZ2luLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb29wY3ljbGVUZXN0TW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vdGVzdC5tb2R1bGUnO1xuaW1wb3J0IHsgTW9ja0xvZ2luU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2hlbHBlcnMvbW9jay1sb2dpbi5zZXJ2aWNlJztcbmltcG9ydCB7IE1vY2tSb3V0ZXIgfSBmcm9tICcuLi8uLi8uLi9oZWxwZXJzL21vY2stcm91dGUuc2VydmljZSc7XG5pbXBvcnQgeyBNb2NrQWN0aXZlTW9kYWwgfSBmcm9tICcuLi8uLi8uLi9oZWxwZXJzL21vY2stYWN0aXZlLW1vZGFsLnNlcnZpY2UnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnTG9naW5Db21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IExvZ2luTW9kYWxDb21wb25lbnQ7XG4gICAgbGV0IGZpeHR1cmU6IENvbXBvbmVudEZpeHR1cmU8TG9naW5Nb2RhbENvbXBvbmVudD47XG4gICAgbGV0IG1vY2tMb2dpblNlcnZpY2U6IE1vY2tMb2dpblNlcnZpY2U7XG4gICAgbGV0IG1vY2tSb3V0ZXI6IE1vY2tSb3V0ZXI7XG4gICAgbGV0IG1vY2tBY3RpdmVNb2RhbDogTW9ja0FjdGl2ZU1vZGFsO1xuXG4gICAgYmVmb3JlRWFjaChhc3luYygoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbQ29vcGN5Y2xlVGVzdE1vZHVsZV0sXG4gICAgICAgIGRlY2xhcmF0aW9uczogW0xvZ2luTW9kYWxDb21wb25lbnRdLFxuICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICBGb3JtQnVpbGRlcixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBMb2dpblNlcnZpY2UsXG4gICAgICAgICAgICB1c2VDbGFzczogTW9ja0xvZ2luU2VydmljZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSlcbiAgICAgICAgLm92ZXJyaWRlVGVtcGxhdGUoTG9naW5Nb2RhbENvbXBvbmVudCwgJycpXG4gICAgICAgIC5jb21waWxlQ29tcG9uZW50cygpO1xuICAgIH0pKTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KExvZ2luTW9kYWxDb21wb25lbnQpO1xuICAgICAgY29tcCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgICBtb2NrTG9naW5TZXJ2aWNlID0gVGVzdEJlZC5nZXQoTG9naW5TZXJ2aWNlKTtcbiAgICAgIG1vY2tSb3V0ZXIgPSBUZXN0QmVkLmdldChSb3V0ZXIpO1xuICAgICAgbW9ja0FjdGl2ZU1vZGFsID0gVGVzdEJlZC5nZXQoTmdiQWN0aXZlTW9kYWwpO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBhdXRoZW50aWNhdGUgdGhlIHVzZXInLCBpbmplY3QoXG4gICAgICBbXSxcbiAgICAgIGZha2VBc3luYygoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGNyZWRlbnRpYWxzID0ge1xuICAgICAgICAgIHVzZXJuYW1lOiAnYWRtaW4nLFxuICAgICAgICAgIHBhc3N3b3JkOiAnYWRtaW4nLFxuICAgICAgICAgIHJlbWVtYmVyTWU6IHRydWVcbiAgICAgICAgfTtcblxuICAgICAgICBjb21wLmxvZ2luRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICB1c2VybmFtZTogJ2FkbWluJyxcbiAgICAgICAgICBwYXNzd29yZDogJ2FkbWluJyxcbiAgICAgICAgICByZW1lbWJlck1lOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgICBtb2NrTG9naW5TZXJ2aWNlLnNldFJlc3BvbnNlKHt9KTtcbiAgICAgICAgbW9ja1JvdXRlci51cmwgPSAnL2FkbWluL21ldHJpY3MnO1xuXG4gICAgICAgIC8vIFdIRU4vXG4gICAgICAgIGNvbXAubG9naW4oKTtcbiAgICAgICAgdGljaygpOyAvLyBzaW11bGF0ZSBhc3luY1xuXG4gICAgICAgIC8vIFRIRU5cbiAgICAgICAgZXhwZWN0KGNvbXAuYXV0aGVudGljYXRpb25FcnJvcikudG9FcXVhbChmYWxzZSk7XG4gICAgICAgIGV4cGVjdChtb2NrQWN0aXZlTW9kYWwuY2xvc2VTcHkpLnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgZXhwZWN0KG1vY2tMb2dpblNlcnZpY2UubG9naW5TcHkpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKGNyZWRlbnRpYWxzKTtcbiAgICAgIH0pXG4gICAgKSk7XG5cbiAgICBpdCgnc2hvdWxkIGVtcHR5IHRoZSBjcmVkZW50aWFscyB1cG9uIGNhbmNlbCcsICgpID0+IHtcbiAgICAgIC8vIEdJVkVOXG4gICAgICBjb21wLmxvZ2luRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgdXNlcm5hbWU6ICdhZG1pbicsXG4gICAgICAgIHBhc3N3b3JkOiAnYWRtaW4nXG4gICAgICB9KTtcblxuICAgICAgY29uc3QgZXhwZWN0ZWQgPSB7XG4gICAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgICAgcGFzc3dvcmQ6ICcnLFxuICAgICAgICByZW1lbWJlck1lOiBmYWxzZVxuICAgICAgfTtcblxuICAgICAgLy8gV0hFTlxuICAgICAgY29tcC5jYW5jZWwoKTtcblxuICAgICAgLy8gVEhFTlxuICAgICAgZXhwZWN0KGNvbXAuYXV0aGVudGljYXRpb25FcnJvcikudG9FcXVhbChmYWxzZSk7XG4gICAgICBleHBlY3QoY29tcC5sb2dpbkZvcm0uZ2V0KCd1c2VybmFtZScpIS52YWx1ZSkudG9FcXVhbChleHBlY3RlZC51c2VybmFtZSk7XG4gICAgICBleHBlY3QoY29tcC5sb2dpbkZvcm0uZ2V0KCdwYXNzd29yZCcpIS52YWx1ZSkudG9FcXVhbChleHBlY3RlZC5wYXNzd29yZCk7XG4gICAgICBleHBlY3QoY29tcC5sb2dpbkZvcm0uZ2V0KCdyZW1lbWJlck1lJykhLnZhbHVlKS50b0VxdWFsKGV4cGVjdGVkLnJlbWVtYmVyTWUpO1xuICAgICAgZXhwZWN0KG1vY2tBY3RpdmVNb2RhbC5kaXNtaXNzU3B5KS50b0hhdmVCZWVuQ2FsbGVkV2l0aCgnY2FuY2VsJyk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIHJlZGlyZWN0IHVzZXIgd2hlbiByZWdpc3RlcicsICgpID0+IHtcbiAgICAgIC8vIFdIRU5cbiAgICAgIGNvbXAucmVnaXN0ZXIoKTtcblxuICAgICAgLy8gVEhFTlxuICAgICAgZXhwZWN0KG1vY2tBY3RpdmVNb2RhbC5kaXNtaXNzU3B5KS50b0hhdmVCZWVuQ2FsbGVkV2l0aCgndG8gc3RhdGUgcmVnaXN0ZXInKTtcbiAgICAgIGV4cGVjdChtb2NrUm91dGVyLm5hdmlnYXRlU3B5KS50b0hhdmVCZWVuQ2FsbGVkV2l0aChbJy9hY2NvdW50L3JlZ2lzdGVyJ10pO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCByZWRpcmVjdCB1c2VyIHdoZW4gcmVxdWVzdCBwYXNzd29yZCcsICgpID0+IHtcbiAgICAgIC8vIFdIRU5cbiAgICAgIGNvbXAucmVxdWVzdFJlc2V0UGFzc3dvcmQoKTtcblxuICAgICAgLy8gVEhFTlxuICAgICAgZXhwZWN0KG1vY2tBY3RpdmVNb2RhbC5kaXNtaXNzU3B5KS50b0hhdmVCZWVuQ2FsbGVkV2l0aCgndG8gc3RhdGUgcmVxdWVzdFJlc2V0Jyk7XG4gICAgICBleHBlY3QobW9ja1JvdXRlci5uYXZpZ2F0ZVNweSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoWycvYWNjb3VudC9yZXNldCcsICdyZXF1ZXN0J10pO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9